<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sf="http://www.ataccama.com/xslt/functions" xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl sf"> -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:functx="http://www.functx.com"
	exclude-result-prefixes="sf fn functx">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/metadata">
		<metadata>
			<xsl:copy-of select="settings"/>
			<xsl:copy-of select="systems"/>
			<logicalModel>
				<instanceModel>
					<xsl:copy-of select="logicalModel/instanceModel/relationships"/>
					<tables>	
						<xsl:for-each select="logicalModel/instanceModel/tables/table">
							<xsl:variable name="currentTable" select="@name"/>
							<table name="{@name}" type="{@type}" elemId="{@elemId}">
								<xsl:copy-of select="description"/>
								<xsl:copy-of select="columns"/>
								<matchingTab enableIdentify="{matchingTab/@enableIdentify}" matching="{matchingTab/@matching}">
									<xsl:copy-of select="matchingTab/matchingTabColumns"/>
									<multipleMatching disableDefault="{matchingTab/multipleMatching/@disableDefault}">
										<matchingDefinitions>
											<xsl:for-each select="matchingTab/multipleMatching/matchingDefinitions/matchingDefinition">
												<matchingDefinition name="{@name}" rematchFlagColumn="{@rematchFlagColumn}" isolateFlagColumn="{@isolateFlagColumn}" masterIdColumn="{@masterIdColumn}"/>
											</xsl:for-each>
										</matchingDefinitions>
									</multipleMatching>							
									<xsl:copy-of select="matchingTab/defaultMatchingDefinition"/>
								</matchingTab>
								<xsl:copy-of select="aggregationTab"/>
								<xsl:copy-of select="advanced"/>
								<guiTab>
									<xsl:copy-of select="//logicalModel/masterModels/masterModel[guiConfig/@enableGui='true']/instanceTables/instanceTable[@instanceTable=$currentTable]/guiTab/reduceES"/>											
									<xsl:copy-of select="//logicalModel/masterModels/masterModel[guiConfig/@enableGui='true']/instanceTables/instanceTable[@instanceTable=$currentTable]/guiTab/computedColumns"/>
									<views>
										<xsl:for-each select="//logicalModel/masterModels/masterModel[guiConfig/@enableGui='true']/instanceTables/instanceTable[@instanceTable=$currentTable]/guiTab/views/detailView">
											<xsl:copy-of select="."/>
										</xsl:for-each>
										<xsl:for-each select="//logicalModel/masterModels/masterModel[guiConfig/@enableGui='true']/instanceTables/instanceTable[@instanceTable=$currentTable]/guiTab/views/breadcrumbView">
											<breadcrumbView>
												<columns>
													<name>
														<xsl:for-each select="columns/column">
															<xsl:text>${</xsl:text><xsl:value-of select="@name"/><xsl:text>} </xsl:text>
														</xsl:for-each>
													</name>
												</columns>
											</breadcrumbView>
										</xsl:for-each>
									</views>
								</guiTab>
							</table>
						</xsl:for-each>
					</tables>
					<xsl:copy-of select="logicalModel/instanceModel/dicTables"/>
				</instanceModel>
				<masterModels>
					<xsl:for-each select="logicalModel/masterModels/masterModel">
						<masterModel name="{@name}">
							<xsl:copy-of select="description"/>
							<relationships>
								<xsl:for-each select="relationships/relationship">
									<relationship elemId="{@elemId}">
										<!-- copy all attributes -->
										<xsl:for-each select="@*[name()!='instanceRelationship']">
							                <xsl:attribute name="{name(.)}"><xsl:value-of select="."/></xsl:attribute>
							            </xsl:for-each>									
										<xsl:copy-of select="description"/>
										<xsl:copy-of select="foreignKey"/>
										<advanced>
											<xsl:copy-of select="advanced/extendedSameSystem"/>
										</advanced>
									</relationship>
								</xsl:for-each>
							</relationships>
							<masterTables>
								<xsl:for-each select="masterTables/masterTable">
									<masterTable elemId="{@elemId}">
							            <xsl:for-each select="@*">
							                <xsl:attribute name="{name(.)}"><xsl:value-of select="."/></xsl:attribute>
							            </xsl:for-each>											
										<xsl:copy-of select="description"/>
										<xsl:copy-of select="columns"/>
										<guiTab>
											<xsl:copy-of select="guiTab/reduceES"/>											
											<xsl:copy-of select="guiTab/computedColumns"/>
											<views>
												<xsl:for-each select="guiTab/views/detailView">
													<xsl:copy-of select="."/>
												</xsl:for-each>
												<xsl:for-each select="guiTab/views/breadcrumbView">
													<breadcrumbView>
														<columns>
															<name>
																<xsl:for-each select="columns/column">
																	<xsl:text>${</xsl:text><xsl:value-of select="@name"/><xsl:text>} </xsl:text>
																</xsl:for-each>
															</name>
														</columns>
													</breadcrumbView>
												</xsl:for-each>
											</views>
										</guiTab>
										<xsl:copy-of select="advanced"/>
									</masterTable>
								</xsl:for-each>
							</masterTables>
							<instanceTables>
								<xsl:for-each select="instanceTables/instanceTable">
									<instanceTable elemId="{@elemId}">
										<xsl:for-each select="@*">
							                <xsl:attribute name="{name(.)}"><xsl:value-of select="."/></xsl:attribute>
							            </xsl:for-each>
										<guiTab>
											<xsl:copy-of select="guiTab/reduceES"/>											
											<xsl:copy-of select="guiTab/computedColumns"/>										
											<views>
												<xsl:for-each select="guiTab/views/detailView">
													<xsl:copy-of select="."/>
												</xsl:for-each>
												<xsl:for-each select="guiTab/views/breadcrumbView">
													<breadcrumbView>
														<columns>
															<name>
																<xsl:for-each select="columns/column">
																	<xsl:text>${</xsl:text><xsl:value-of select="@name"/><xsl:text>} </xsl:text>
																</xsl:for-each>
															</name>
														</columns>
													</breadcrumbView>
												</xsl:for-each>
											</views>
										</guiTab>
										<xsl:copy-of select="columns"/>
									</instanceTable>								
								</xsl:for-each>
							</instanceTables>
						</masterModel>					
					</xsl:for-each>
				</masterModels>
				<xsl:copy-of select="logicalModel/dictionary"/>
			</logicalModel>
			<outputOperations>
				<exportModel>
					<consolidationExportOperations>
						<xsl:copy-of select="outputOperations/exportModel/*"/>
					</consolidationExportOperations>
				</exportModel>
				<eventHandler>
					<consolidationEventHandler>
						<xsl:copy-of select="outputOperations/eventHandler/handlers"/>
					</consolidationEventHandler>
				</eventHandler>
			</outputOperations>	
			<nativeServices>
				<consolidationServices>
					<basicServices processPurge="{nativeServices/basicServices/@processPurge}" getModel="{nativeServices/basicServices/@getModel}" listInstance="{if(nativeServices/basicServices/@searchInstance='true')then 'true' else nativeServices/basicServices/@listInstance}" processDelta="{nativeServices/basicServices/@processDelta}" rwControl="{nativeServices/basicServices/@rwControl}" batchControl="{nativeServices/basicServices/@batchControl}" getOverride="{nativeServices/basicServices/@getOverride}" genTraversal="{nativeServices/basicServices/@genTraversal}" listMaster="{if(nativeServices/basicServices/@searchMaster='true')then 'true' else nativeServices/basicServices/@listMaster}" modelStat="{nativeServices/basicServices/@modelStat}" getInstance="{nativeServices/basicServices/@getInstance}" getMaster="{basicServices/@getMaster}" processMatch="{nativeServices/basicServices/@processMatch}"/>
					<configurableServices>				
						<xsl:copy-of select="nativeServices/configurableServices/*[local-name(.)!='processUpsert']"/>
					</configurableServices>
				</consolidationServices>
				<xsl:copy-of select="nativeServices/endpointsDefinition"/>
				<sorServiceTab listSor="false" getSor="false"/>
			</nativeServices>
			<streaming>
				<consolidationStreaming>
					<consumers>
						<xsl:for-each select="streaming/consumers/consumer">
							<jmsStreamSource enable="{@enable}" name="{@name}">
								<xsl:copy-of select="child::node()"/>
							</jmsStreamSource>
						</xsl:for-each>
					</consumers>
				</consolidationStreaming>
			</streaming>
			<xsl:copy-of select="advancedSettings"/>	
			<guiConfig>
				<hierarchies>
					<xsl:for-each select="logicalModel/masterModels/masterModel[guiConfig/@enableGui='true']/guiConfig/hierarchies/*">
						<xsl:copy-of select="."/>
					</xsl:for-each>					
				</hierarchies>				
				<xsl:copy-of select="logicalModel/masterModels/masterModel[guiConfig/@enableGui='true']/guiConfig/actions"/>
				<xsl:copy-of select="logicalModel/masterModels/masterModel[guiConfig/@enableGui='true']/guiConfig/dataTypeFormats"/>
				<xsl:copy-of select="logicalModel/masterModels/masterModel[guiConfig/@enableGui='true']/guiConfig/indicators"/>								
			</guiConfig>				
		</metadata>
	</xsl:template>
</xsl:stylesheet>
