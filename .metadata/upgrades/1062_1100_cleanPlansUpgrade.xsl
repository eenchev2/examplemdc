<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>	

<xsl:param name="fileName"/>    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>
    
    <xsl:variable name="entity" select="replace($fileName, '(.*)_clean.comp', '$1')"/>

<!-- Translate naming -->
    <xsl:template match="step[@className='com.ataccama.dqc.tasks.clean.LookupAlgorithm' and contains(@id, 'Translate ')]">
		<step id="translate_{replace(@id, 'Translate ', '')}_in_{$entity}" className="{@className}" mode="NORMAL" xmlns:comm="{@xmlns:comm}">
			<xsl:copy-of select="properties"/>
			<xsl:copy-of select="visual-constraints"/>
		</step>
    </xsl:template>
    
<!-- Lookup naming -->
    <xsl:template match="step[@className='com.ataccama.dqc.tasks.clean.LookupAlgorithm' and contains(@id, 'Lookup ')]">
		<step id="translate_{properties/@foreignKeyColumn}_in_{$entity}" className="{@className}" mode="NORMAL" xmlns:comm="{@xmlns:comm}">
			<xsl:copy-of select="properties"/>
			<xsl:copy-of select="visual-constraints"/>
		</step>
    </xsl:template>
    
<!-- Validate naming -->
    <xsl:template match="step[@className='com.ataccama.dqc.tasks.clean.LookupAlgorithm' and contains(@id, 'Validate_')]">
		<step id="validate_{replace(@id, 'Validate_', '')}_in_{$entity}" className="{@className}" mode="NORMAL" xmlns:comm="{@xmlns:comm}">
			<xsl:copy-of select="properties"/>
			<xsl:copy-of select="visual-constraints"/>
		</step>
    </xsl:template>
    
<!-- connections - for 'Lookup' step is not possible to create connection now. We can't detect column name which we need to create conn. -->
	<xsl:template match="connection[contains(target/@step, 'Translate ') or contains(target/@step, 'Validate_') 
								or contains(source/@step, 'Translate ') or contains(source/@step, 'Validate_')]">
		<xsl:variable name="sourceStep">
  			<xsl:choose>
    			<xsl:when test="contains(source/@step, 'Validate_')">
					<xsl:value-of select="concat('validate_', replace(source/@step, 'Validate_', ''), '_in_', $entity)"/>
				</xsl:when>
				<xsl:when test="contains(source/@step, 'Translate ')">
					<xsl:value-of select="concat('translate_', replace(source/@step, 'Translate ', ''), '_in_', $entity)"/>
				</xsl:when>
    			<xsl:otherwise>
    				<xsl:value-of select="source/@step"/>
    			</xsl:otherwise>
  			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="targetStep">
  			<xsl:choose>
    			<xsl:when test="contains(target/@step, 'Validate_')">
					<xsl:value-of select="concat('validate_', replace(target/@step, 'Validate_', ''), '_in_', $entity)"/>
				</xsl:when>
				<xsl:when test="contains(target/@step, 'Translate ')">
					<xsl:value-of select="concat('translate_', replace(target/@step, 'Translate ', ''), '_in_', $entity)"/>
				</xsl:when>
    			<xsl:otherwise>
    				<xsl:value-of select="target/@step"/>
    			</xsl:otherwise>
  			</xsl:choose>
		</xsl:variable>
		
		<connection className="{@className}" disabled="{@disabled}">
			<source step="{$sourceStep}" endpoint="{source/@endpoint}"/>
			<target step="{$targetStep}" endpoint="{target/@endpoint}"/>
			<xsl:copy-of select="visual-constraints"/>
		</connection>
	
	</xsl:template>
		       	
</xsl:stylesheet>