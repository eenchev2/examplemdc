<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sf="http://www.ataccama.com/xslt/functions" xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl sf"> -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:functx="http://www.functx.com"
	exclude-result-prefixes="sf fn functx">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/metadata">
		<metadata>
			<xsl:copy-of select="settings"/>
			<xsl:copy-of select="systems"/>
			<xsl:copy-of select="logicalModel"/>
			<xsl:copy-of select="outputOperations"/>			
			<xsl:copy-of select="nativeServices"/>
			<xsl:copy-of select="streaming"/>
			<guiConfig>
				<xsl:copy-of select="guiConfig/searchTab"/>
				<hierarchies>
					<guiHierarchiesMaster>
						<layers>
							<layer enable="true" allRels="false" layerName="{logicalModel/masterModels/masterModel[1]/@name}">
								<guiHierarchiesArray>
									<xsl:for-each select="guiConfig/hierarchies/hierarchy1N">
										<hierarchy1N enable="true" name="{@label}" label="{@label}" relationship="{@relationship}">
											<xsl:variable name="relName" select="@relationship"/>
											<startPoint label="{startPoint/@label}" position="LEFT" entity="{/metadata/logicalModel/masterModels/masterModel[1]/relationships/relationship[@name=$relName]/@parentTable}"/>
											<endPoint label="{endPoint/@label}" position="RIGHT" entity="{/metadata/logicalModel/masterModels/masterModel[1]/relationships/relationship[@name=$relName]/@childTable}"/>
										</hierarchy1N>
									</xsl:for-each>		
									<xsl:for-each select="guiConfig/hierarchies/hierarchyMN">
										<hierarchyMN enable="true" name="{@label}" label="{@label}" tableMN="{@tableMN}">
											<xsl:variable name="tableMNName" select="@tableMN"/>
											<startMNPoint label="{startPoint/@label}" position="LEFT" relationship="{/metadata/logicalModel/masterModels/masterModel[1]/relationships/relationship[@childTable=$tableMNName][1]/@name}"/>
											<endMNPoint label="{endPoint/@label}" position="RIGHT" relationship="{/metadata/logicalModel/masterModels/masterModel[1]/relationships/relationship[@childTable=$tableMNName][2]/@name}"/>
										</hierarchyMN>
									</xsl:for-each>															
								</guiHierarchiesArray>
							</layer>
						</layers>
					</guiHierarchiesMaster>
				</hierarchies>		
				<xsl:copy-of select="guiConfig/actions"/>
				<xsl:copy-of select="guiConfig/indicators"/>
				<xsl:copy-of select="guiConfig/guiConstants"/>
				<xsl:copy-of select="guiConfig/wfConfig"/>
				<xsl:copy-of select="guiConfig/globalValidations"/>
				<guiPreferences>
					<dataTypeFormats datetime="{guiConfig/dataTypeFormats/@datetime}" boolean="{guiConfig/dataTypeFormats/@boolean}" integer="{guiConfig/dataTypeFormats/@integer}" float="{guiConfig/dataTypeFormats/@float}" day="{guiConfig/dataTypeFormats/@day}" long="{guiConfig/dataTypeFormats/@long}"/>
				</guiPreferences>
			</guiConfig>			
			<xsl:copy-of select="advancedSettings"/>					
		</metadata>
	</xsl:template>
</xsl:stylesheet>
