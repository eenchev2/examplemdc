<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>

	<xsl:template match="component[@class='com.ataccama.dqc.web.HttpDispatcher']">
		<component class="com.ataccama.dqc.web.HttpDispatcher">
			<listeners>
				<xsl:for-each select="listeners/*[@name='default']">					
					<listener name="default" port="{@port}" ssl="{@ssl}" threads="{@threads}">
						<handlerSecurity class="com.ataccama.dqc.web.security.KeycloakSecurity">
						    <interceptUrls>
						        <interceptUrl pattern="/xmlRpcOverHttp/**" access="permitAll"/>
	`					        <interceptUrl pattern="/soapOverHttp/**" access="permitAll"/>
						        <interceptUrl pattern="/**" access="isAuthenticated()"/>
						    </interceptUrls>
							<deploymentContexts>
								<deploymentContext pattern="/api/mda" configFile="keycloak-mda-server.json"/>
								<deploymentContext pattern="/api/userSettings" configFile="keycloak-mda-server.json"/>
								<deploymentContext pattern="/api/issueTrackerMda" configFile="keycloak-mda-im.json"/>
								<deploymentContext pattern="/api/dqit/**" configFile="keycloak-mda-im.json"/>
								<deploymentContext pattern="/**" configFile="keycloak-admin-center.json"/>
							</deploymentContexts>
						</handlerSecurity>
	                </listener>					
				</xsl:for-each>			
				<xsl:for-each select="listeners/*[not(@name='default')]">
					<xsl:copy-of select="."/>
				</xsl:for-each>
				<xsl:apply-templates select="//component[@class='com.ataccama.server.jetty.JettyComponent']/listeners/*"/>
			</listeners>
		</component>
	</xsl:template>	
	
	<xsl:template match="component[@class='com.ataccama.server.jetty.JettyComponent']">
	</xsl:template>
	
	<xsl:template match="component[@class='com.ataccama.web.amc.core.auth.UmcManagerComponent']">
		<component class="com.ataccama.server.component.usersettings.UserSettingsComponent">
			<xsl:comment><![CDATA[<userSettingsFactory class="com.ataccama.usersettings.inmemory.InMemoryUserSettingsFactory"/>]]></xsl:comment>
			<userSettingsFactory class="com.ataccama.usersettings.jdbc.JdbcUserSettingsFactory" dataSource="mdc_db" prefix="us_"/>
		</component>	
	</xsl:template>	

	<xsl:template match="component[@class='com.ataccama.dqc.server.services.AuthenticationService']">
		<component class="com.ataccama.server.component.dqit.DqitServerComponent">
			<location>/api/dqit</location>
			<configFile>issue_management/config.xml</configFile>
		</component>        
        <component class="com.ataccama.epp.mda.DqitMdaServerComponent">
        </component>	
	</xsl:template>
	
	<xsl:template match="component[@class='com.ataccama.server.component.elastic.ElasticsearchComponent']">
	</xsl:template>	

</xsl:stylesheet>