<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/> 
         
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>       
    
	<xsl:template match="mda-config">
		<mda-config>
			<templatePath>gui_templates</templatePath>
			<workflow>mda-workflow.xml</workflow>                
			<labelFile>mda-label.gen.properties</labelFile>
			<lookupsFile>mda-lookups.gen.xml</lookupsFile>
			<dataSetsFile>mda-datasets.gen.xml</dataSetsFile>
			<filterDefinitionsFile>mda-search.gen.xml</filterDefinitionsFile>
			<displaySettings>mda-display.gen.xml</displaySettings>
			<xsl:apply-templates select="@*|node()" />			
		</mda-config>
	</xsl:template>        
    
	<xsl:template match="model">
	</xsl:template>    
    
	<xsl:template match="synchronizationDirectory">
	</xsl:template>	
	
	<xsl:template match="searchBackend">
	</xsl:template>
	
	<xsl:template match="editLockTimeout">
	</xsl:template>	

	<xsl:template match="auditLog">
	</xsl:template>	

	<xsl:template match="issueBackend">
	</xsl:template>	
	
	<xsl:template match="validations">	
		<validations>mda-validations.gen.xml</validations>
	</xsl:template>		

	<xsl:template match="authConfig">
	</xsl:template>	

</xsl:stylesheet>