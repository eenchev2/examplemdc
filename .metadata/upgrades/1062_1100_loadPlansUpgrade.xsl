<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>	

<xsl:param name="fileName"/>    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>

<!-- Record Counters naming -->
    <xsl:template match="step[@className='com.ataccama.dqc.tasks.flow.RecordCounter' and contains(@id, 'SRC_')]">
		<step id="counter_{replace(@id, 'SRC_', '')}_load_out" className="{@className}" mode="NORMAL" xmlns:comm="{@xmlns:comm}">
			<xsl:copy-of select="properties"/>
			<xsl:copy-of select="visual-constraints"/>
		</step>
    </xsl:template>
    
<!-- Translate Lookups naming -->
    <xsl:template match="step[@className='com.ataccama.dqc.tasks.clean.LookupAlgorithm' and contains(@id, 'Translate ')]">
    	<xsl:variable name="upperStepTrans" select="//connection[target/@step = current()/@id]/source/@step"/>
		<xsl:variable name="entityTrans">
  			<xsl:choose>
    			<xsl:when test="contains($upperStepTrans, 'map_internal')">
					<xsl:value-of select="replace($upperStepTrans, '(.*)_to_(.*)' , '$2')"/>
				</xsl:when>
				<xsl:when test="contains($upperStepTrans, 'map_')">
					<xsl:value-of select="replace(replace($upperStepTrans, 'map_' , ''), '_source_columns', '')"/>
				</xsl:when>
    			<xsl:otherwise></xsl:otherwise>
  			</xsl:choose>
		</xsl:variable>
		<step id="translate_{replace(@id, 'Translate ', '')}_in_{$entityTrans}" className="{@className}" disabled="{@disabled}" mode="NORMAL" xmlns:comm="{@xmlns:comm}">
			<xsl:copy-of select="properties"/>
			<xsl:copy-of select="visual-constraints"/>
		</step>
    </xsl:template>
    
<!-- Validate Lookups naming -->
    <xsl:template match="step[@className='com.ataccama.dqc.tasks.clean.LookupAlgorithm' and contains(@id, 'Validate_')]">
    	<xsl:variable name="upperStepVal" select="//connection[target/@step = current()/@id]/source/@step"/>
		<xsl:variable name="entityVal">
  			<xsl:choose>
    			<xsl:when test="contains($upperStepVal, 'map_internal')">
					<xsl:value-of select="replace($upperStepVal, '(.*)_to_(.*)' , '$2')"/>
				</xsl:when>
				<xsl:when test="contains($upperStepVal, 'map_')">
					<xsl:value-of select="replace(replace($upperStepVal, 'map_' , ''), '_source_columns', '')"/>
				</xsl:when>
    			<xsl:otherwise></xsl:otherwise>
  			</xsl:choose>
		</xsl:variable>
		<step id="validate_{replace(@id, 'Validate_', '')}_in_{$entityVal}" className="{@className}" disabled="{@disabled}" mode="NORMAL" xmlns:comm="{@xmlns:comm}">
			<xsl:copy-of select="properties"/>
			<xsl:copy-of select="visual-constraints"/>
		</step>
    </xsl:template>    

<!-- connections -->	
	<xsl:template match="connection[contains(target/@step, 'SRC_') or contains(target/@step, 'Validate_') or contains(target/@step, 'Translate ')
									or contains(source/@step, 'SRC_') or contains(source/@step, 'Validate_') or contains(source/@step, 'Translate ')]">
		<!-- <xsl:variable name="entity" select="replace(target/@step, 'SRC_', '')"/> -->
		
		<xsl:variable name="entity">
  			<xsl:choose>
    			<xsl:when test="contains(source/@step, 'SRC_')">
					<xsl:value-of select="replace(source/@step, 'SRC_', '')"/>
				</xsl:when>
				<xsl:when test="contains(target/@step, 'SRC_')">
					<xsl:value-of select="replace(target/@step, 'SRC_', '')"/>
				</xsl:when>
				<xsl:when test="contains(source/@step, 'map_internal')">
					<xsl:value-of select="replace(source/@step, '(.*)_to_(.*)' , '$2')"/>
				</xsl:when>
				<xsl:when test="contains(source/@step, 'map_')">
					<xsl:value-of select="replace(replace(source/@step, 'map_', ''), '_source_columns', '')"/>
				</xsl:when>
				<xsl:when test="//step[@id = current()/target/@step and @className = 'com.ataccama.dqc.tasks.common.usersteps.io.OutputStep']">
					<xsl:value-of select="target/@step"/>
				</xsl:when>
    			<xsl:otherwise></xsl:otherwise>
  			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="sourceStep">
  			<xsl:choose>
    			<xsl:when test="contains(source/@step, 'Validate_')">
					<xsl:value-of select="concat('validate_', replace(source/@step, 'Validate_', ''), '_in_', $entity)"/>
				</xsl:when>
				<xsl:when test="contains(source/@step, 'Translate ')">
					<xsl:value-of select="concat('translate_', replace(source/@step, 'Translate ', ''), '_in_', $entity)"/>
				</xsl:when>
				<xsl:when test="contains(source/@step, 'SRC_')">
					<xsl:value-of select="concat('counter_', $entity, '_load_out')"/>
				</xsl:when>
    			<xsl:otherwise>
    				<xsl:value-of select="source/@step"/>
    			</xsl:otherwise>
  			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="targetStep">
  			<xsl:choose>
    			<xsl:when test="contains(target/@step, 'Validate_')">
					<xsl:value-of select="concat('validate_', replace(target/@step, 'Validate_', ''), '_in_', $entity)"/>
				</xsl:when>
				<xsl:when test="contains(target/@step, 'Translate ')">
					<xsl:value-of select="concat('translate_', replace(target/@step, 'Translate ', ''), '_in_', $entity)"/>
				</xsl:when>
				<xsl:when test="contains(target/@step, 'SRC_')">
					<xsl:value-of select="concat('counter_', $entity, '_load_out')"/>
				</xsl:when>
    			<xsl:otherwise>
    				<xsl:value-of select="target/@step"/>
    			</xsl:otherwise>
  			</xsl:choose>
		</xsl:variable>
		
		<connection className="{@className}" disabled="{@disabled}">
			<source step="{$sourceStep}" endpoint="{source/@endpoint}"/>
			<target step="{$targetStep}" endpoint="{target/@endpoint}"/>
			<xsl:copy-of select="visual-constraints"/>
		</connection>
	</xsl:template>
	       	
</xsl:stylesheet>