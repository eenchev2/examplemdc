<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sf="http://www.ataccama.com/xslt/functions" xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl sf"> -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:functx="http://www.functx.com"
	exclude-result-prefixes="sf fn functx">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/metadata">
		<metadata>
			<settings deletionStrategy = "{settings/@deletionStrategy}" enableRC="false" scope="{settings/@scope}" batchSizeRC="{@settings/batchSizeRC}">
				<xsl:copy-of select="settings/lengthValidation"/>
				<xsl:copy-of select="settings/advancedSettings"/>
			</settings>
			<!-- 
			<settings deletionStrategy="deactivate" enableRC="false" scope="active" batchSizeRC="100000">
	<lengthValidation databaseType="Oracle R9+" nmePrefixLength="2"/>
	<advancedSettings alternativeKeys="true" expStringLength="500" matchingCompatibility="false" srcStringLength="100">
		<ignoredComparisonColumns/>
	</advancedSettings>
</settings>
			 -->
			<xsl:copy-of select="systems"/>
			<xsl:copy-of select="outputOperations"/>
			<logicalModel>
				<xsl:copy-of select="logicalModel/instanceModel"/>
				<masterModels>
				<xsl:for-each select="logicalModel/masterModels/masterModel">
					<masterModel name="{@name}">
						<description><xsl:value-of select="@description|description"/></description>
						<xsl:copy-of select="relationships"/>
						<masterTables>
							<xsl:for-each select="masterTables/masterTable">
								<masterTable name="{@name}" label="{@label}" entityRole="{@entityRole}" instanceTable="{@instanceTable}" topLevel="{@topLevel}">
									<description><xsl:value-of select="@description|description"/></description>
									<xsl:copy-of select="columns"/>
									<guiTab>
										<xsl:copy-of select="guiTab/reduceES"/>
										<views>
											<xsl:for-each select="guiTab/views/detailView">
												<detailView>
													<groups>
													 	<xsl:variable name="conditions" select="distinct-values(groups/*[viewCondition!='']/viewCondition)"/>
														<xsl:for-each select="groups/labeledGroup">		
															<labeledGroup templatePosition="{@templatePosition}" viewCondition="{if(current()/viewCondition!='') then concat('cond_' ,index-of($conditions, current()/viewCondition)) else ''}" name="{@name}" label="{@label}">
																<xsl:copy-of select="columns"/>
															</labeledGroup>
														</xsl:for-each>
														<xsl:for-each select="groups/labeledRelatedList">
															<labeledRelatedList relationship="{@relationship}" templatePosition="{@templatePosition}" viewCondition="{if(current()/viewCondition!='') then concat('cond_' ,index-of($conditions, current()/viewCondition)) else ''}" name="{@name}" label="{@label}">
																<xsl:copy-of select="linkedRecordParams"/>
															</labeledRelatedList>
														</xsl:for-each>
														<xsl:for-each select="groups/labeledRelatedMNList">
															<labeledRelatedMNList relationship="{@relationship}" entity_out="{@entity_out}" templatePosition="{@templatePosition}" viewCondition="{if(current()/viewCondition!='') then concat('cond_' ,index-of($conditions, current()/viewCondition)) else ''}" name="{@name}" relationship_out="{@relationship_out}" label="{@label}">
																<xsl:copy-of select="MNrelationParams"/>
																<xsl:copy-of select="MNlinkedRecordsParams"/>
															</labeledRelatedMNList>
														</xsl:for-each>
														<xsl:for-each select="groups/listGrid">
															<listGrid relationship="{@relationship}" templatePosition="{@templatePosition}" viewCondition="{if(current()/viewCondition!='') then concat('cond_' ,index-of($conditions, current()/viewCondition)) else ''}" name="{@name}" label="{@label}">
																<xsl:copy-of select="lists"/>
															</listGrid>
														</xsl:for-each>
													</groups>
													<guiDetailConditions>
                                                       <xsl:variable name="conditions" select="distinct-values(groups/*[viewCondition!='']/viewCondition)"/>
                                                        <xsl:for-each select="$conditions">                                                            
                                                            <guiDetailCondition name="cond_{position()}">
                                                                <condition><xsl:value-of select="."/></condition>
                                                            </guiDetailCondition>
                                                        </xsl:for-each>
                                                    </guiDetailConditions>
												</detailView>
											</xsl:for-each>
											<xsl:for-each select="guiTab/views/breadcrumbView">
												<xsl:copy-of select="."/>
											</xsl:for-each>
										</views>
										<xsl:copy-of select="guiTab/computedColumns"/>
									</guiTab>
									<xsl:copy-of select="advanced"/>
									<guiEdit guiReadOnly="{guiEdit/@guiReadOnly}">
										<xsl:variable select="guiEdit/@guiReadOnly" name="guiReadOnly"/>
										<editColumns>
											<xsl:for-each select="guiEdit/editColumns/column">
												<column nmeMapping="{@nmeMapping}" editable="{if($guiReadOnly='Read-Only') then 'false' else 'true'}" name="{@name}"/>
											</xsl:for-each>
										</editColumns>
										<xsl:copy-of select="guiEdit/parentColumns"/>
									</guiEdit>
									<!-- <xsl:copy-of select="guiEdit"/> -->
								</masterTable>
							</xsl:for-each>	
						</masterTables>
						<instanceTables>
							<xsl:for-each select="instanceTables/instanceTable">
								<instanceTable name="{@name}" label="{@label}" instanceTable="{@instanceTable}" topLevel="{@topLevel}">
									<guiTab>
										<xsl:copy-of select="guiTab/reduceES"/>
										<views>
											<xsl:for-each select="guiTab/views/detailView">
												<detailView>
													<groups>
														<xsl:variable name="conditions" select="distinct-values(groups/*[viewCondition!='']/viewCondition)"/>
														<xsl:for-each select="groups/labeledGroup">
															<labeledGroup templatePosition="{@templatePosition}" viewCondition="{if(current()/viewCondition!='') then concat('cond_' ,index-of($conditions, current()/viewCondition)) else ''}" name="{@name}" label="{@label}">
																<xsl:copy-of select="columns"/>
															</labeledGroup>
														</xsl:for-each>
														<xsl:for-each select="groups/labeledRelatedList">
															<labeledRelatedList relationship="{@relationship}" templatePosition="{@templatePosition}" viewCondition="{if(current()/viewCondition!='') then concat('cond_' ,index-of($conditions, current()/viewCondition)) else ''}" name="{@name}" label="{@label}">
																<xsl:copy-of select="linkedRecordParams"/>
															</labeledRelatedList>
														</xsl:for-each>
														<xsl:for-each select="groups/labeledRelatedMNList">
															<labeledRelatedMNList relationship="{@relationship}" entity_out="{@entity_out}" templatePosition="{@templatePosition}" viewCondition="{if(current()/viewCondition!='') then concat('cond_' ,index-of($conditions, current()/viewCondition)) else ''}" name="{@name}" relationship_out="{@relationship_out}" label="{@label}">
																<xsl:copy-of select="MNrelationParams"/>
																<xsl:copy-of select="MNlinkedRecordsParams"/>
															</labeledRelatedMNList>
														</xsl:for-each>
														<xsl:for-each select="groups/listGrid">
															<listGrid relationship="{@relationship}" templatePosition="{@templatePosition}" viewCondition="{if(current()/viewCondition!='') then concat('cond_' ,index-of($conditions, current()/viewCondition)) else ''}" name="{@name}" label="{@label}">
																<xsl:copy-of select="lists"/>
															</listGrid>
														</xsl:for-each>
												</groups>
												<guiDetailConditions>
                                                	<xsl:variable name="conditions" select="distinct-values(groups/*[viewCondition!='']/viewCondition)"/>
                                                    <xsl:for-each select="$conditions">                                                            
                                                       	<guiDetailCondition name="cond_{position()}">
                                                    	    <condition><xsl:value-of select="."/></condition>
                                                        </guiDetailCondition>
                                                    </xsl:for-each>
                                                 </guiDetailConditions>
											</detailView>
										</xsl:for-each>
									</views>
									<xsl:copy-of select="guiTab/computedColumns"/>
								</guiTab>
								<xsl:copy-of select="columns"/>
							</instanceTable>
						</xsl:for-each>
					</instanceTables>
					<xsl:copy-of select="guiConfig"/>
					<xsl:copy-of select="maxLength"/>
				</masterModel>
			</xsl:for-each>
			</masterModels>	
							
				<xsl:copy-of select="logicalModel/dictionary"/>
			</logicalModel>
	
			<xsl:copy-of select="nativeServices"/>
			<xsl:copy-of select="advancedSettings"/>		
		</metadata>
	</xsl:template>
</xsl:stylesheet>
