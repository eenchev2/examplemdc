<?xml version='1.0' encoding='UTF-8'?>
<metametadata-library>
	<nodes><!-- Physical model: databases: tables --><node generated="true" icon="icons/views.png" name="databaseModel" abstract="false" id="DatabaseModelNode" label="Physical Model View" sortChildren="false">
			<subNodes>
				<subNode min="1" max="1" id="InstanceTablesNode"/>
				<subNode min="1" max="1" id="MasterTablesNode"/>
				<subNode min="1" max="1" id="RefDataTablesNode"/>
				<subNode min="1" max="1" id="LookupTablesNode"/>
				<subNode min="1" max="1" id="SoRTablesNode"/>
			</subNodes>
			<attributes>
				<attribute defaultValue="false" name="generateDoc" type="boolean" required="true"/>
			</attributes>
			<description>Shows both the Instance and Master Layers transformed to physical representations. Internal engine attributes are added as well. Also Reference data related entities and Lookups are shown here as a reference.&lt;br/&gt;
Please note the Physical Model is not 100% equal to DB objects.</description>
		</node>
		<node name="instanceTables" id="InstanceTablesNode" label="Instance Tables" inherits="TablesNode">
			<subNodes>
				
			</subNodes>
			<description>List of tables representing MDM Physical Model of Instance Layer</description>
		</node>
		<node name="masterTables" id="MasterTablesNode" label="Master Tables" inherits="TablesNode">
			<description>List of tables representing MDM Physical Model of Master Layer(s).</description>
		</node>
		<node icon="icons/tables.png" name="tables" id="TablesNode" label="(tables)">
			<attributes>
			</attributes>
			<subNodes>
				<subNode min="0" max="-1" id="PhysicalTable"/>
			</subNodes>
			<editors>
			</editors>
		</node>
		<node icon="icons/view.png" name="physicalTable" explorerLeaf="true" id="PhysicalTable" label="{.[ancestor::masterTables]/@layerName} {@name} ({@type})">
			<editors>
				<editor openModal="true" class="com.ataccama.ame.core.editors.PropertyEditor"/>
			</editors>
			<attributes>
				<attribute name="name" label="Table Name" type="string">
					<description>Table name</description>
				</attribute>
				<attribute name="layerName" type="string">
					<description>Layer name:
Instance
Master
Reference
</description>
				</attribute>
				<attribute visible="false" name="customActivity" type="boolean"/>
				<attribute visible="false" name="refData" type="string"/>
				<attribute name="type" label="Table Type" type="string">
					<description>Table type
Instance
Master
Reference</description>
				</attribute>
				<attribute visible="false" name="description" type="string"/>
				<attribute visible="false" name="label" type="string"/>
			</attributes>
			<subNodes>
				<subNode min="1" max="1" id="RelationshipsNode"/>
				<subNode min="1" max="1" id="PhysicalTableColumns"/>
				<subNode min="1" max="1" id="VirtualColumns"/>
				<subNode min="1" max="1" id="intColumnsNode"/>
			</subNodes>
			<validations>
				<validation class="com.ataccama.ame.core.validations.UniqueValuesValidation">
					<selectPath>columns/column/@name</selectPath>
					<message>Duplicate column name!</message>
				</validation>
			</validations>
		</node>
		<node expanded="true" name="columns" id="PhysicalTableColumns" label="Columns" newTab="false">
			<subNodes>
				<subNode min="0" max="-1" id="PhysicalTableColumn"/>
			</subNodes>
		</node>
		<node name="column" id="PhysicalTableColumn" label="{@name}">
			<attributes>
				<attribute name="name" type="string"/>
				<attribute name="type" type="string"/>
				<attribute name="size" type="integer"/>
				<attribute name="origin" type="string"/>
				<attribute name="isPk" label="PK" type="boolean"/>
				<attribute name="isFk" label="FK" type="boolean"/>
				<attribute visible="false" name="dic" type="string"/>
				<attribute visible="true" defaultValue="false" name="artificial" type="boolean"/>
				<attribute defaultValue="false" name="originalPk" type="boolean"/>
				<attribute defaultValue="false" name="originalFk" type="boolean"/>
				<attribute defaultValue="true" name="load" type="boolean" required="true">
					<description>&lt;ul&gt;
&lt;li&gt;&lt;i&gt;true&lt;/i&gt;: Columns are used.
&lt;li&gt;&lt;i&gt;false&lt;/i&gt;: Columns are never used, they are seen only here.
&lt;/ul&gt;
&lt;!-- 100 --&gt;</description>
				</attribute>
				<attribute visible="false" defaultValue="false" name="extendedSS" type="boolean"/>
				<attribute visible="false" name="dqcType" type="string"/>
				<attribute visible="false" name="usageValidation" type="string"/>
				<attribute visible="false" name="refData" type="string"/>
				<attribute visible="false" name="isSrc" type="string"/>
				<attribute visible="false" name="instUse" type="string"/>
				<attribute visible="false" defaultValue="false" name="eng" type="boolean"/>
				<attribute visible="false" name="description" type="string"/>
				<attribute visible="false" name="label" type="string"/>
				<attribute visible="false" name="isSco" type="string"/>
				<attribute visible="false" name="isExp" type="string"/>
			</attributes>
			<subNodes>
				<subNode min="0" max="1" id="ValueDefinitionNode"/>
				<subNode min="0" max="1" id="CompoundDefinitionNode"/>
				<subNode min="0" max="1" id="AggregationDefinitionNode"/>
			</subNodes>
			<validations>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation" inverseCondition="true">
					<expression>.[@name = lower-case(../../intColumns/intColumn/@name) and @origin != &#39;internal&#39;  and @origin != &#39;internal_match&#39;]</expression>
					<message>Column name &#39;{@name}&#39; in {../../@name} ({../../@layerName}) is reserved and cannot be used. Origin: {@origin}.</message>
				</validation>
			</validations>
		</node>
		<node visible="false" name="aggregationSource" id="AggregationDefinitionNode" label="Aggregation">
			<attributes>
				<attribute name="relationshipName" type="string"/>
				<attribute name="sourceColumn" type="string"/>
				<attribute name="method" type="string"/>
				<attribute name="separator" type="string"/>
				<attribute name="filterExpression" type="text"/>
			</attributes>
		</node>
		<node name="refData" id="RefDataTablesNode" label="Reference Data Tables" inherits="TablesNode">
			<description>List of tables representing MDM Physical Model of Reference Data dictionaries. It is a part of Instance Layer.</description>
		</node>
		<node visible="false" name="valueDefinition" id="ValueDefinitionNode" label="Value definition">
			<attributes>
				<attribute name="srcColumn" type="string"/>
				<attribute name="type" type="enum" enumValues="historical,oldValue"/>
				<attribute name="maxCnt" type="integer"/>
				<attribute name="separator" type="string"/>
			</attributes>
		</node>
		<node expanded="true" name="relationships" id="RelationshipsNode" label="Relationships" newTab="true">
			<subNodes>
				<subNode min="0" max="-1" id="RelationshipNode"/>
			</subNodes>
		</node>
		<node name="relationship" id="RelationshipNode" label="{@name}">
			<attributes>
				<attribute name="name" type="string"/>
				<attribute name="parentTable" type="string"/>
				<attribute name="childTable" type="string"/>
				<attribute name="parentColumn" type="string"/>
				<attribute name="childColumn" type="string"/>
				<attribute visible="false" name="description" type="string"/>
			</attributes>
			<validations>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation" inverseCondition="true">
					<expression>.[@parentTable=preceding-sibling::relationship/@parentTable and @childColumn=preceding-sibling::relationship/@childColumn and @childColumn!=&#39;&#39;]</expression>
					<message>Duplicate foreign key - define alternative FK name!</message>
				</validation>
			</validations>
		</node>
		<node visible="false" name="compoundDefinition" id="CompoundDefinitionNode" visibleInParent="false">
			<attributes>
				
			</attributes>
			<subNodes>
				<subNode min="0" max="-1" id="CompoundColumnNode"/>
			</subNodes>
		</node>
		<node name="column" id="CompoundColumnNode">
			<attributes>
				<attribute name="name" type="string"/>
				<attribute name="label" type="string"/>
				<attribute name="type" type="string"/>
				<attribute name="dqcType" type="string"/>
				<attribute name="description" type="string"/>
			</attributes>
		</node>
		<node name="lookups" id="LookupTablesNode" label="Lookups" inherits="TablesNode">
			<description>List of Reference Data lookups (i.e. *.lkp files), that are used to translate source system dependent codes into master codes.</description>
		</node>
		<node visible="false" name="virtualColumns" id="VirtualColumns" label="Virtual Columns" newTab="true" visibleInParent="false">
			<subNodes>
				<subNode min="0" max="-1" id="VirtualColumn"/>
			</subNodes>
		</node>
		<node name="column" id="VirtualColumn" label="Column">
			<attributes>
				<attribute name="name" type="string"/>
				<attribute name="type" type="string"/>
				<attribute name="size" type="integer"/>
				<attribute name="origin" type="string"/>
				<attribute name="isPk" label="PK" type="boolean"/>
				<attribute name="isFk" label="FK" type="boolean"/>
				<attribute visible="false" name="dqcType" type="string"/>
			</attributes>
		</node>
		<node name="intColumn" id="intColumnNode">
			<attributes>
				<attribute name="name" type="string"/>
			</attributes>
		</node>
		<node visible="false" generated="true" name="intColumns" explorerLeaf="true" id="intColumnsNode">
			<subNodes>
				<subNode min="0" max="-1" id="intColumnNode"/>
			</subNodes>
		</node>
		<node name="sorTables" id="SoRTablesNode" label="SoR Tables" inherits="TablesNode"/>
	</nodes>
</metametadata-library>