<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:comm="http://www.ataccama.com/purity/comment"
	exclude-result-prefixes="sf fn comm">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>
<xsl:param name="settings" select="document('param:settings')/*"/>
<xsl:param name="preview" select="document('param:preview')/*"/>
<xsl:include href="incl_gen_plan_templates.xsl"/>
<xsl:include href="incl_plan_comments.xsl"/>
<xsl:include href="incl_constants.xsl"/>
<xsl:include href="_constants.xsl"/>

<xsl:template match="/*">

<purity-config xmlns:comm="http://www.ataccama.com/purity/comment" version="{$version}">

<modelComment bounds="0,0,937,60" borderColor="183,183,0" backgroundColor="255,255,180" foregroundColor="51,51,51">
<xsl:text>COMPLETE WHAT TO DO NEXT
</xsl:text>
<xsl:value-of select="@description|description"/>
</modelComment>

	<step id="in" className="com.ataccama.dqc.tasks.common.usersteps.io.InputStep" disabled="false" mode="NORMAL">
        <properties>
            <columns>
                <columnDef name="message" type="STRING"/>
                
                <xsl:for-each select="messageTransformer/headers/header">
                	<xsl:variable name="type">
  						<xsl:choose>
    						<xsl:when test="@header = 'JMSTimestamp'">
								<xsl:value-of select="'DATETIME'"/>
							</xsl:when>
							<xsl:when test="@header = 'JMSMessageID'">
								<xsl:value-of select="'STRING'"/>
							</xsl:when>
							<xsl:when test="@header = 'JMSPriority'">
								<xsl:value-of select="'INTEGER'"/>
							</xsl:when>
							<xsl:when test="@header = 'JMSCorrelationID'">
								<xsl:value-of select="'STRING'"/>
							</xsl:when>
							<xsl:when test="@header = 'JMSType'">
								<xsl:value-of select="'STRING'"/>
							</xsl:when>
    						<xsl:otherwise>
    							<xsl:value-of select="'STRING'"/>
    						</xsl:otherwise>
  						</xsl:choose>
					</xsl:variable>
                
                	<columnDef name="{@column}" type="{$type}"/>
                </xsl:for-each>	
                
                <xsl:for-each select="messageTransformer/properties/property">
                	<columnDef name="{@column}" type="{@type}"/>
                </xsl:for-each>
                <!-- 
                <xsl:variable name="custom_master_column">
	<xsl:for-each select="$logicalModel/masterModels/masterModel/masterTables/masterTable[lower-case(@instanceTable)=lower-case($table_name)]/advanced[@groupColumn != '']">
		<xsl:value-of select="lower-case(@groupColumn)"/><xsl:text> and </xsl:text>
	</xsl:for-each>
</xsl:variable>
                <columnDef name="{streaming/consumers/consumer/@name}" type="" /> -->
            </columns>
            <shadowColumns>
            </shadowColumns>
            <comm:comment>
            	<xsl:call-template name="generated_step"/>
            </comm:comment>
        </properties>
        <visual-constraints bounds="24,72,-1,-1" layout="vertical"/>
    </step>
    
    <xsl:if test="$settings/@enableRC='true'">    	  	       
	    <step id="counter_stream_in" className="com.ataccama.dqc.tasks.flow.RecordCounter" disabled="false" mode="NORMAL">
			<properties reportPerfPerBatch="true" append="true" reportFileName="\\logger" timeStampFormat="yyyy-MM-dd HH:mm:ss" batchSize="{$settings/@batchSizeRC}" reportPerfPerSecond="true">
			<comm:comment>
	        	<xsl:call-template name="generated_step"/>
	        </comm:comment>
	        </properties>
	      	<visual-constraints layout="vertical">
				<xsl:attribute name="bounds">
					<xsl:text>24,192,-1,-1</xsl:text>
				</xsl:attribute>
			</visual-constraints>	        
		</step>	
		<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
			<source step="in" endpoint="out"/>
			<target step="counter_stream_in" endpoint="in"/>
			<visual-constraints>
				<bendpoints/>
			</visual-constraints>
		</connection>		
    </xsl:if>
	
	<xsl:choose>
		<xsl:when test="processedEntities/@allEntities='true'">
			<xsl:for-each select="$logicalModel/instanceModel/tables/table">
				<xsl:call-template name="allEntities"/>
			</xsl:for-each>
		</xsl:when>
		<xsl:otherwise>			
			<xsl:apply-templates select="processedEntities/entities/entity"/>
		</xsl:otherwise>
	</xsl:choose>

</purity-config>
</xsl:template>

<xsl:template match="entity">
	<xsl:if test="$settings/@enableRC='true'"> 
		<step id="counter_{@name}_stream_out" className="com.ataccama.dqc.tasks.flow.RecordCounter" disabled="false" mode="NORMAL">
			<properties reportPerfPerBatch="true" append="true" reportFileName="\\logger" timeStampFormat="yyyy-MM-dd HH:mm:ss" batchSize="{$settings/@batchSizeRC}" reportPerfPerSecond="true">
				<comm:comment>
	       			<xsl:call-template name="generated_step"/>
	    		</comm:comment>
	    	</properties>
	    	<visual-constraints layout="vertical">
				<xsl:attribute name="bounds">
					<xsl:value-of select="((position()-1)*216)+24"/><xsl:text>,360,-1,-1</xsl:text>
				</xsl:attribute>
			</visual-constraints>	        
		</step>
		<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
			<source step="counter_{@name}_stream_out" endpoint="out"/>
			<target step="{@name}" endpoint="in"/>
			<visual-constraints>
				<bendpoints/>
			</visual-constraints>
		</connection>			
	</xsl:if>
	<step id="{@name}" className="com.ataccama.dqc.tasks.common.usersteps.io.OutputStep" disabled="false" mode="NORMAL">
        <properties>
            <requiredColumns>
            	<requiredColumn name="source_id" type="STRING"/>
                <requiredColumn name="origin" type="STRING"/>
                <requiredColumn name="source_timestamp" type="DATETIME" />
                <requiredColumn name="change_type" type="STRING" />
                <xsl:choose>
                	<xsl:when test="columns/column">
                		<xsl:variable name="partialColumns" select="columns/column"/>
                		<xsl:for-each select="$preview/databaseModel/instanceTables/physicalTable[@name=current()/@name]/columns/column[@origin='source' and @name=$partialColumns/@name]">
                			<xsl:call-template name="column_required"/>
                		</xsl:for-each>	
                	</xsl:when>
                	<xsl:otherwise>
                		<xsl:for-each select="$preview/databaseModel/instanceTables/physicalTable[@name=current()/@name]/columns/column[@origin='source']">
                			<xsl:call-template name="column_required"/>
                		</xsl:for-each>
                	</xsl:otherwise>
                </xsl:choose>                
            </requiredColumns>
            <comm:comment>
            	<xsl:call-template name="generated_step"/>
            </comm:comment>
        </properties>
        <visual-constraints layout="vertical">
			<xsl:attribute name="bounds">
				<xsl:choose>
					<xsl:when test="$settings/@enableRC='true'">
						<xsl:value-of select="((position()-1)*216)+24"/><xsl:text>,480,-1,-1</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="((position()-1)*216)+24"/><xsl:text>,360,-1,-1</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</visual-constraints>
    </step>
</xsl:template>

<xsl:template name="allEntities">
	<xsl:if test="$settings/@enableRC='true'"> 
		<step id="counter_{@name}_stream_out" className="com.ataccama.dqc.tasks.flow.RecordCounter" disabled="false" mode="NORMAL">
			<properties reportPerfPerBatch="true" append="true" reportFileName="\\logger" timeStampFormat="yyyy-MM-dd HH:mm:ss" batchSize="{$settings/@batchSizeRC}" reportPerfPerSecond="true">
				<comm:comment>
	       			<xsl:call-template name="generated_step"/>
	    		</comm:comment>
	    	</properties>
	    	<visual-constraints layout="vertical">
				<xsl:attribute name="bounds">
					<xsl:value-of select="((position()-1)*216)+24"/><xsl:text>,360,-1,-1</xsl:text>
				</xsl:attribute>
			</visual-constraints>	        
		</step>
		<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
			<source step="counter_{@name}_stream_out" endpoint="out"/>
			<target step="{@name}" endpoint="in"/>
			<visual-constraints>
				<bendpoints/>
			</visual-constraints>
		</connection>			
	</xsl:if>
	<step id="{@name}" className="com.ataccama.dqc.tasks.common.usersteps.io.OutputStep" disabled="false" mode="NORMAL">
        <properties>
            <requiredColumns>
            	<requiredColumn name="source_id" type="STRING"/>
                <requiredColumn name="origin" type="STRING"/>
                <requiredColumn name="source_timestamp" type="DATETIME" />
                <requiredColumn name="change_type" type="STRING" />
                <xsl:for-each select="$preview/databaseModel/instanceTables/physicalTable[@name=current()/@name]/columns/column[@origin='source']">
                	<xsl:call-template name="column_required"/>
                </xsl:for-each>
            </requiredColumns>
            <comm:comment>
            	<xsl:call-template name="generated_step"/>
            </comm:comment>
        </properties>
        <visual-constraints layout="vertical">
			<xsl:attribute name="bounds">
				<xsl:choose>
					<xsl:when test="$settings/@enableRC='true'">
						<xsl:value-of select="((position()-1)*216)+24"/><xsl:text>,480,-1,-1</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="((position()-1)*216)+24"/><xsl:text>,360,-1,-1</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</visual-constraints>
    </step>
</xsl:template>

</xsl:stylesheet>