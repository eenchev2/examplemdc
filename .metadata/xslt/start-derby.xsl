@echo off

set CATALINA_HOME=%DQC_HOME%\..\tomcat
set JAVA_OPTS=-Dderby.system.home="%DQC_HOME%/../derby"

call "%DQC_HOME%\bin\run_java.bat" org.apache.derby.iapi.tools.run server -noSecurityManager  start