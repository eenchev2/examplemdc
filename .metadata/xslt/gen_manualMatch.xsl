<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:comm="http://www.ataccama.com/purity/comment"
	exclude-result-prefixes="sf fn comm">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:param name="instances" select="document('param:instances')/*"/> <!--preview/databaseModel/instanceTables -->
<xsl:param name="tables" select="document('param:tables')/*"/> <!--logicalModel/instanceModel/tables -->
<xsl:param name="settings" select="document('param:settings')/*"/>

<xsl:include href="incl_constants.xsl"/>
<xsl:include href="incl_plan_comments.xsl"/>
<xsl:include href="_constants.xsl"/>

<xsl:template match="/*">

<purity-config xmlns:comm="http://www.ataccama.com/purity/comment" version="{$version}">
	<xsl:variable name="selectedEntities" select="selectedEntities"/>
	<xsl:variable name="selectedAllEntities" select="selectedAllEntities"/>
	
	<modelComment backgroundColor="255,255,180" borderColor="183,183,0" bounds="48,24,558,73" foregroundColor="51,51,51">Complete the plan to process the manual matching exceptions in bulk / batch mode for given matched entities. 
For more details on manual matching please see topic NME Manual Matching interfaces at Documentation portal.
	</modelComment>
	
	<xsl:choose>
		<!-- full -->
		<xsl:when test="local-name(.)='allmatchings'">	
			<xsl:if test="@allEntities='false'"> 		
				<xsl:for-each select="$tables/table[@name=$selectedAllEntities/selectedAllEntity/@name]">
					<xsl:call-template name="tableAll">
						<xsl:with-param name="multipleMatching" select="matchingTab/multipleMatching"/>
						<xsl:with-param name="defaultMatching" select="matchingTab/defaultMatchingDefinition"/>
						<xsl:with-param name="position" select="position()-1"/>
					</xsl:call-template>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="@allEntities='true'">
				<xsl:for-each select="$tables/table">
					<xsl:call-template name="tableAll">
						<xsl:with-param name="multipleMatching" select="matchingTab/multipleMatching"/>
						<xsl:with-param name="defaultMatching" select="matchingTab/defaultMatchingDefinition"/>
						<xsl:with-param name="position" select="position()-1"/>
					</xsl:call-template>
				</xsl:for-each>
			</xsl:if>
		</xsl:when>
		<!-- partial -->
		<xsl:otherwise>
			<xsl:variable name="matchingName" select="matchings/matching/@name"/>
			<xsl:if test="@allEntities='false'"> 		
				<xsl:for-each select="$tables/table[@name=$selectedEntities/selectedEntity/@name and (matchingTab/multipleMatching/matchingDefinitions/matchingDefinition/@name = $matchingName or matchingTab/defaultMatchingDefinition/@name = $matchingName)]">			
					<xsl:call-template name="table">
						<xsl:with-param name="matchingName" select="$matchingName"/>
						<xsl:with-param name="matchingDefinitions" select="matchingTab/multipleMatching/matchingDefinitions"/>
						<xsl:with-param name="defaultMatching" select="matchingTab/defaultMatchingDefinition"/>
						<xsl:with-param name="position" select="position()-1"/>
					</xsl:call-template>			
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="@allEntities='true'">
				<xsl:for-each select="$tables/table[matchingTab/multipleMatching/matchingDefinitions/matchingDefinition/@name = $matchingName or matchingTab/defaultMatchingDefinition/@name = $matchingName]">			
					<xsl:call-template name="table">
						<xsl:with-param name="matchingName" select="$matchingName"/>
						<xsl:with-param name="matchingDefinitions" select="matchingTab/multipleMatching/matchingDefinitions"/>
						<xsl:with-param name="defaultMatching" select="matchingTab/defaultMatchingDefinition"/>
						<xsl:with-param name="position" select="position()-1"/>
					</xsl:call-template>			
				</xsl:for-each>
			</xsl:if>
		</xsl:otherwise>
	</xsl:choose>
</purity-config>
</xsl:template>

<xsl:template name="tableAll">
 	<xsl:param name="position"/>
 	<xsl:param name="multipleMatching"/>
 	<xsl:param name="defaultMatching"/>
	<xsl:variable name="name" select="@name"/>
		
	<step id="{@name}" className="com.ataccama.dqc.tasks.common.usersteps.io.OutputStep" disabled="false" mode="NORMAL">
		<properties>
			<requiredColumns>
				<requiredColumn name="operation" type="STRING">
					<comm:comment>Possible values are: [MERGE_INSTANCE | SPLIT_ISOLATE | UN_ISOLATE | MERGE_MASTER]c</comm:comment>
				</requiredColumn>	
				<requiredColumn name="id" type="LONG">
					<comm:comment>Instance record id to be merged or split (required for MERGE_INSTANCE, SPLIT_ISOLATE)</comm:comment>
				</requiredColumn>
				<requiredColumn name="isolate_group_id" type="LONG">
					<comm:comment>To isolate several instance records together to the same master group, set the same value in this column (optional in SPLIT_ISOLATE)</comm:comment>
				</requiredColumn>

				<xsl:if test="$multipleMatching/@disableDefault='false'">
					<!-- default matching -->
					<xsl:for-each select="$defaultMatching">
						<requiredColumn name="{@masterIdColumn}" type="LONG">
							<comm:comment>Master id of instance records to be merged into another master group (required for MERGE_MASTER)
column name is configured in matchingOperation in model configuration file, default is master_id</comm:comment>
						</requiredColumn>
					</xsl:for-each>
					<xsl:for-each select="$defaultMatching">
						<requiredColumn name="target_{@masterIdColumn}" type="LONG">
							<comm:comment>Target master id to be set on selected instance records (required for MERGE_INSTANCE, MERGE_MASTER)
column name is prefix "target_" + name configured in matchingOperation in model configuration file, default is target_master_id</comm:comment>
						</requiredColumn>
					</xsl:for-each>						
				</xsl:if>	
					
				<!-- multiple matching -->				
				<xsl:for-each select="$multipleMatching/matchingDefinitions/matchingDefinition">
					<requiredColumn name="{@masterIdColumn}" type="LONG">
						<comm:comment>Master id of instance records to be merged into another master group (required for MERGE_MASTER)
column name is configured in matchingOperation in model configuration file, default is master_id</comm:comment>
					</requiredColumn>
				</xsl:for-each>
				<xsl:for-each select="$multipleMatching/matchingDefinitions/matchingDefinition">
						<requiredColumn name="target_{@masterIdColumn}" type="LONG">
							<comm:comment>Target master id to be set on selected instance records (required for MERGE_INSTANCE, MERGE_MASTER)
column name is prefix "target_" + name configured in matchingOperation in model configuration file, default is target_master_id</comm:comment>
						</requiredColumn>
				</xsl:for-each>	
				<requiredColumn name="rematch" type="BOOLEAN">
					<comm:comment>Set the flag to rematch a master group being unisolated (optional in UN_ISOLATE)</comm:comment>
				</requiredColumn>			
			</requiredColumns>
			<comm:comment>
           		<xsl:call-template name="generated_step"/>
           	</comm:comment>
		</properties>
		<visual-constraints layout="vertical">
			<xsl:attribute name="bounds"><xsl:value-of select="($position*168)+48"/><xsl:text>,392,-1,-1</xsl:text></xsl:attribute>
		</visual-constraints>
	</step>
	
	<xsl:if test="$settings/@enableRC='true'">    	  	       
	    <step id="counter_{$name}_manual_match_in" className="com.ataccama.dqc.tasks.flow.RecordCounter" disabled="false" mode="NORMAL">
			<properties reportPerfPerBatch="true" append="true" reportFileName="\\logger" timeStampFormat="yyyy-MM-dd HH:mm:ss" batchSize="{$settings/@batchSizeRC}" reportPerfPerSecond="true">
			<comm:comment>
	        	<xsl:call-template name="generated_step"/>
	        </comm:comment>
	        </properties>
	      	<visual-constraints layout="vertical">
				<xsl:attribute name="bounds"><xsl:value-of select="($position*168)+48"/><xsl:text>,312,-1,-1</xsl:text></xsl:attribute>
			</visual-constraints>	        
		</step>

		<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
			<source step="counter_{$name}_manual_match_in" endpoint="out"/>
			<target step="{$name}" endpoint="in"/>
			<visual-constraints>
				<bendpoints/>
			</visual-constraints>
		</connection>	
    </xsl:if>
</xsl:template>

<xsl:template name="table">
 	<xsl:param name="position"/>
 	<xsl:param name="matchingDefinitions"/>
 	<xsl:param name="defaultMatching"/>
 	<xsl:param name="matchingName"/>
	<xsl:variable name="name" select="@name"/>		
		
	<step id="{@name}" className="com.ataccama.dqc.tasks.common.usersteps.io.OutputStep" disabled="false" mode="NORMAL">
		<properties>
			<requiredColumns>
				<requiredColumn name="operation" type="STRING">
					<comm:comment>Possible values are: [MERGE_INSTANCE | SPLIT_ISOLATE | UN_ISOLATE | MERGE_MASTER]c</comm:comment>
				</requiredColumn>				
				<requiredColumn name="id" type="LONG">
					<comm:comment>Instance record id to be merged or split (required for MERGE_INSTANCE, SPLIT_ISOLATE)</comm:comment>
				</requiredColumn>				
				<requiredColumn name="isolate_group_id" type="LONG">
					<comm:comment>To isolate several instance records together to the same master group, set the same value in this column (optional in SPLIT_ISOLATE)</comm:comment>
				</requiredColumn>				

				<!-- default matching -->
				<xsl:for-each select="$defaultMatching[@name=$matchingName]">
					<requiredColumn name="{@masterIdColumn}" type="LONG">
						<comm:comment>Master id of instance records to be merged into another master group (required for MERGE_MASTER)
column name is configured in matchingOperation in model configuration file, default is master_id</comm:comment>
					</requiredColumn>
				</xsl:for-each>
				<xsl:for-each select="$defaultMatching[@name=$matchingName]">
					<requiredColumn name="target_{@masterIdColumn}" type="LONG">
						<comm:comment>Target master id to be set on selected instance records (required for MERGE_INSTANCE, MERGE_MASTER)
column name is prefix "target_" + name configured in matchingOperation in model configuration file, default is target_master_id</comm:comment>
					</requiredColumn>					
				</xsl:for-each>						
						
				<!-- multiple matching -->				
				<xsl:for-each select="$matchingDefinitions/matchingDefinition[@name=$matchingName]">
					<requiredColumn name="{@masterIdColumn}" type="LONG">
						<comm:comment>Master id of instance records to be merged into another master group (required for MERGE_MASTER)
column name is configured in matchingOperation in model configuration file, default is master_id</comm:comment>
					</requiredColumn>					
				</xsl:for-each>
				<xsl:for-each select="$matchingDefinitions/matchingDefinition[@name=$matchingName]">
					<requiredColumn name="target_{@masterIdColumn}" type="LONG">
						<comm:comment>Target master id to be set on selected instance records (required for MERGE_INSTANCE, MERGE_MASTER)
column name is prefix "target_" + name configured in matchingOperation in model configuration file, default is target_master_id</comm:comment>
					</requiredColumn>						
				</xsl:for-each>	
				<requiredColumn name="rematch" type="BOOLEAN">
					<comm:comment>Set the flag to rematch a master group being unisolated (optional in UN_ISOLATE)</comm:comment>
				</requiredColumn>						
			</requiredColumns>
			<comm:comment>
           		<xsl:call-template name="generated_step"/>
           	</comm:comment>
		</properties>
		<visual-constraints layout="vertical">
			<xsl:attribute name="bounds"><xsl:value-of select="($position*168)+48"/><xsl:text>,392,-1,-1</xsl:text></xsl:attribute>
		</visual-constraints>
	</step>
	
	<xsl:if test="$settings/@enableRC='true'">    	  	       
	    <step id="counter_{$name}_manual_match_in" className="com.ataccama.dqc.tasks.flow.RecordCounter" disabled="false" mode="NORMAL">
			<properties reportPerfPerBatch="true" append="true" reportFileName="\\logger" timeStampFormat="yyyy-MM-dd HH:mm:ss" batchSize="{$settings/@batchSizeRC}" reportPerfPerSecond="true">
			<comm:comment>
	        	<xsl:call-template name="generated_step"/>
	        </comm:comment>
	        </properties>
	      	<visual-constraints layout="vertical">
				<xsl:attribute name="bounds"><xsl:value-of select="($position*168)+48"/><xsl:text>,312,-1,-1</xsl:text></xsl:attribute>
			</visual-constraints>	        
		</step>

		<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
			<source step="counter_{$name}_manual_match_in" endpoint="out"/>
			<target step="{$name}" endpoint="in"/>
			<visual-constraints>
				<bendpoints/>
			</visual-constraints>
		</connection>	
    </xsl:if>		
	
</xsl:template>

</xsl:stylesheet>