<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:comm="http://www.ataccama.com/purity/comment"
	exclude-result-prefixes="sf fn comm">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>
<xsl:param name="settings" select="document('param:settings')/*"/>
<xsl:include href="incl_gen_plan_templates.xsl"/>
<xsl:include href="incl_plan_comments.xsl"/>
<xsl:include href="incl_constants.xsl"/>
<xsl:include href="_constants.xsl"/>
<xsl:param name="master" select="document('param:master')/*"/>
<xsl:param name="root" select="document('param:root')/*"/>


<!-- bound to /preview/databaseModel/instanceTables/*[@name=$current/@instanceEntity] -->
<xsl:template match="/*">
<xsl:variable name="table_name" select="@name"/>
<xsl:variable name="master_layer" select="$master/@layerName"/>
<xsl:variable name="record_counter_name_in" select="concat('counter_',$master_layer,'_',$root/@entity,'_merge_in')"/>
<xsl:variable name="record_counter_name_out" select="concat('counter_',$master_layer,'_',$root/@entity,'_merge_out')"/>
<purity-config xmlns:comm="http://www.ataccama.com/purity/comment" version="{$version}">

<!-- <modelComment bounds="68,20,926,125" borderColor="183,183,0" backgroundColor="255,255,180" foregroundColor="51,51,51">
	<xsl:call-template name="merging_plan_comment">
		<xsl:with-param name="master_column" select="$master/advanced/@groupColumn"/>
	</xsl:call-template>
</modelComment> -->

	<step id="in" className="com.ataccama.dqc.tasks.common.usersteps.io.InputStep" disabled="false" mode="NORMAL">
        <properties>
          	<columns>
                <xsl:for-each select="$master//columns/column[@origin='internal' or @origin='merge']">
                	<xsl:choose>
                		<xsl:when test="@type='long_int'"><columnDef name="{lower-case(@name)}" type="LONG"/></xsl:when>
                		<xsl:otherwise><columnDef name="{lower-case(@name)}" type="{upper-case(@type)}"/></xsl:otherwise>
              		</xsl:choose>
                </xsl:for-each>
            </columns>
            <shadowColumns>
				<xsl:for-each select="$master//columns/column[@origin='validate']">
                	<xsl:call-template name="column_shadow"/>
                </xsl:for-each>
            </shadowColumns>
            <comm:comment>
            	<xsl:call-template name="generated_step"/>
            </comm:comment>
        </properties>
        <visual-constraints bounds="216,192,-1,-1" layout="vertical"/>
    </step>

    <xsl:if test="$settings/@enableRC='false'">
    	<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
        	<source step="in" endpoint="out"/>
        	<target step="out" endpoint="in"/>
    	</connection>
    </xsl:if>
    
    <xsl:if test="$settings/@enableRC='true'">	
		<step id="{$record_counter_name_in}" className="com.ataccama.dqc.tasks.flow.RecordCounter" disabled="false" mode="NORMAL">
			<properties reportPerfPerBatch="true" append="true" reportFileName="\\logger" timeStampFormat="yyyy-MM-dd HH:mm:ss" batchSize="{$settings/@batchSizeRC}" reportPerfPerSecond="true">
			<comm:comment>
	        	<xsl:call-template name="generated_step"/>
	        </comm:comment>
	        </properties>
	        <visual-constraints bounds="216,264,-1,-1" layout="vertical"/>
		</step>
		
		<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
	     	<source step="in" endpoint="out"/>
	        <target step="{$record_counter_name_in}" endpoint="in"/>
	    </connection>
	    
	    
	    <step id="{$record_counter_name_out}" className="com.ataccama.dqc.tasks.flow.RecordCounter" disabled="false" mode="NORMAL">
			<properties reportPerfPerBatch="true" append="true" reportFileName="\\logger" timeStampFormat="yyyy-MM-dd HH:mm:ss" batchSize="{$settings/@batchSizeRC}" reportPerfPerSecond="true">
			<comm:comment>
	        	<xsl:call-template name="generated_step"/>
	        </comm:comment>
	        </properties>
	        <visual-constraints bounds="216,384,-1,-1" layout="vertical"/>
		</step>		
		
		<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
	        <source step="{$record_counter_name_in}" endpoint="out"/>
	        <target step="{$record_counter_name_out}" endpoint="in"/>
	    </connection>
		
		<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
	        <source step="{$record_counter_name_out}" endpoint="out"/>
	        <target step="out" endpoint="in"/>
	    </connection>
    </xsl:if>

	<step id="out" className="com.ataccama.dqc.tasks.common.usersteps.io.OutputStep" disabled="false" mode="NORMAL">
        <properties>
            <requiredColumns>
                <xsl:for-each select="$master//columns/column[@origin='validate']">
                	<xsl:choose>
                		<xsl:when test="@type='long_int'"><requiredColumn name="{lower-case(@name)}" type="LONG"/></xsl:when>
                		<xsl:otherwise><requiredColumn name="{lower-case(@name)}" type="{upper-case(@type)}"/></xsl:otherwise>
              		</xsl:choose>                               	
                </xsl:for-each>
            </requiredColumns>
            <comm:comment>
            	<xsl:call-template name="generated_step"/>
            </comm:comment>
        </properties>
        <visual-constraints bounds="216,456,-1,-1" layout="vertical"/>
    </step>
</purity-config>
</xsl:template>

</xsl:stylesheet>