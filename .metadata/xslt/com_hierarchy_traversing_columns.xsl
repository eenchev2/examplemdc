<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>
<xsl:param name="traversingPlanPublisher" select="document('param:traversingPlanPublisher')/*"/>
<xsl:param name="databaseModel" select="document('param:databaseModel')/*"/>
	
	<xsl:template match="/*">
		<xsl:variable name="rootEntity" select="$traversingPlanPublisher/@rootEntity"/>
		<xsl:variable name="rootLayerName" select="substring-before(fn:substring-after($rootEntity,'('),')')"/>
		<xsl:variable name="rootEntityName" select="fn:substring-before($rootEntity,' ')"/>
			
		<ccColumns>
			<xsl:choose>
				<xsl:when test="not(local-name()='traversingPlanPublisher')">
					<xsl:variable name="entityName" select="fn:substring-before(@name,' ')"/>			
					<xsl:variable name="layerName" select="substring-before(fn:substring-after(@name,'('),')')"/>						
					<xsl:variable name="instanceColumns" select="$databaseModel/instanceTables/physicalTable[@name=$entityName and @layerName=$layerName]/columns/column"/>		
					<xsl:variable name="masterColumns" select="$databaseModel/masterTables/physicalTable[@name=$entityName and @layerName=$layerName]/columns/column"/>					
					<!-- <xsl:variable name="masterVirtualChilds" select="$logicalModel/masterModels/masterModel[@name=$layerName]/instanceTables/instanceTable[@name=$logicalModel/masterModels/masterModel[@name=$layerName]/relationships/relationship[@parentTable=$entityName]/@childTable]"/> -->
					<xsl:for-each select="$instanceColumns">
						<xsl:if test="not(@name='id') and not(substring(@name,1,4)='eng_')">
							<ccColumn name="{@name}"/>
						</xsl:if>
					</xsl:for-each>			
					<xsl:for-each select="$masterColumns">
						<xsl:if test="not(@name='id') and not(substring(@name,1,4)='eng_')">
							<ccColumn name="{@name}"/>
						</xsl:if>
					</xsl:for-each>	
<!-- 					<xsl:for-each select="$masterVirtualChilds">
						<child name="{@name}"/>
					</xsl:for-each> -->
				</xsl:when>		
				<xsl:when test="local-name()='traversingPlanPublisher'">
					<xsl:variable name="rootInstanceColumns" select="$databaseModel/instanceTables/physicalTable[@name=$rootEntityName and @layerName=$rootLayerName]/columns/column"/>		
					<xsl:variable name="rootMasterColumns" select="$databaseModel/masterTables/physicalTable[@name=$rootEntityName and @layerName=$rootLayerName]/columns/column"/>
					<!-- <xsl:variable name="rootMasterVirtualChilds" select="$logicalModel/masterModels/masterModel[@name=$layerName]/instanceTables/instanceTable[@name=$logicalModel/masterModels/masterModel[@name=$layerName]/relationships/relationship[@parentTable=$rootEntityName]/@childTable]"/> -->			
					<xsl:choose>	
						<xsl:when test="$rootLayerName='instance'">
							<xsl:for-each select="$rootInstanceColumns">
								<xsl:if test="not(@name='id') and not(substring(@name,1,4)='eng_')">
									<ccColumn name="{@name}"/>
								</xsl:if>
							</xsl:for-each>		
						</xsl:when>	
						<xsl:otherwise>
							<xsl:for-each select="$rootMasterColumns">
								<xsl:if test="not(@name='id') and not(substring(@name,1,4)='eng_')">
									<ccColumn name="{@name}"/>
								</xsl:if>
							</xsl:for-each>	
<!-- 							<xsl:for-each select="$rootMasterVirtualChilds">
								<child name="{@name}"/>
							</xsl:for-each> -->
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
			</xsl:choose>								
		</ccColumns>	
		
	</xsl:template>

</xsl:stylesheet>