<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" cdata-section-elements="sql"/>
	<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>

	<xsl:template match="/*">
		<searchDefinitios>
			<xsl:if test="filters/filter[@enable='true' and substring-before(substring-after(@entity,'('),')')='instance']">
				<instanceLayer>
					<entities>
						<xsl:for-each select="filters/filter[@enable='true' and substring-before(substring-after(@entity,'('),')')='instance']">
				            <entity name="{substring-before(@entity,' ')}">
				                <filters>
				                    <filter name="{@name}" joinByOr="{@joinByOr}">
				                        <columns>
				                        	<xsl:for-each select="columns/column">
				                            	<column name="{@name}">
				                            		<xsl:attribute name="operator">
				                            			<xsl:choose>
															<xsl:when test="@operator = '='">EQ</xsl:when>
															<xsl:when test="@operator = '&gt;'">GT</xsl:when>
															<xsl:when test="@operator = '&lt;'">LT</xsl:when>
															<xsl:when test="@operator = '&gt;='">GE</xsl:when>
															<xsl:when test="@operator = '&lt;='">LE</xsl:when>
															<xsl:when test="@operator = '&lt;&gt;'">NE</xsl:when>
															<xsl:otherwise><xsl:value-of select="@operator"/></xsl:otherwise>
														</xsl:choose>
				                            		</xsl:attribute>
				                            		<xsl:if test="@caseSensitive='true'">
				                            			<xsl:attribute name="caseSensitive">
				                            				<xsl:value-of select="@caseSensitive"/>
				                            			</xsl:attribute>
				                            		</xsl:if>
				                            	</column>
				                            </xsl:for-each>
				                        </columns>
				                    </filter>
				                </filters>
				            </entity>
						</xsl:for-each>
					</entities> 
				</instanceLayer>	
			</xsl:if>	
			<xsl:if test="filters/filter[@enable='true' and (substring-before(substring-after(@entity,'('),')')!='instance' or substring-before(substring-after(@entity,'('),')')!='dataset' or substring-before(substring-after(@entity,'('),')')!='sor')]">
			    <xsl:variable name="filters" select="filters"/>
			    <masterLayers>
			    	<xsl:for-each select="$logicalModel/masterModels/masterModel">
			    		<xsl:variable name="layerName" select="@name"/>
				    	<xsl:for-each select="$filters/filter[@enable='true' and substring-before(substring-after(@entity,'('),')')=$layerName]">
					        <masterLayer name="{$layerName}">
					            <entities>
					                <entity name="{substring-before(@entity,' ')}">
					                    <filters>
					                        <filter name="{@name}" joinByOr="{@joinByOr}">
					                            <columns>
					                            	<xsl:for-each select="columns/column">
					                                	<column name="{@name}">
						                            		<xsl:attribute name="operator">
						                            			<xsl:choose>
																	<xsl:when test="@operator = '='">EQ</xsl:when>
																	<xsl:when test="@operator = '&gt;'">GT</xsl:when>
																	<xsl:when test="@operator = '&lt;'">LT</xsl:when>
																	<xsl:when test="@operator = '&gt;='">GE</xsl:when>
																	<xsl:when test="@operator = '&lt;='">LE</xsl:when>
																	<xsl:when test="@operator = '&lt;&gt;'">NE</xsl:when>
																	<xsl:otherwise><xsl:value-of select="@operator"/></xsl:otherwise>
																</xsl:choose>
						                            		</xsl:attribute>					                                	
						                            		<xsl:if test="@caseSensitive='true'">
						                            			<xsl:attribute name="caseSensitive">
						                            				<xsl:value-of select="@caseSensitive"/>
						                            			</xsl:attribute>
						                            		</xsl:if>					                                	
					                                	</column>
					                                </xsl:for-each>
					                            </columns>
					                        </filter>
					                    </filters>
					                </entity>
					            </entities>
					        </masterLayer>
						</xsl:for-each>
					</xsl:for-each>
			    </masterLayers>
			</xsl:if>					   
			<xsl:if test="filters/filter[@enable='true' and substring-before(substring-after(@entity,'('),')')='sor']">
				<sorLayer>
					<entities>
						<xsl:for-each select="filters/filter[@enable='true' and substring-before(substring-after(@entity,'('),')')='sor']">
				            <entity name="{substring-before(@entity,' ')}">
				                <filters>
				                    <filter name="{@name}" joinByOr="{@joinByOr}">
				                        <columns>
				                        	<xsl:for-each select="columns/column">
				                            	<column name="{@name}">
				                            		<xsl:attribute name="operator">
				                            			<xsl:choose>
															<xsl:when test="@operator = '='">EQ</xsl:when>
															<xsl:when test="@operator = '&gt;'">GT</xsl:when>
															<xsl:when test="@operator = '&lt;'">LT</xsl:when>
															<xsl:when test="@operator = '&gt;='">GE</xsl:when>
															<xsl:when test="@operator = '&lt;='">LE</xsl:when>
															<xsl:when test="@operator = '&lt;&gt;'">NE</xsl:when>
															<xsl:otherwise><xsl:value-of select="@operator"/></xsl:otherwise>
														</xsl:choose>
				                            		</xsl:attribute>				                            	
				                            		<xsl:if test="@caseSensitive='true'">
				                            			<xsl:attribute name="caseSensitive">
				                            				<xsl:value-of select="@caseSensitive"/>
				                            			</xsl:attribute>
				                            		</xsl:if>
				                            	</column>
				                            </xsl:for-each>
				                        </columns>
				                    </filter>
				                </filters>
				            </entity>
						</xsl:for-each>
					</entities> 
				</sorLayer>	
			</xsl:if>
			<xsl:if test="filters/filter[@enable='true' and substring-before(substring-after(@entity,'('),')')='dataset']">
				<dataSetLayer>
					<entities>
						<xsl:for-each select="filters/filter[@enable='true' and substring-before(substring-after(@entity,'('),')')='dataset']">
				            <entity name="{substring-before(@entity,' ')}">
				                <filters>
				                    <filter name="{@name}" joinByOr="{@joinByOr}">
				                        <columns>
				                        	<xsl:for-each select="columns/column">
				                            	<column name="{@name}">
				                            		<xsl:attribute name="operator">
				                            			<xsl:choose>
															<xsl:when test="@operator = '='">EQ</xsl:when>
															<xsl:when test="@operator = '&gt;'">GT</xsl:when>
															<xsl:when test="@operator = '&lt;'">LT</xsl:when>
															<xsl:when test="@operator = '&gt;='">GE</xsl:when>
															<xsl:when test="@operator = '&lt;='">LE</xsl:when>
															<xsl:when test="@operator = '&lt;&gt;'">NE</xsl:when>
															<xsl:otherwise><xsl:value-of select="@operator"/></xsl:otherwise>
														</xsl:choose>
				                            		</xsl:attribute>				                            	
				                            		<xsl:if test="@caseSensitive='true'">
				                            			<xsl:attribute name="caseSensitive">
				                            				<xsl:value-of select="@caseSensitive"/>
				                            			</xsl:attribute>
				                            		</xsl:if>				                            	
				                            	</column>
				                            </xsl:for-each>
				                        </columns>
				                    </filter>
				                </filters>
				            </entity>
						</xsl:for-each>
					</entities> 
				</dataSetLayer>	
			</xsl:if>					    
		</searchDefinitios>	
	</xsl:template>
</xsl:stylesheet>