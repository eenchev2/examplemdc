<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="instanceTable" select="document('param:instanceTable')/*"/>
	<xsl:param name="masterTable" select="document('param:masterTable')/*"/>
	<xsl:param name="sorTable" select="document('param:sorTable')/*"/>

 <xsl:template match="/*">
	<xsl:if test="$instanceTable">
		<cc>
			<xsl:attribute name="name">
				<xsl:value-of select="$instanceTable/@entityName"/>
				<xsl:value-of select="'_instance_'"/>
				<xsl:choose>
					<xsl:when test="substring-before(@name,'_')=&#39;eng&#39;">
						<xsl:if test="@name='eng_source_timestamp'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_stst'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>
						<xsl:if test="@name='eng_source_system'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_system'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_last_update_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_mtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_last_source_update_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_smtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>
						<xsl:if test="@name='eng_creation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_ctdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_deletion_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_dtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_activation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_atdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_deactivation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_itdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>																																																																																				
					</xsl:when>
					<xsl:otherwise>					
						<xsl:value-of select="@name"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="engName">
				<xsl:choose>
					<xsl:when test="substring-before(@name,'_')=&#39;eng&#39;">
						<xsl:if test="@name='eng_source_timestamp'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_stst'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>
						<xsl:if test="@name='eng_source_system'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_system'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_last_update_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_mtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_last_source_update_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_smtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>
						<xsl:if test="@name='eng_creation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_ctdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_deletion_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_dtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_activation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_atdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_deactivation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_itdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>																																																																																				
					</xsl:when>
					<xsl:otherwise>					
						<xsl:value-of select="''"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>			
		</cc>
	</xsl:if>
	<xsl:if test="$masterTable">
		<cc>
			<xsl:attribute name="name">
				<xsl:value-of select="$masterTable/cc/@entity"/>
				<xsl:value-of select="'_'"/>
				<xsl:value-of select="$masterTable/cc/@layer"/>
				<xsl:value-of select="'_'"/>
				<xsl:choose>
					<xsl:when test="substring-before(@name,'_')=&#39;eng&#39;">
						<xsl:if test="@name='eng_source_timestamp'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_stst'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>
						<xsl:if test="@name='eng_source_system'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_system'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_last_update_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_mtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_last_source_update_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_smtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>
						<xsl:if test="@name='eng_creation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_ctdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_deletion_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_dtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_activation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_atdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_deactivation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_itdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>																																																																																				
					</xsl:when>
					<xsl:otherwise>					
						<xsl:value-of select="@name"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="engName">
				<xsl:choose>
					<xsl:when test="substring-before(@name,'_')=&#39;eng&#39;">
						<xsl:if test="@name='eng_source_timestamp'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_stst'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>
						<xsl:if test="@name='eng_source_system'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_system'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_last_update_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_mtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_last_source_update_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_smtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>
						<xsl:if test="@name='eng_creation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_ctdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_deletion_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_dtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_activation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_atdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_deactivation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_itdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>																																																																																				
					</xsl:when>
					<xsl:otherwise>					
						<xsl:value-of select="''"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>				
		</cc>
	</xsl:if>
	<xsl:if test="$sorTable">
		<cc>
			<xsl:attribute name="name">
				<xsl:value-of select="$sorTable/@entityName"/>
				<xsl:value-of select="'_sor_'"/>
				<xsl:choose>
					<xsl:when test="substring-before(@name,'_')=&#39;eng&#39;">
						<xsl:if test="@name='eng_source_timestamp'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_stst'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>
						<xsl:if test="@name='eng_source_system'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_system'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_last_update_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_mtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_last_source_update_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_smtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>
						<xsl:if test="@name='eng_creation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_ctdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_deletion_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_dtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_activation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_atdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_deactivation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_itdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>																																																																																				
					</xsl:when>
					<xsl:otherwise>					
						<xsl:value-of select="@name"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="engName">
				<xsl:choose>
					<xsl:when test="substring-before(@name,'_')=&#39;eng&#39;">
						<xsl:if test="@name='eng_source_timestamp'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_stst'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>
						<xsl:if test="@name='eng_source_system'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_system'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_last_update_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_mtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_last_source_update_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_smtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>
						<xsl:if test="@name='eng_creation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_ctdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_deletion_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_dtdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_activation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_atdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>	
						<xsl:if test="@name='eng_deactivation_date'">
							<xsl:variable name="columnName">
								<xsl:value-of select="'eng_itdt'"/>
							</xsl:variable>													
							<xsl:value-of select="$columnName"/>
						</xsl:if>																																																																																				
					</xsl:when>
					<xsl:otherwise>					
						<xsl:value-of select="''"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>				
		</cc>
	</xsl:if>	
 </xsl:template>

</xsl:stylesheet>

