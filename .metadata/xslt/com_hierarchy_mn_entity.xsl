<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>
<xsl:param name="layerType" select="document('param:layerType')/*"/>
	
	<xsl:template match="/*">
		<guiHierarchyMNEntities>
			<xsl:choose>
				<xsl:when test="$layerType/name()='guiHierarchiesInstance'">
					<xsl:for-each select="$logicalModel/instanceModel/tables/table">	
						<xsl:variable name="tableName" select="@name"/>
						<xsl:if test="count($logicalModel/instanceModel/relationships/relationship[@childTable=$tableName])>1">
							<guiHierarchyMNEntity entity="{$tableName}">
								<xsl:attribute name="entity">
									<xsl:value-of select="$tableName"/>
								</xsl:attribute>
							</guiHierarchyMNEntity>
						</xsl:if>
					</xsl:for-each>
				</xsl:when>
				<xsl:when test="$layerType/name()='guiHierarchiesSoR'">
					<xsl:for-each select="$logicalModel/sorModel/tables/table">	
						<xsl:variable name="tableName" select="@name"/>
						<xsl:if test="count($logicalModel/sorModel/relationships/relationship[@childTable=$tableName])>1">
							<guiHierarchyMNEntity entity="{$tableName}">
								<xsl:attribute name="entity">
									<xsl:value-of select="$tableName"/>
								</xsl:attribute>
							</guiHierarchyMNEntity>
						</xsl:if>
					</xsl:for-each>
				</xsl:when>			
				<xsl:otherwise>
					<xsl:for-each select="$logicalModel/masterModels/masterModel[@name=$layerType/@layerName]/masterTables/masterTable">	
						<xsl:variable name="tableName" select="@name"/>
						<xsl:if test="count($logicalModel/masterModels/masterModel[@name=$layerType/@layerName]/relationships/relationship[@childTable=$tableName])>1">
							<guiHierarchyMNEntity entity="{$tableName}"/>
						</xsl:if>
					</xsl:for-each>
					<xsl:for-each select="$logicalModel/masterModels/masterModel[@name=$layerType/@layerName]/instanceTables/instanceTable">	
						<xsl:variable name="tableName" select="@name"/>
						<xsl:if test="count($logicalModel/masterModels/masterModel[@name=$layerType/@layerName]/relationships/relationship[@childTable=$tableName])>1">
							<guiHierarchyMNEntity entity="{$tableName}"/>
						</xsl:if>
					</xsl:for-each>				
				</xsl:otherwise>
			</xsl:choose>
		</guiHierarchyMNEntities>
	</xsl:template>

</xsl:stylesheet>