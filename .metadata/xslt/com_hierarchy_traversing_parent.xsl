<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>
<xsl:param name="traversingPlanPublisher" select="document('param:traversingPlanPublisher')/*"/>
<xsl:param name="handler" select="document('param:handler')/*"/>
	
	<xsl:template match="/*">
		<xsl:variable name="rootEntity" select="$traversingPlanPublisher/@rootEntity"/>
		<xsl:variable name="layerName" select="substring-before(fn:substring-after($rootEntity,'('),')')"/>
		<xsl:variable name="rootEntityName" select="fn:substring-before($rootEntity,' ')"/>
			
		<childs>
			<xsl:choose>
				<xsl:when test="not(local-name()='traversingPlanPublisher')">
					<xsl:variable name="entityName" select="fn:substring-before(@name,' ')"/>				
					<xsl:variable name="instanceChilds" select="$logicalModel/instanceModel/tables/table[@name=$logicalModel/instanceModel/relationships/relationship[@parentTable=$entityName]/@childTable]"/>		
					<xsl:variable name="masterChilds" select="$logicalModel/masterModels/masterModel[@name=$layerName]/masterTables/masterTable[@name=$logicalModel/masterModels/masterModel[@name=$layerName]/relationships/relationship[@parentTable=$entityName]/@childTable]"/>
					<xsl:variable name="masterVirtualChilds" select="$logicalModel/masterModels/masterModel[@name=$layerName]/instanceTables/instanceTable[@name=$logicalModel/masterModels/masterModel[@name=$layerName]/relationships/relationship[@parentTable=$entityName]/@childTable]"/>
					<xsl:variable name="parentLayer" select="substring-before(fn:substring-after(@name,'('),')')"/>
					<xsl:choose>
						<xsl:when test="$parentLayer='instance'">
							<xsl:for-each select="$instanceChilds[@name=$handler/filter/entities/entity[cc/@layerName='instance']/cc/@entityName and count($handler/filter/entities/entity)&gt;0]">
								<child name="{@name} (instance)"/>
							</xsl:for-each>
							<xsl:for-each select="$instanceChilds[count($handler/filter/entities/entity)=0]">
								<child name="{@name} (instance)"/>
							</xsl:for-each>											
						</xsl:when>
						<xsl:otherwise>
							<xsl:for-each select="$instanceChilds[@name=$handler/filter/entities/entity[cc/@layerName='instance']/cc/@entityName and count($handler/filter/entities/entity)&gt;0]">
								<child name="{@name} (instance)"/>
							</xsl:for-each>	
							<xsl:for-each select="$instanceChilds[count($handler/filter/entities/entity)=0]">
								<child name="{@name} (instance)"/>
							</xsl:for-each>														
							<xsl:for-each select="$masterChilds[@name=$handler/filter/entities/entity[cc/@layerName=$layerName]/cc/@entityName and count($handler/filter/entities/entity)&gt;0]">
								<child name="{@name} ({$layerName})"/>
							</xsl:for-each>	
							<xsl:for-each select="$masterChilds[count($handler/filter/entities/entity)=0]">
								<child name="{@name} ({$layerName})"/>
							</xsl:for-each>								
							<xsl:for-each select="$masterVirtualChilds[@name=$handler/filter/entities/entity[cc/@layerName=$layerName]/cc/@entityName and count($handler/filter/entities/entity)&gt;0]">
								<child name="{@name} ({$layerName})"/>
							</xsl:for-each>
							<xsl:for-each select="$masterVirtualChilds[count($handler/filter/entities/entity)=0]">
								<child name="{@name} ({$layerName})"/>
							</xsl:for-each>							
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>		
				<xsl:when test="local-name()='traversingPlanPublisher'">
					<xsl:variable name="rootInstanceChilds" select="$logicalModel/instanceModel/tables/table[@name=$logicalModel/instanceModel/relationships/relationship[@parentTable=$rootEntityName]/@childTable]"/>		
					<xsl:variable name="rootMasterChilds" select="$logicalModel/masterModels/masterModel[@name=$layerName]/masterTables/masterTable[@name=$logicalModel/masterModels/masterModel[@name=$layerName]/relationships/relationship[@parentTable=$rootEntityName]/@childTable]"/>
					<xsl:variable name="rootMasterVirtualChilds" select="$logicalModel/masterModels/masterModel[@name=$layerName]/instanceTables/instanceTable[@name=$logicalModel/masterModels/masterModel[@name=$layerName]/relationships/relationship[@parentTable=$rootEntityName]/@childTable]"/>
					<xsl:variable name="rootMasterInstanceChilds" select="$logicalModel/instanceModel/tables/table[@name=$logicalModel/masterModels/masterModel[@name=$layerName]/masterTables/masterTable[@name=$rootEntityName]/@instanceTable]"/>			
					<xsl:choose>	
						<xsl:when test="$layerName='instance'">
							<xsl:for-each select="$rootInstanceChilds[count($handler/filter/entities/entity)=0]">
								<child name="{@name}"/>
							</xsl:for-each>
							<xsl:for-each select="$rootInstanceChilds[@name=$handler/filter/entities/entity[cc/@layerName='instance']/cc/@entityName and count($handler/filter/entities/entity)&gt;0]">
								<child name="{@name}"/>
							</xsl:for-each>									
						</xsl:when>	
						<xsl:otherwise>
							<xsl:for-each select="$rootMasterChilds[count($handler/filter/entities/entity)=0]">
								<child name="{@name} ({$layerName})"/>
							</xsl:for-each>
							<xsl:for-each select="$rootMasterChilds[@name=$handler/filter/entities/entity[cc/@layerName=$layerName]/cc/@entityName and count($handler/filter/entities/entity)&gt;0]">
								<child name="{@name} ({$layerName})"/>
							</xsl:for-each>								
							<xsl:for-each select="$rootMasterVirtualChilds[count($handler/filter/entities/entity)=0]">
								<child name="{@name} ({$layerName})"/>
							</xsl:for-each>
							<xsl:for-each select="$rootMasterVirtualChilds[@name=$handler/filter/entities/entity[cc/@layerName=$layerName]/cc/@entityName and count($handler/filter/entities/entity)&gt;0]">
								<child name="{@name} ({$layerName})"/>
							</xsl:for-each>							
							<xsl:for-each select="$rootMasterInstanceChilds[count($handler/filter/entities/entity)=0]">
								<child name="{@name} (instance)"/>
							</xsl:for-each>
							<xsl:for-each select="$rootMasterInstanceChilds[@name=$handler/filter/entities/entity[cc/@layerName='instance']/cc/@entityName and count($handler/filter/entities/entity)&gt;0]">
								<child name="{@name} (instance)"/>
							</xsl:for-each>														
						</xsl:otherwise>
					</xsl:choose>	
				</xsl:when>
			</xsl:choose>										
		</childs>	
		
	</xsl:template>

</xsl:stylesheet>