<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>
	
	<xsl:template match="/*">
		<ccMatchingProposals>
			<xsl:for-each select="$logicalModel/instanceModel/tables/table[matchingTab/defaultMatchingDefinition/@name!='' or matchingTab/multipleMatching/matchingDefinitions/matchingDefinition]">
				<xsl:variable name="tableName" select="@name"/>
				<xsl:for-each select="$logicalModel/instanceModel/tables/table[@name=$tableName]/matchingTab/defaultMatchingDefinition[@name!='']">
					<ccMatchingProposal>
						<xsl:attribute name="name">
							<xsl:value-of select="$tableName"/>
							<xsl:value-of select="' ('"/>
							<xsl:value-of select="@name"/>
							<xsl:value-of select="')'"/>
						</xsl:attribute>
						<xsl:attribute name="filterName">
							<xsl:value-of select="$tableName"/>
							<xsl:value-of select="'_proposal_'"/>
							<xsl:value-of select="@name"/>
						</xsl:attribute>				
						<xsl:attribute name="filterLayer">
							<xsl:value-of select="'match_proposal'"/>
						</xsl:attribute>												
					</ccMatchingProposal>
				</xsl:for-each>
				<xsl:for-each select="$logicalModel/instanceModel/tables/table[@name=$tableName]/matchingTab/multipleMatching/matchingDefinitions/matchingDefinition">
					<ccMatchingProposal>
						<xsl:attribute name="name">
							<xsl:value-of select="$tableName"/>
							<xsl:value-of select="' ('"/>
							<xsl:value-of select="@name"/>
							<xsl:value-of select="')'"/>
						</xsl:attribute>
						<xsl:attribute name="filterName">
							<xsl:value-of select="$tableName"/>
							<xsl:value-of select="'_proposal_'"/>
							<xsl:value-of select="@name"/>
						</xsl:attribute>				
						<xsl:attribute name="filterLayer">
							<xsl:value-of select="'match_proposal'"/>
						</xsl:attribute>													
					</ccMatchingProposal>
				</xsl:for-each>				
			</xsl:for-each>
		</ccMatchingProposals>
	</xsl:template>

</xsl:stylesheet>