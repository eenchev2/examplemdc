<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:comm="http://www.ataccama.com/purity/comment"
	exclude-result-prefixes="sf fn comm">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
<xsl:param name="databaseModel" select="document('param:databaseModel')/*"/>
<xsl:param name="historyPlugin" select="document('param:historyPlugin')/*"/>
<xsl:include href="incl_constants.xsl"/>
<xsl:include href="_constants.xsl"/>
<xsl:include href="incl_plan_comments.xsl"/>

<!--bound to /outputOperations/exportModel/*  -->
<xsl:template match="/*">
	<purity-config xmlns:comm="http://www.ataccama.com/purity/comment" version="{$version}">
		<xsl:choose>
			<xsl:when test="local-name(.)='fullSorExport'">						
				<xsl:call-template name="sorSteps" >
					<xsl:with-param name="mode" select="'full'"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</purity-config>
</xsl:template>

<xsl:template name="sorSteps">
	<xsl:param name="mode"/>
	<xsl:variable name="enableEngTid"><xsl:value-of select="advanced/@enableTID"/></xsl:variable>
	
	<xsl:if test="@allEntities='true'">
		<xsl:apply-templates select="$databaseModel/sorTables/physicalTable">
			<xsl:with-param name="allCols" select="'true'"/>
			<xsl:with-param name="mode" select="$mode"/>
			<xsl:with-param name="enableEngTid" select="$enableEngTid"/>
			<xsl:with-param name="selectedTables" select="current()/selectedTables"/>
		</xsl:apply-templates>
	</xsl:if>
	<xsl:if test="@allEntities='false'">
		<xsl:apply-templates select="$databaseModel/sorTables/physicalTable[@name=current()/selectedSoRTables/selectedSoRTable/@name]">
			<xsl:with-param name="selectedTables" select="current()/selectedSoRTables"/>
			<xsl:with-param name="mode" select="$mode"/>
			<xsl:with-param name="enableEngTid" select="$enableEngTid"/>			
		</xsl:apply-templates>
	</xsl:if>
</xsl:template>

<xsl:template match="physicalTable">
	<xsl:param name="allCols"/>
	<xsl:param name="selectedTables"/>
	<xsl:param name="mode"/>
	<xsl:param name="enableEngTid"/>	
	<xsl:variable name="tableName" select="@name"/>
	<xsl:variable name="allColumns" select="$selectedTables/selectedSoRTable[@name=$tableName]/@allColumns"/>
	<step id="{@name}" className="com.ataccama.dqc.tasks.common.usersteps.io.InputStep" disabled="false" mode="NORMAL">
        <properties>
            <columns>
				<!-- <xsl:call-template name="instanceColumns"/> -->
				<xsl:if test="$allCols='true' or $allColumns='true'">  
                	<xsl:for-each select="columns/column[not(@name='eng_modified_by')]">
                		<xsl:call-template name="column"/>
                	</xsl:for-each>
                	<!-- added by TVY-->
                	<xsl:if test="$mode='delta'">   
						<columnDef name="eng_change_type" type="STRING"/>
						<columnDef name="eng_activity_change_type" type="STRING"/>  					
					</xsl:if> 						
            	</xsl:if> 
            	<xsl:if test="$allColumns='false'">
					<xsl:for-each select="columns/column[@name=$selectedTables/selectedSoRTable[@name=$tableName]/selectedSoRColumns/selectedSoRColumn/@name]">
                		<xsl:call-template name="column"/>
                	</xsl:for-each>
                	<xsl:if test="$selectedTables/selectedSoRTable[@name=$tableName]/selectedSoRColumns/selectedSoRColumn/@name='eng_change_type'">
                		<columnDef name="eng_change_type" type="STRING"/>
	                </xsl:if>
	                <xsl:if test="$selectedTables/selectedSoRTable[@name=$tableName]/selectedSoRColumns/selectedSoRColumn/@name='eng_activity_change_type'">
	                	<columnDef name="eng_activity_change_type" type="STRING"/>
	                </xsl:if>
            	</xsl:if>
            	
				<xsl:if test="$enableEngTid='true'">
					<columnDef name="eng_creation_tid" type="LONG"/>
					<columnDef name="eng_deletion_tid" type="LONG"/>
					<columnDef name="eng_last_update_tid" type="LONG"/>
					<columnDef name="eng_activation_tid" type="LONG"/>
					<columnDef name="eng_deactivation_tid" type="LONG"/>
				</xsl:if>		            	
            	
            </columns>
            <shadowColumns/>
            <comm:comment>
          			<xsl:call-template name="generated_step"/>
          		</comm:comment>
        </properties>
        
     	<visual-constraints layout="vertical">
			<xsl:attribute name="bounds">
				<xsl:value-of select="((position()-1)*220)+48"/><xsl:text>,72,-1,-1</xsl:text>
			</xsl:attribute>
		</visual-constraints>        
    </step>
</xsl:template>

<xsl:template name="column">
	<xsl:choose>
		<xsl:when test="@type='long_int'"><columnDef name="{@name}" type="LONG"/></xsl:when>
		<xsl:otherwise><columnDef name="{@name}" type="{fn:upper-case(@type)}"/></xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>