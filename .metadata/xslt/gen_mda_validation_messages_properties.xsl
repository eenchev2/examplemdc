<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="text" encoding="UTF-8" indent="yes"/>

<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>
<xsl:param name="pPat">"</xsl:param>

<xsl:template match="/*">
<xsl:variable name="newline"><xsl:text>
</xsl:text></xsl:variable>

	<xsl:for-each select="validationMessages/validationMessage">
		<xsl:value-of select="@key"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="replace(@message,$pPat,'')"/>	
		<xsl:value-of select="$newline"/>
	</xsl:for-each>
	<xsl:value-of select="'low='"/>
	<xsl:value-of select="replace(globalValidationSettings/validationIconsDQI/@low,$pPat,'')"/>
	<xsl:value-of select="$newline"/>
	<xsl:value-of select="'medium='"/>
	<xsl:value-of select="replace(globalValidationSettings/validationIconsDQI/@medium,$pPat,'')"/>
	<xsl:value-of select="$newline"/>
	<xsl:value-of select="'high='"/>
	<xsl:value-of select="replace(globalValidationSettings/validationIconsDQI/@high,$pPat,'')"/>
	<xsl:value-of select="$newline"/>
	
	<xsl:for-each select="$logicalModel/instanceModel/tables/table/guiTab/guiValidations/validationExpColumns/validationExpColumn/validationTabKeys/validationTabKey[@message!='']">
		<xsl:value-of select="'INSTANCE.'"/>
		<xsl:value-of select="current()/ancestor::table/@name"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="'src_'"/>		
		<xsl:value-of select="current()/ancestor::validationExpColumn/@name"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="@name"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="replace(@message,$pPat,'')"/>
		<xsl:value-of select="$newline"/>
	</xsl:for-each>
	<xsl:for-each select="$logicalModel/instanceModel/tables/table/columns/column/validations/validationKeys/validationKey[@message!='']">
		<xsl:value-of select="'INSTANCE.'"/>
		<xsl:value-of select="current()/ancestor::table/@name"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="'src_'"/>		
		<xsl:value-of select="current()/ancestor::column/@name"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="@name"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="replace(@message,$pPat,'')"/>
		<xsl:value-of select="$newline"/>
	</xsl:for-each>	
	<xsl:for-each select="$logicalModel/instanceModel/advanced/specialColumns/column/validations/validationKeys/validationKey[@message!='']">
		<xsl:value-of select="'INSTANCE.'"/>
		<xsl:value-of select="current()/ancestor::table/@name"/>
		<xsl:value-of select="'.'"/>	
		<xsl:value-of select="current()/ancestor::column/@name"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="replace(@message,$pPat,'')"/>
		<xsl:value-of select="$newline"/>
	</xsl:for-each>	
	<xsl:for-each select="$logicalModel/instanceModel/matchingTab/matchingTabColumns/column/validations/validationKeys/validationKey[@message!='']">
		<xsl:value-of select="'INSTANCE.'"/>
		<xsl:value-of select="current()/ancestor::table/@name"/>
		<xsl:value-of select="'.'"/>	
		<xsl:value-of select="current()/ancestor::column/@name"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="replace(@message,$pPat,'')"/>
		<xsl:value-of select="$newline"/>
	</xsl:for-each>	
	<xsl:for-each select="$logicalModel/masterModels/masterModel/masterTables/masterTable/guiTab/guiValidations/validationExpColumns/validationExpColumn/validationTabKeys/validationTabKey[@message!='']">
		<xsl:value-of select="'MASTER.'"/>
		<xsl:value-of select="current()/ancestor::masterModel/@name"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="current()/ancestor::masterTable/@name"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="'src_'"/>		
		<xsl:value-of select="current()/ancestor::validationExpColumn/@name"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="@name"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="replace(@message,$pPat,'')"/>
		<xsl:value-of select="$newline"/>
	</xsl:for-each>
	<xsl:for-each select="$logicalModel/masterModels/masterModel/masterTables/masterTable/columns/column/validations/validationKeys/validationKey[@message!='']">
		<xsl:value-of select="'MASTER.'"/>
		<xsl:value-of select="current()/ancestor::masterModel/@name"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="current()/ancestor::masterTable/@name"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="'src_'"/>
		<xsl:value-of select="current()/ancestor::column/@name"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="@name"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="replace(@message,$pPat,'')"/>
		<xsl:value-of select="$newline"/>
	</xsl:for-each>		
	<xsl:for-each select="$logicalModel/masterModels/masterModel/masterTables/masterTable/advanced/specialColumns/column/validations/validationKeys/validationKey[@message!='']">
		<xsl:value-of select="'MASTER.'"/>
		<xsl:value-of select="current()/ancestor::masterModel/@name"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="current()/ancestor::masterTable/@name"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="current()/ancestor::column/@name"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="replace(@message,$pPat,'')"/>
		<xsl:value-of select="$newline"/>
	</xsl:for-each>		
</xsl:template>
</xsl:stylesheet>