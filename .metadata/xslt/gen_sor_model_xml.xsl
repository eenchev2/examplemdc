<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>
<xsl:template match="/*">

<model>
	<entities>
		<xsl:for-each select="preview/databaseModel/sorTables/physicalTable">
			<xsl:variable name="tableName" select="lower-case(@name)"/>
			<entity name="{$tableName}">
				<columns>
					<xsl:for-each select="columns/column[@name!='id' and @origin!='internal']">
						<column name="{@name}" type="{@type}" origin="SOURCE">
							<xsl:if test="@size!=''">
								<xsl:attribute name="size" select="@size"/>
							</xsl:if>
						</column>
					</xsl:for-each>
				</columns>
				<relationships>
					<xsl:for-each select="/metadata/logicalModel/sorModel/relationships/relationship[@childTable=$tableName]">
						<rel name="{sf:nvl(lower-case(@parentRole), lower-case(@name))}"  parentEntity="{@parentTable}" 
							foreignKeyColumn="{sorForeignKey/column/@childColumn}"
							parentColumn="{sorForeignKey/column/@parentColumn}">	
							<xsl:attribute name="reverseName">
								<xsl:choose>
									<xsl:when test="@childRole != ''"><xsl:value-of select="lower-case(@childRole)"/></xsl:when>
									<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case(@name)"/></xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
						</rel>
					</xsl:for-each>
					<xsl:for-each select="/metadata/logicalModel/sorModel/tables/table[@name=$tableName]/columns/column[@refData != '']">
						<xsl:variable name="foreignKey">
								<xsl:choose>
									<xsl:when test="@isCmo='true'"><xsl:text>cmo_</xsl:text><xsl:value-of select="@name"/></xsl:when>
									<xsl:otherwise><xsl:value-of select="@name"/></xsl:otherwise>
								</xsl:choose>
						</xsl:variable>
						<rel reverseName="master_codes_{$foreignKey}_{lower-case($tableName)}" parentEntity="rd_{@refData}" name="{$foreignKey}_master_details" parentColumn="master_code" foreignKeyColumn="{$foreignKey}"/>
					</xsl:for-each>
				</relationships>
			</entity>
		</xsl:for-each>
		<xsl:for-each select="/metadata/logicalModel/dictionary/tables/table[@name=/metadata/logicalModel/sorModel/tables/table/columns/column/@refData]">
			<entity name="rd_{@name}" usage="DICTIONARY">
				<columns>
					<xsl:variable name="master_code_type" select="@masterCodeType"/>
					<column name="master_code" origin="source" size="1000" type="{$master_code_type}"/>
					<column name="master_name" origin="source" size="1000" type="string"/>
					<xsl:for-each select="columns/column">
						<column name="{@name}" origin="source" size="1000" type="{@type}"/>
					</xsl:for-each>
				</columns>
			</entity>
		</xsl:for-each>		
	</entities>	
</model>

</xsl:template>

</xsl:stylesheet>