<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" cdata-section-elements="sql"/>
	<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>

	<xsl:template match="/*">
	<xsl:variable name="hierarchies" select="."/>
		<hierarchy>
			<xsl:if test="guiHierarchiesInstance/@allRels='true' or guiHierarchiesInstance/guiHierarchiesArray/*[@enable='true']">
				<instanceLayer>
					<entities>
						<xsl:choose>
							<xsl:when test="guiHierarchiesInstance/@allRels='true'">
								<xsl:for-each select="$logicalModel/instanceModel/tables/table[@name=$logicalModel/instanceModel/relationships/relationship/@parentTable or @name=$logicalModel/instanceModel/relationships/relationship/@childTable]">
									<entity name="{@name}">
										<xsl:variable name="tableName" select="@name"/>					
										<endpoints>
											<xsl:for-each select="$logicalModel/instanceModel/relationships/relationship[@parentTable=$tableName]">
												<xsl:variable name="relationName" select="@name"/>
												<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpoint1N" label="{@childTable}" position="RIGHT">
													<xsl:attribute name="relName">
														<xsl:choose>
															<xsl:when test="$logicalModel/instanceModel/relationships/relationship[@name=$relationName]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/instanceModel/relationships/relationship[@name=$relationName]/@childRole)"/></xsl:when>
															<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/instanceModel/relationships/relationship[@name=$relationName]/@name)"/></xsl:otherwise>
														</xsl:choose>	
													</xsl:attribute>								
												</endpoint>
											</xsl:for-each>
											<xsl:for-each select="$logicalModel/instanceModel/relationships/relationship[@childTable=$tableName]">
												<xsl:variable name="relationName" select="@name"/>
												<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpoint1N" label="{@parentTable}" position="LEFT">
													<xsl:attribute name="relName">
														<xsl:choose>
															<xsl:when test="$logicalModel/instanceModel/relationships/relationship[@name=$relationName]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/instanceModel/relationships/relationship[@name=$relationName]/@childRole)"/></xsl:when>
															<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/instanceModel/relationships/relationship[@name=$relationName]/@name)"/></xsl:otherwise>
														</xsl:choose>	
													</xsl:attribute>								
												</endpoint>
											</xsl:for-each>											
										</endpoints>
									</entity>
								</xsl:for-each>								
							</xsl:when>
							<xsl:otherwise>					
								<xsl:for-each select="$logicalModel/instanceModel/tables/table">
									<xsl:if test="@name=$logicalModel/instanceModel/relationships/relationship[@name=$hierarchies/guiHierarchiesInstance/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@parentTable or @name=$logicalModel/instanceModel/relationships/relationship[@name=$hierarchies/guiHierarchiesInstance/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@childTable or @name=$logicalModel/instanceModel/relationships/relationship[@name=$hierarchies/guiHierarchiesInstance/guiHierarchiesArray/hierarchyMN[@enable='true']/startMNPoint/@relationship]/@parentTable or @name=$logicalModel/instanceModel/relationships/relationship[@name=$hierarchies/guiHierarchiesInstance/guiHierarchiesArray/hierarchyMN[@enable='true']/endMNPoint/@relationship]/@parentTable">
										<entity name="{@name}">
											<xsl:variable name="tableName" select="@name"/>					
											<endpoints>
												<xsl:if test="@name=$logicalModel/instanceModel/relationships/relationship[@name=$hierarchies/guiHierarchiesInstance/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@parentTable or @name=$logicalModel/instanceModel/relationships/relationship[@name=$hierarchies/guiHierarchiesInstance/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@childTable">
													<xsl:for-each select="$hierarchies/guiHierarchiesInstance/guiHierarchiesArray/hierarchy1N[@enable='true' and (startPoint/@entity=$tableName or endPoint/@entity=$tableName)]">
														<xsl:variable name="relationName" select="@relationship"/>
														<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpoint1N" label="{if(startPoint/@entity=$tableName) then sf:nvl(startPoint/@label,$tableName) else sf:nvl(endPoint/@label,$tableName)}" position="{if(startPoint/@entity=$tableName) then startPoint/@position else endPoint/@position}">
															<xsl:attribute name="relName">
																<xsl:choose>
																	<xsl:when test="$logicalModel/instanceModel/relationships/relationship[@name=$relationName]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/instanceModel/relationships/relationship[@name=$relationName]/@childRole)"/></xsl:when>
																	<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/instanceModel/relationships/relationship[@name=$relationName]/@name)"/></xsl:otherwise>
																</xsl:choose>	
															</xsl:attribute>								
														</endpoint>
													</xsl:for-each>
												</xsl:if>
												<xsl:if test="@name=$logicalModel/instanceModel/relationships/relationship[@name=$hierarchies/guiHierarchiesInstance/guiHierarchiesArray/hierarchyMN[@enable='true']/startMNPoint/@relationship]/@parentTable or @name=$logicalModel/instanceModel/relationships/relationship[@name=$hierarchies/guiHierarchiesInstance/guiHierarchiesArray/hierarchyMN[@enable='true']/endMNPoint/@relationship]/@parentTable">
													<xsl:for-each select="$hierarchies/guiHierarchiesInstance/guiHierarchiesArray/hierarchyMN[@enable='true']">
														<xsl:variable name="startPointRel" select="startMNPoint/@relationship"/>
														<xsl:variable name="endPointRel" select="endMNPoint/@relationship"/>
														<xsl:variable name="relationName" select="@relationship"/>
														<xsl:if test="$tableName=$logicalModel/instanceModel/relationships/relationship[@name=$startPointRel]/@parentTable">
															<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpointMN" label="{sf:nvl(startMNPoint/@label,$startPointRel)}" position="{startMNPoint/@position}">
																<xsl:attribute name="relName">
																	<xsl:choose>
																		<xsl:when test="$logicalModel/instanceModel/relationships/relationship[@name=$startPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/instanceModel/relationships/relationship[@name=$startPointRel]/@childRole)"/></xsl:when>
																		<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/instanceModel/relationships/relationship[@name=$startPointRel]/@name)"/></xsl:otherwise>
																	</xsl:choose>	
																</xsl:attribute>		
																<xsl:attribute name="targetRelName">
																	<xsl:choose>
																		<xsl:when test="$logicalModel/instanceModel/relationships/relationship[@name=$endPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/instanceModel/relationships/relationship[@name=$endPointRel]/@childRole)"/></xsl:when>
																		<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/instanceModel/relationships/relationship[@name=$endPointRel]/@name)"/></xsl:otherwise>
																	</xsl:choose>	
																</xsl:attribute>																								
															</endpoint>
														</xsl:if>
														<xsl:if test="$tableName=$logicalModel/instanceModel/relationships/relationship[@name=$endPointRel]/@parentTable">
															<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpointMN" label="{sf:nvl(endMNPoint/@label,$endPointRel)}" position="{endMNPoint/@position}">
																<xsl:attribute name="relName">
																	<xsl:choose>
																		<xsl:when test="$logicalModel/instanceModel/relationships/relationship[@name=$endPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/instanceModel/relationships/relationship[@name=$endPointRel]/@childRole)"/></xsl:when>
																		<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/instanceModel/relationships/relationship[@name=$endPointRel]/@name)"/></xsl:otherwise>
																	</xsl:choose>	
																</xsl:attribute>	
																<xsl:attribute name="targetRelName">
																	<xsl:choose>
																		<xsl:when test="$logicalModel/instanceModel/relationships/relationship[@name=$startPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/instanceModel/relationships/relationship[@name=$startPointRel]/@childRole)"/></xsl:when>
																		<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/instanceModel/relationships/relationship[@name=$startPointRel]/@name)"/></xsl:otherwise>
																	</xsl:choose>	
																</xsl:attribute>																							
															</endpoint>
														</xsl:if>												
													</xsl:for-each>												
												</xsl:if>
											</endpoints>
										</entity>
									</xsl:if>
								</xsl:for-each>						
							</xsl:otherwise>
						</xsl:choose>
					</entities>
				</instanceLayer>
			</xsl:if>
			<xsl:if test="guiHierarchiesMaster/layers/layer/@allRels='true' or guiHierarchiesMaster/layers/layer[@enable='true']/guiHierarchiesArray/*[@enable='true']">
				<masterLayers>
					<xsl:for-each select="$logicalModel/masterModels/masterModel[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@enable='true']/@layerName]">
						<xsl:variable name="modelName" select="@name"/>
						<masterLayer name="{@name}">
							<entities>
								<xsl:choose>
									<xsl:when test="$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/@allRels='true'">
										<xsl:for-each select="$logicalModel/masterModels/masterModel[@name=$modelName]/masterTables/masterTable[@name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship/@parentTable or @name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship/@childTable]">
											<entity name="{@name}">
												<xsl:variable name="tableName" select="@name"/>					
												<endpoints>
													<xsl:for-each select="$logicalModel/$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@parentTable=$tableName]">
														<xsl:variable name="relationName" select="@name"/>
														<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpoint1N" label="{@childTable}" position="RIGHT">
															<xsl:attribute name="relName">
																<xsl:choose>
																	<xsl:when test="$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@childRole)"/></xsl:when>
																	<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@name)"/></xsl:otherwise>
																</xsl:choose>	
															</xsl:attribute>								
														</endpoint>
													</xsl:for-each>
													<xsl:for-each select="$logicalModel/$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@childTable=$tableName]">
														<xsl:variable name="relationName" select="@name"/>
														<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpoint1N" label="{@parentTable}" position="LEFT">
															<xsl:attribute name="relName">
																<xsl:choose>
																	<xsl:when test="$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@childRole)"/></xsl:when>
																	<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@name)"/></xsl:otherwise>
																</xsl:choose>	
															</xsl:attribute>								
														</endpoint>
													</xsl:for-each>											
												</endpoints>
											</entity>
										</xsl:for-each>	
										<xsl:for-each select="$logicalModel/masterModels/masterModel[@name=$modelName]/instanceTables/instanceTable[@name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship/@parentTable or @name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship/@childTable]">
											<entity name="{@name}">
												<xsl:variable name="tableName" select="@name"/>					
												<endpoints>
													<xsl:for-each select="$logicalModel/$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@parentTable=$tableName]">
														<xsl:variable name="relationName" select="@name"/>
														<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpoint1N" label="{if(startPoint/@entity=$tableName) then sf:nvl(startPoint/@label,$tableName) else sf:nvl(endPoint/@label,$tableName)}" position="RIGHT">
															<xsl:attribute name="relName">
																<xsl:choose>
																	<xsl:when test="$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@childRole)"/></xsl:when>
																	<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@name)"/></xsl:otherwise>
																</xsl:choose>	
															</xsl:attribute>								
														</endpoint>
													</xsl:for-each>
													<xsl:for-each select="$logicalModel/$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@childTable=$tableName]">
														<xsl:variable name="relationName" select="@name"/>
														<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpoint1N" label="{if(startPoint/@entity=$tableName) then sf:nvl(startPoint/@label,$tableName) else sf:nvl(endPoint/@label,$tableName)}" position="LEFT">
															<xsl:attribute name="relName">
																<xsl:choose>
																	<xsl:when test="$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@childRole)"/></xsl:when>
																	<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@name)"/></xsl:otherwise>
																</xsl:choose>	
															</xsl:attribute>								
														</endpoint>
													</xsl:for-each>											
												</endpoints>
											</entity>
										</xsl:for-each>																		
									</xsl:when>
									<xsl:otherwise>														
										<xsl:for-each select="$logicalModel/masterModels/masterModel[@name=$modelName]/masterTables/masterTable">
											<xsl:if test="@name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@parentTable or @name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@childTable or @name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchyMN[@enable='true']/startMNPoint/@relationship]/@parentTable or @name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchyMN[@enable='true']/endMNPoint/@relationship]/@parentTable">	
												<entity name="{@name}">												
													<xsl:variable name="tableName" select="@name"/>														
													<endpoints>
														<xsl:if test="@name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@parentTable or @name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@childTable">
															<xsl:for-each select="$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchy1N[@enable='true' and (startPoint/@entity=$tableName or endPoint/@entity=$tableName)]">
																<xsl:variable name="relationName" select="@relationship"/>
																<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpoint1N" label="{if(startPoint/@entity=$tableName) then sf:nvl(startPoint/@label,$tableName) else sf:nvl(endPoint/@label,$tableName)}" position="{if(startPoint/@entity=$tableName) then startPoint/@position else endPoint/@position}">
																	<xsl:attribute name="relName">
																		<xsl:choose>
																			<xsl:when test="$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@childRole)"/></xsl:when>
																			<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@name)"/></xsl:otherwise>
																		</xsl:choose>	
																	</xsl:attribute>								
																</endpoint>
															</xsl:for-each>
														</xsl:if>
														<xsl:if test="@name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchyMN[@enable='true']/startMNPoint/@relationship]/@parentTable or @name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchyMN[@enable='true']/endMNPoint/@relationship]/@parentTable">
															<xsl:for-each select="$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchyMN[@enable='true']">
																<xsl:variable name="startPointRel" select="startMNPoint/@relationship"/>
																<xsl:variable name="endPointRel" select="endMNPoint/@relationship"/>
																<xsl:variable name="relationName" select="@relationship"/>
																<xsl:if test="$tableName=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$startPointRel]/@parentTable">
																	<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpointMN" label="{sf:nvl(startMNPoint/@label,$startPointRel)}" position="{startMNPoint/@position}">
																		<xsl:attribute name="relName">
																			<xsl:choose>
																				<xsl:when test="$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$startPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$startPointRel]/@childRole)"/></xsl:when>
																				<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$startPointRel]/@name)"/></xsl:otherwise>
																			</xsl:choose>	
																		</xsl:attribute>	
																		<xsl:attribute name="targetRelName">
																			<xsl:choose>
																				<xsl:when test="$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$endPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$endPointRel]/@childRole)"/></xsl:when>
																				<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$endPointRel]/@name)"/></xsl:otherwise>
																			</xsl:choose>	
																		</xsl:attribute>																										
																	</endpoint>
																</xsl:if>
																<xsl:if test="$tableName=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$endPointRel]/@parentTable">
																	<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpointMN" label="{sf:nvl(endMNPoint/@label,$endPointRel)}" position="{endMNPoint/@position}">
																		<xsl:attribute name="relName">
																			<xsl:choose>
																				<xsl:when test="$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$endPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$endPointRel]/@childRole)"/></xsl:when>
																				<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$endPointRel]/@name)"/></xsl:otherwise>
																			</xsl:choose>	
																		</xsl:attribute>	
																		<xsl:attribute name="targetRelName">
																			<xsl:choose>
																				<xsl:when test="$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$startPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$startPointRel]/@childRole)"/></xsl:when>
																				<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$startPointRel]/@name)"/></xsl:otherwise>
																			</xsl:choose>	
																		</xsl:attribute>																																		
																	</endpoint>
																</xsl:if>										
															</xsl:for-each>													
														</xsl:if>
													</endpoints>									
												</entity>
											</xsl:if>
										</xsl:for-each>
										<xsl:for-each select="$logicalModel/masterModels/masterModel[@name=$modelName]/instanceTables/instanceTable">
											<xsl:if test="@name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@parentTable or @name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@childTable or @name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchyMN[@enable='true']/startMNPoint/@relationship]/@parentTable or @name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchyMN[@enable='true']/endMNPoint/@relationship]/@parentTable">
												<entity name="{@name}">
													<xsl:variable name="tableName" select="@name"/>					
													<endpoints>
														<xsl:if test="@name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@parentTable or @name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@childTable">
															<xsl:for-each select="$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchy1N[@enable='true' and (startPoint/@entity=$tableName or endPoint/@entity=$tableName)]">
																<xsl:variable name="relationName" select="@relationship"/>
																<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpoint1N" label="{if(startPoint/@entity=$tableName) then sf:nvl(startPoint/@label,$tableName) else sf:nvl(endPoint/@label,$tableName)}" position="{if(startPoint/@entity=$tableName) then startPoint/@position else endPoint/@position}">
																	<xsl:attribute name="relName">
																		<xsl:choose>
																			<xsl:when test="$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@childRole)"/></xsl:when>
																			<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$relationName]/@name)"/></xsl:otherwise>
																		</xsl:choose>	
																	</xsl:attribute>								
																</endpoint>
															</xsl:for-each>
														</xsl:if>
														<xsl:if test="@name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchyMN[@enable='true']/startMNPoint/@relationship]/@parentTable or @name=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchyMN[@enable='true']/endMNPoint/@relationship]/@parentTable">
															<xsl:for-each select="$hierarchies/guiHierarchiesMaster/layers/layer[@layerName=$modelName]/guiHierarchiesArray/hierarchyMN[@enable='true']">
																<xsl:variable name="startPointRel" select="startMNPoint/@relationship"/>
																<xsl:variable name="endPointRel" select="endMNPoint/@relationship"/>
																<xsl:variable name="relationName" select="@relationship"/>
																<xsl:if test="$tableName=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$startPointRel]/@parentTable">
																	<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpointMN" label="{sf:nvl(startMNPoint/@label,$startPointRel)}" position="{startMNPoint/@position}">
																		<xsl:attribute name="relName">
																			<xsl:choose>
																				<xsl:when test="$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$startPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$startPointRel]/@childRole)"/></xsl:when>
																				<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$startPointRel]/@name)"/></xsl:otherwise>
																			</xsl:choose>	
																		</xsl:attribute>
																		<xsl:attribute name="targetRelName">
																			<xsl:choose>
																				<xsl:when test="$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$endPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$endPointRel]/@childRole)"/></xsl:when>
																				<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$endPointRel]/@name)"/></xsl:otherwise>
																			</xsl:choose>	
																		</xsl:attribute>																											
																	</endpoint>
																</xsl:if>
																<xsl:if test="$tableName=$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$endPointRel]/@parentTable">
																	<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpointMN" label="{sf:nvl(endMNPoint/@label,$endPointRel)}" position="{endMNPoint/@position}">
																		<xsl:attribute name="relName">
																			<xsl:choose>
																				<xsl:when test="$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$endPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$endPointRel]/@childRole)"/></xsl:when>
																				<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$endPointRel]/@name)"/></xsl:otherwise>
																			</xsl:choose>	
																		</xsl:attribute>	
																		<xsl:attribute name="targetRelName">
																			<xsl:choose>
																				<xsl:when test="$logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$startPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$startPointRel]/@childRole)"/></xsl:when>
																				<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/masterModels/masterModel[@name=$modelName]/relationships/relationship[@name=$startPointRel]/@name)"/></xsl:otherwise>
																			</xsl:choose>	
																		</xsl:attribute>																									
																	</endpoint>
																</xsl:if>												
															</xsl:for-each>													
														</xsl:if>
													</endpoints>									
												</entity>
											</xsl:if>
										</xsl:for-each>																																																
									</xsl:otherwise>
								</xsl:choose>
							</entities>
						</masterLayer>
					</xsl:for-each>
				</masterLayers>	
			</xsl:if>
			<xsl:if test="guiHierarchiesSoR/@allRels='true' or guiHierarchiesSoR/guiHierarchiesArray/*[@enable='true']">
				<sorLayer>
					<entities>
						<xsl:choose>
							<xsl:when test="guiHierarchiesSoR/@allRels='true'">
								<xsl:for-each select="$logicalModel/sorModel/tables/table[@name=$logicalModel/sorModel/relationships/relationship/@parentTable or @name=$logicalModel/sorModel/relationships/relationship/@childTable]">
									<entity name="{@name}">
										<xsl:variable name="tableName" select="@name"/>					
										<endpoints>
											<xsl:for-each select="$logicalModel/sorModel/relationships/relationship[@parentTable=$tableName]">
												<xsl:variable name="relationName" select="@name"/>
												<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpoint1N" label="{@childTable}" position="RIGHT">
													<xsl:attribute name="relName">
														<xsl:choose>
															<xsl:when test="$logicalModel/sorModel/relationships/relationship[@name=$relationName]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/sorModel/relationships/relationship[@name=$relationName]/@childRole)"/></xsl:when>
															<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/sorModel/relationships/relationship[@name=$relationName]/@name)"/></xsl:otherwise>
														</xsl:choose>	
													</xsl:attribute>								
												</endpoint>
											</xsl:for-each>
											<xsl:for-each select="$logicalModel/sorModel/relationships/relationship[@childTable=$tableName]">
												<xsl:variable name="relationName" select="@name"/>
												<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpoint1N" label="{@parentTable}" position="LEFT">
													<xsl:attribute name="relName">
														<xsl:choose>
															<xsl:when test="$logicalModel/sorModel/relationships/relationship[@name=$relationName]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/sorModel/relationships/relationship[@name=$relationName]/@childRole)"/></xsl:when>
															<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/sorModel/relationships/relationship[@name=$relationName]/@name)"/></xsl:otherwise>
														</xsl:choose>	
													</xsl:attribute>								
												</endpoint>
											</xsl:for-each>											
										</endpoints>
									</entity>
								</xsl:for-each>								
							</xsl:when>
							<xsl:otherwise>					
								<xsl:for-each select="$logicalModel/sorModel/tables/table">
									<xsl:if test="@name=$logicalModel/sorModel/relationships/relationship[@name=$hierarchies/guiHierarchiesSoR/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@parentTable or @name=$logicalModel/sorModel/relationships/relationship[@name=$hierarchies/guiHierarchiesSoR/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@childTable or @name=$logicalModel/sorModel/relationships/relationship[@name=$hierarchies/guiHierarchiesSoR/guiHierarchiesArray/hierarchyMN[@enable='true']/startMNPoint/@relationship]/@parentTable or @name=$logicalModel/sorModel/relationships/relationship[@name=$hierarchies/guiHierarchiesSoR/guiHierarchiesArray/hierarchyMN[@enable='true']/endMNPoint/@relationship]/@parentTable">
										<entity name="{@name}">
											<xsl:variable name="tableName" select="@name"/>					
											<endpoints>
												<xsl:if test="@name=$logicalModel/sorModel/relationships/relationship[@name=$hierarchies/guiHierarchiesSoR/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@parentTable or @name=$logicalModel/sorModel/relationships/relationship[@name=$hierarchies/guiHierarchiesSoR/guiHierarchiesArray/hierarchy1N[@enable='true']/@relationship]/@childTable">
													<xsl:for-each select="$hierarchies/guiHierarchiesSoR/guiHierarchiesArray/hierarchy1N[@enable='true' and (startPoint/@entity=$tableName or endPoint/@entity=$tableName)]">
														<xsl:variable name="relationName" select="@relationship"/>
														<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpoint1N" label="{if(startPoint/@entity=$tableName) then sf:nvl(startPoint/@label,$tableName) else sf:nvl(endPoint/@label,$tableName)}" position="{if(startPoint/@entity=$tableName) then startPoint/@position else endPoint/@position}">
															<xsl:attribute name="relName">
																<xsl:choose>
																	<xsl:when test="$logicalModel/sorModel/relationships/relationship[@name=$relationName]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/sorModel/relationships/relationship[@name=$relationName]/@childRole)"/></xsl:when>
																	<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/sorModel/relationships/relationship[@name=$relationName]/@name)"/></xsl:otherwise>
																</xsl:choose>	
															</xsl:attribute>								
														</endpoint>
													</xsl:for-each>
												</xsl:if>
												<xsl:if test="@name=$logicalModel/sorModel/relationships/relationship[@name=$hierarchies/guiHierarchiesSoR/guiHierarchiesArray/hierarchyMN[@enable='true']/startMNPoint/@relationship]/@parentTable or @name=$logicalModel/sorModel/relationships/relationship[@name=$hierarchies/guiHierarchiesSoR/guiHierarchiesArray/hierarchyMN[@enable='true']/endMNPoint/@relationship]/@parentTable">
													<xsl:for-each select="$hierarchies/guiHierarchiesSoR/guiHierarchiesArray/hierarchyMN[@enable='true']">
														<xsl:variable name="startPointRel" select="startMNPoint/@relationship"/>
														<xsl:variable name="endPointRel" select="endMNPoint/@relationship"/>
														<xsl:variable name="relationName" select="@relationship"/>
														<xsl:if test="$tableName=$logicalModel/sorModel/relationships/relationship[@name=$startPointRel]/@parentTable">
															<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpointMN" label="{sf:nvl(startMNPoint/@label,$startPointRel)}" position="{startMNPoint/@position}">
																<xsl:attribute name="relName">
																	<xsl:choose>
																		<xsl:when test="$logicalModel/sorModel/relationships/relationship[@name=$startPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/sorModel/relationships/relationship[@name=$startPointRel]/@childRole)"/></xsl:when>
																		<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/sorModel/relationships/relationship[@name=$startPointRel]/@name)"/></xsl:otherwise>
																	</xsl:choose>	
																</xsl:attribute>
																<xsl:attribute name="targetRelName">
																	<xsl:choose>
																		<xsl:when test="$logicalModel/sorModel/relationships/relationship[@name=$endPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/sorModel/relationships/relationship[@name=$endPointRel]/@childRole)"/></xsl:when>
																		<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/sorModel/relationships/relationship[@name=$endPointRel]/@name)"/></xsl:otherwise>
																	</xsl:choose>	
																</xsl:attribute>																									
															</endpoint>
														</xsl:if>
														<xsl:if test="$tableName=$logicalModel/sorModel/relationships/relationship[@name=$endPointRel]/@parentTable">
															<endpoint class="com.ataccama.mda.core.config.hierarchy.MdaHierarchyEndpointMN" label="{sf:nvl(endMNPoint/@label,$endPointRel)}" position="{endMNPoint/@position}">
																<xsl:attribute name="relName">
																	<xsl:choose>
																		<xsl:when test="$logicalModel/sorModel/relationships/relationship[@name=$endPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/sorModel/relationships/relationship[@name=$endPointRel]/@childRole)"/></xsl:when>
																		<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/sorModel/relationships/relationship[@name=$endPointRel]/@name)"/></xsl:otherwise>
																	</xsl:choose>	
																</xsl:attribute>
																<xsl:attribute name="targetRelName">
																	<xsl:choose>
																		<xsl:when test="$logicalModel/sorModel/relationships/relationship[@name=$startPointRel]/@childRole != ''"><xsl:value-of select="lower-case($logicalModel/sorModel/relationships/relationship[@name=$startPointRel]/@childRole)"/></xsl:when>
																		<xsl:otherwise><xsl:text>rev_</xsl:text><xsl:value-of select="lower-case($logicalModel/sorModel/relationships/relationship[@name=$startPointRel]/@name)"/></xsl:otherwise>
																	</xsl:choose>	
																</xsl:attribute>																								
															</endpoint>
														</xsl:if>												
													</xsl:for-each>												
												</xsl:if>
											</endpoints>
										</entity>
									</xsl:if>
								</xsl:for-each>						
							</xsl:otherwise>
						</xsl:choose>
					</entities>
				</sorLayer>
			</xsl:if>
		</hierarchy>
	</xsl:template>
</xsl:stylesheet>