<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

 <xsl:template match="/*">
	<xsl:choose>
		<xsl:when test="@layerName='match_proposal'">
			<cc planName="ep_match_proposal_{fn:substring-before(entityName/@entity,' ')}_{substring-before(fn:substring-after(entityName/@entity,'('),')')}{@suffix}.comp"/>			
		</xsl:when>
		<xsl:otherwise>
			<cc planName="ep_{concat(@layerName,'_',entityName/@entity,@suffix)}.comp"/>		
		</xsl:otherwise>
	</xsl:choose>

 </xsl:template>

</xsl:stylesheet>

