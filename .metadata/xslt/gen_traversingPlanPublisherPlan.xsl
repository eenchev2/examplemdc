<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet exclude-result-prefixes="sf fn comm" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:sf="http://www.ataccama.com/xslt/functions" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:comm="http://www.ataccama.com/purity/comment" version="2.0">
<xsl:output indent="yes" encoding="UTF-8" method="xml"/>
<xsl:include href="incl_constants.xsl"/>
<xsl:include href="_constants.xsl"/>
<xsl:include href="incl_plan_comments.xsl"/>
<xsl:param name="databaseModel" select="document('param:databaseModel')/*"/>
<xsl:param select="document(&#39;param:eventHandler&#39;)/*" name="eventHandler"/>

<xsl:template match="/*">
	<purity-config version="{$version}">
		<xsl:variable name="entityName" select="fn:substring-before(@rootEntity,' ')"/>			
		<xsl:variable name="layerName" select="substring-before(fn:substring-after(@rootEntity,'('),')')"/>		
		<xsl:choose>
			<xsl:when test="$layerName='instance'">
				<step id="{$entityName}.instance" className="com.ataccama.dqc.tasks.common.usersteps.io.InputStep" disabled="false" mode="NORMAL">
					<properties>
			        	<columns>           	
			         		<xsl:choose>
			          			<xsl:when test="@allColumns=&#39;true&#39;">
					          		<xsl:for-each select="$eventHandler/metaColumns/metaColumn">
					          			<columnDef name="meta_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>
					          		</xsl:for-each>
					          		<xsl:for-each select="$databaseModel/instanceTables/physicalTable[@name=$entityName]/columns/column[@name!='id']">
					         				<columnDef name="record_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>
					         				<columnDef name="old_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>	            			
					          		</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>																								
									<xsl:for-each select="columnsTraversing/columnTraversing">
										<xsl:variable name="columnName" select="@name"/>								
										<xsl:variable name="type" select="$databaseModel/instanceTables/physicalTable[@name=$entityName and @layerName=$layerName]/columns/column[@name=lower-case($columnName)]/@type"/>
										<xsl:variable name="name" select="$databaseModel/instanceTables/physicalTable[@name=$entityName and @layerName=$layerName]/columns/column[@name=lower-case($columnName)]/@name"/>
										<xsl:if test="@record=&#39;true&#39;">
					           				<columnDef name="record_{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
					           			</xsl:if>
					           			<xsl:if test="@old=&#39;true&#39;">
					           				<columnDef name="old_{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
					           			</xsl:if>
					           			<xsl:if test="@old = &#39;false&#39; and @record = &#39;false&#39;">
					           				<columnDef name="{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
					           			</xsl:if>
			          				</xsl:for-each>				                 	            		
			           				<xsl:for-each select="columnsMeta/columnMeta">			
			           					<xsl:variable name="metaColumnName" select="replace(@name,'meta_','')"/>			            		
										<xsl:variable name="type" select="$eventHandler/metaColumns/metaColumn[@name=$metaColumnName]/@type"/>			            																			
										<columnDef name="{lower-case(@name)}" type="{upper-case($type)}"/>								
									</xsl:for-each>					
								</xsl:otherwise>
							</xsl:choose>
							<columnDef name="has_event" type="BOOLEAN"/>			
						</columns>
						<shadowColumns/>
						<comm:comment>
			        		<xsl:call-template name="generated_step"/>
			        	</comm:comment>
					</properties>	
			        <visual-constraints layout="vertical">
						<xsl:attribute name="bounds">
							<xsl:value-of select="((position()-1)*336)+48"/><xsl:text>,48,-1,-1</xsl:text>
						</xsl:attribute>
					</visual-constraints>					
				</step>
			</xsl:when>
			<xsl:otherwise>
				<step className="com.ataccama.dqc.tasks.common.usersteps.io.InputStep" disabled="false" mode="NORMAL">
					<xsl:attribute name="id">
						<xsl:value-of select="$entityName"/>
						<xsl:value-of select="'.master'"/>
						<xsl:value-of select="'.'"/>
						<xsl:value-of select="$layerName"/>
					</xsl:attribute>		
					<properties>
			        	<columns>           	
			         		<xsl:choose>
			          			<xsl:when test="@allColumns=&#39;true&#39;">
					          		<xsl:for-each select="$eventHandler/metaColumns/metaColumn">
					          			<columnDef name="meta_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>
					          		</xsl:for-each>
					          		<xsl:for-each select="$databaseModel/masterTables/physicalTable[@layerName=$layerName and @name=$entityName]/columns/column[@name!='id']">
					         				<columnDef name="record_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>
					         				<columnDef name="old_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>	            			
					          		</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>																								
									<xsl:for-each select="columnsTraversing/columnTraversing">
										<xsl:variable name="columnName" select="@name"/>								
										<xsl:variable name="type" select="$databaseModel/masterTables/physicalTable[@layerName=$layerName and @name=$entityName]/columns/column[@name=lower-case($columnName)]/@type"/>
										<xsl:variable name="name" select="$databaseModel/masterTables/physicalTable[@layerName=$layerName and @name=$entityName]/columns/column[@name=lower-case($columnName)]/@name"/>
										<xsl:if test="@record=&#39;true&#39;">
					           				<columnDef name="record_{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
					           			</xsl:if>
					           			<xsl:if test="@old=&#39;true&#39;">
					           				<columnDef name="old_{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
					           			</xsl:if>
					           			<xsl:if test="@old = &#39;false&#39; and @record = &#39;false&#39;">
					           				<columnDef name="{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
					           			</xsl:if>
			          				</xsl:for-each>				                 	            		
			           				<xsl:for-each select="columnsMeta/columnMeta">			
			           					<xsl:variable name="metaColumnName" select="replace(@name,'meta_','')"/>			            		
										<xsl:variable name="type" select="$eventHandler/metaColumns/metaColumn[@name=$metaColumnName]/@type"/>			            																			
										<columnDef name="{lower-case(@name)}" type="{upper-case($type)}"/>								
									</xsl:for-each>					
								</xsl:otherwise>
							</xsl:choose>	
							<columnDef name="has_event" type="BOOLEAN"/>		
						</columns>
						<shadowColumns/>
						<comm:comment>
			        		<xsl:call-template name="generated_step"/>
			        	</comm:comment>
					</properties>		
			        <visual-constraints layout="vertical">
						<xsl:attribute name="bounds">
							<xsl:value-of select="((position()-1)*336)+48"/><xsl:text>,48,-1,-1</xsl:text>
						</xsl:attribute>
					</visual-constraints>						
				</step>
			</xsl:otherwise>
		</xsl:choose>				
		<xsl:apply-templates select="//childEntitiesTraversing/childEntityTraversing"/>			
	</purity-config>
</xsl:template>

<xsl:template match="childEntityTraversing">
	<xsl:variable name="entityName" select="fn:substring-before(@name,' ')"/>			
	<xsl:variable name="layerName" select="substring-before(fn:substring-after(@name,'('),')')"/>		
	<xsl:choose>
		<xsl:when test="$layerName='instance'">
			<step id="{$entityName}.instance" className="com.ataccama.dqc.tasks.common.usersteps.io.InputStep" disabled="false" mode="NORMAL">
				<properties>
		        	<columns>           	
		         		<xsl:choose>
		          			<xsl:when test="@allColumns=&#39;true&#39;">
				          		<xsl:for-each select="$eventHandler/metaColumns/metaColumn">
				          			<columnDef name="meta_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>
				          		</xsl:for-each>
				          		<xsl:for-each select="$databaseModel/instanceTables/physicalTable[@name=$entityName]/columns/column[@name!='id']">
				         				<columnDef name="record_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>
				         				<columnDef name="old_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>	            			
				          		</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>																								
								<xsl:for-each select="columnsTraversing/columnTraversing">
									<xsl:variable name="columnName" select="@name"/>								
									<xsl:variable name="type" select="$databaseModel/instanceTables/physicalTable[@name=$entityName]/columns/column[@name=lower-case($columnName)]/@type"/>
									<xsl:variable name="name" select="$databaseModel/instanceTables/physicalTable[@name=$entityName]/columns/column[@name=lower-case($columnName)]/@name"/>
									<xsl:if test="@record=&#39;true&#39;">
				           				<columnDef name="record_{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
				           			</xsl:if>
				           			<xsl:if test="@old=&#39;true&#39;">
				           				<columnDef name="old_{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
				           			</xsl:if>
				           			<xsl:if test="@old = &#39;false&#39; and @record = &#39;false&#39;">
				           				<columnDef name="{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
				           			</xsl:if>
		          				</xsl:for-each>				                 	            		
		           				<xsl:for-each select="columnsMeta/columnMeta">			
		           					<xsl:variable name="metaColumnName" select="replace(@name,'meta_','')"/>			            		
									<xsl:variable name="type" select="$eventHandler/metaColumns/metaColumn[@name=$metaColumnName]/@type"/>			            																			
									<columnDef name="{lower-case(@name)}" type="{upper-case($type)}"/>								
								</xsl:for-each>					
							</xsl:otherwise>
						</xsl:choose>	
						<columnDef name="has_event" type="BOOLEAN"/>		
					</columns>
					<shadowColumns/>
					<comm:comment>
		        		<xsl:call-template name="generated_step"/>
		        	</comm:comment>
				</properties>
		        <visual-constraints layout="vertical">
					<xsl:attribute name="bounds">
						<xsl:value-of select="((position()-1)*230)+240"/><xsl:text>,48,-1,-1</xsl:text>
					</xsl:attribute>
				</visual-constraints>								
			</step>
		</xsl:when>
		<xsl:when test="$layerName=''">
			<step id="{@name}.instance" className="com.ataccama.dqc.tasks.common.usersteps.io.InputStep" disabled="false" mode="NORMAL">
				<properties>
		        	<columns>           	
		         		<xsl:choose>
		          			<xsl:when test="@allColumns=&#39;true&#39;">
				          		<xsl:for-each select="$eventHandler/metaColumns/metaColumn">
				          			<columnDef name="meta_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>
				          		</xsl:for-each>
				          		<xsl:for-each select="$databaseModel/instanceTables/physicalTable[@name=$entityName]/columns/column[@name!='id']">
				         				<columnDef name="record_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>
				         				<columnDef name="old_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>	            			
				          		</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>																								
								<xsl:for-each select="columnsTraversing/columnTraversing">
									<xsl:variable name="columnName" select="@name"/>								
									<xsl:variable name="type" select="$databaseModel/instanceTables/physicalTable[@name=$entityName]/columns/column[@name=lower-case($columnName)]/@type"/>
									<xsl:variable name="name" select="$databaseModel/instanceTables/physicalTable[@name=$entityName]/columns/column[@name=lower-case($columnName)]/@name"/>
									<xsl:if test="@record=&#39;true&#39;">
				           				<columnDef name="record_{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
				           			</xsl:if>
				           			<xsl:if test="@old=&#39;true&#39;">
				           				<columnDef name="old_{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
				           			</xsl:if>
				           			<xsl:if test="@old = &#39;false&#39; and @record = &#39;false&#39;">
				           				<columnDef name="{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
				           			</xsl:if>
		          				</xsl:for-each>				                 	            		
		           				<xsl:for-each select="columnsMeta/columnMeta">			
		           					<xsl:variable name="metaColumnName" select="replace(@name,'meta_','')"/>			            		
									<xsl:variable name="type" select="$eventHandler/metaColumns/metaColumn[@name=$metaColumnName]/@type"/>			            																			
									<columnDef name="{lower-case(@name)}" type="{upper-case($type)}"/>								
								</xsl:for-each>					
							</xsl:otherwise>
						</xsl:choose>	
						<columnDef name="has_event" type="BOOLEAN"/>		
					</columns>
					<shadowColumns/>
					<comm:comment>
		        		<xsl:call-template name="generated_step"/>
		        	</comm:comment>
				</properties>
		        <visual-constraints layout="vertical">
					<xsl:attribute name="bounds">
						<xsl:value-of select="((position()-1)*230)+240"/><xsl:text>,48,-1,-1</xsl:text>
					</xsl:attribute>
				</visual-constraints>								
			</step>
		</xsl:when>		
		<xsl:otherwise>
			<step className="com.ataccama.dqc.tasks.common.usersteps.io.InputStep" disabled="false" mode="NORMAL">
				<xsl:attribute name="id">
					<xsl:value-of select="$entityName"/>
					<xsl:value-of select="'.master'"/>
					<xsl:value-of select="'.'"/>
					<xsl:value-of select="$layerName"/>
				</xsl:attribute>		
				<properties>
		        	<columns>           	
		         		<xsl:choose>
		          			<xsl:when test="@allColumns=&#39;true&#39;">
				          		<xsl:for-each select="$eventHandler/metaColumns/metaColumn">
				          			<columnDef name="meta_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>
				          		</xsl:for-each>
				          		<xsl:for-each select="$databaseModel/masterTables/physicalTable[@layerName=$layerName and @name=$entityName]/columns/column[@name!='id']">
				         				<columnDef name="record_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>
				         				<columnDef name="old_{@name}" type="{if(@type='long_int') then 'LONG' else upper-case(@type)}"/>	            			
				          		</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>																								
								<xsl:for-each select="columnsTraversing/columnTraversing">
									<xsl:variable name="columnName" select="@name"/>								
									<xsl:variable name="type" select="$databaseModel/masterTables/physicalTable[@layerName=$layerName and @name=$entityName]/columns/column[@name=lower-case($columnName)]/@type"/>
									<xsl:variable name="name" select="$databaseModel/masterTables/physicalTable[@layerName=$layerName and @name=$entityName]/columns/column[@name=lower-case($columnName)]/@name"/>
									<xsl:if test="@record=&#39;true&#39;">
				           				<columnDef name="record_{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
				           			</xsl:if>
				           			<xsl:if test="@old=&#39;true&#39;">
				           				<columnDef name="old_{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
				           			</xsl:if>
				           			<xsl:if test="@old = &#39;false&#39; and @record = &#39;false&#39;">
				           				<columnDef name="{@name}" type="{if($type='long_int') then 'LONG' else upper-case($type)}"/>
				           			</xsl:if>
		          				</xsl:for-each>				                 	            		
		           				<xsl:for-each select="columnsMeta/columnMeta">			
		           					<xsl:variable name="metaColumnName" select="replace(@name,'meta_','')"/>			            		
									<xsl:variable name="type" select="$eventHandler/metaColumns/metaColumn[@name=$metaColumnName]/@type"/>			            																			
									<columnDef name="{lower-case(@name)}" type="{upper-case($type)}"/>								
								</xsl:for-each>					
							</xsl:otherwise>
						</xsl:choose>		
						<columnDef name="has_event" type="BOOLEAN"/>	
					</columns>
					<shadowColumns/>
					<comm:comment>
		        		<xsl:call-template name="generated_step"/>
		        	</comm:comment>
				</properties>	
		        <visual-constraints layout="vertical">
					<xsl:attribute name="bounds">
						<xsl:value-of select="((position()-1)*230)+240"/><xsl:text>,48,-1,-1</xsl:text>
					</xsl:attribute>
				</visual-constraints>				
			</step>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet> 