<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>
	
	<xsl:template match="/*">
		<ccMatchingProposalsColumns>
			<ccMatchingProposalsColumn name="record_id" type="LONG"/>
			<ccMatchingProposalsColumn name="related_record_id" type="LONG"/>
			<ccMatchingProposalsColumn name="master_id" type="LONG"/>
			<ccMatchingProposalsColumn name="related_master_id" type="LONG"/>
			<ccMatchingProposalsColumn name="match_quality" type="FLOAT"/>
			<ccMatchingProposalsColumn name="match_rule_name" type="STRING"/>
			<ccMatchingProposalsColumn name="from_proposal_rule" type="BOOLEAN"/>
			<ccMatchingProposalsColumn name="from_constraint_issue" type="BOOLEAN"/>
		</ccMatchingProposalsColumns>
	</xsl:template>

</xsl:stylesheet>