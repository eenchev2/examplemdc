<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	
	<!-- detail view groups -->
	<xsl:template match="labeledGroup">
		<!-- <xsl:param name="templatePosition"/> -->
		<fieldGrid classes="mda-{@templatePosition}ContentMargin" labelKey="{@label}">
			<xsl:if test="@viewCondition!=''">
				<xsl:attribute name="parts" select="@viewCondition"/>
			</xsl:if>
			<!--  there should be a new field in configuration describing the left/right position  -->
			<fields>
				<xsl:apply-templates select="columns/column" mode="view">
					<xsl:with-param name="general_name" select="@name"/>
				</xsl:apply-templates>
			</fields>
		</fieldGrid>
	</xsl:template>
	
	<xsl:template match="labeledGroup" mode="readOnly">
			<!-- read only added -->
			<!-- <xsl:param name="templatePosition"/> -->
			<fieldGrid classes="mda-{@templatePosition}ContentMargin" labelKey="{@label}" id="{lower-case(replace(@label, ' ', '_'))}_instance_panel">
				<!--  there should be a new field in configuration describing the left/right position  -->
				<xsl:if test="@viewCondition!=''">
					<xsl:attribute name="parts" select="@viewCondition"/>
				</xsl:if>
				<fields>
					<xsl:apply-templates select="columns/column" mode="view">
						<xsl:with-param name="general_name" select="@name"/>
					</xsl:apply-templates>
				</fields>
			</fieldGrid>
		</xsl:template>

	<xsl:template match="labeledGroup" mode="sor">
		<!-- <xsl:param name="templatePosition"/> -->
		<fieldGrid classes="mda-{@templatePosition}ContentMargin" labelKey="{@label}">
			<xsl:if test="@viewCondition!=''">
				<xsl:attribute name="parts" select="@viewCondition"/>
			</xsl:if>
			<!--  there should be a new field in configuration describing the left/right position  -->
			<fields>
				<xsl:apply-templates select="columns/column" mode="view">
					<xsl:with-param name="general_name" select="@name"/>
				</xsl:apply-templates>
			</fields>
		</fieldGrid>
	</xsl:template>
		
	<xsl:template match="column" mode="view">
		<xsl:param name="general_name"/>
		<field>
			<xsl:attribute name="name">
				<xsl:choose>
					<xsl:when test="lower-case(@name)='eng_last_update_date'">
						<xsl:value-of select="'sys:eng_mtdt'"/>
					</xsl:when>
					<xsl:when test="lower-case(@name)='eng_source_system'">
						<xsl:value-of select="'sys:eng_system'"/>
					</xsl:when>
					<xsl:when test="lower-case(@name)='eng_active'">
						<xsl:value-of select="'sys:eng_active'"/>
					</xsl:when>
					<xsl:when test="lower-case(@name)='eng_modified_by'">
						<xsl:value-of select="'sys:eng_modified_by'"/>
					</xsl:when>										
					<xsl:otherwise>
						<xsl:value-of select="fn:lower-case(@name)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
<!-- 			<xsl:if test="lower-case(@name)='eng_last_update_date' or lower-case(@name)='eng_source_system' or lower-case(@name)='eng_active'">
				<xsl:attribute name="systemNamespace" select="'system'"/>
			</xsl:if> -->
			<xsl:if test="@lookupType!=''">
				<xsl:attribute name="lookupType">
					<xsl:value-of select="@lookupType"/>
				</xsl:attribute>
			</xsl:if> 
			<xsl:if test="@format!=''">
				<xsl:attribute name="format">
					<xsl:value-of select="@format"/>
				</xsl:attribute>
			</xsl:if> 
			<xsl:if test="@condition!=''">
				<xsl:attribute name="parts" select="@condition">
				</xsl:attribute>
			</xsl:if>
			
			<!-- <fieldGrid classes="mda-leftContentMargin" labelKey="Basic organisation information"> 
				<fields> 
					<field name="cmo_type" lookupType="COMBO"/> 
					<field name="cmo_legal_form" lookupType="COMBO"/> 
					<field name="cmo_established_date" format="yyyy-MM-dd"/>
				 </fields> 
				 </fieldGrid> -->
				<!-- there has to be the following attributes added: lookupType="COMBO" 
				format="yyyy-MM-dd" -->
		</field>
	</xsl:template>
	
	<xsl:template match="labeledRelatedMNList">
		<xsl:param name="table_name"/>
		<xsl:variable name="relName" select="@relationship"/>
		<xsl:variable name="relNameOut" select="@relationship_out"/>
		<xsl:variable name="entityOut" select="@entity_out"/>
		
		<xsl:variable name="childRel" select="sf:nvl($modelName/relationships/relationship[@name=$relName]/@childRole,concat('rev_',$relName))"/>
		<xsl:variable name="parentRel" select="sf:nvl($modelName/relationships/relationship[@name=$relName]/@parentRole,$relName)"/>
		<xsl:variable name="childRelOut" select="sf:nvl($modelName/relationships/relationship[@name=$relNameOut]/@childRole,concat('rev_',$relNameOut))"/>
		<xsl:variable name="parentRelOut" select="sf:nvl($modelName/relationships/relationship[@name=$relNameOut]/@parentRole,$relNameOut)"/>		
		
		<xsl:variable name="relationLeft">
			<xsl:value-of select="if($table_name=$modelName/relationships/relationship[@name=$relName]/@parentTable) then $childRel else $parentRel"/>
		</xsl:variable>
		<xsl:variable name="relationRight">
			<xsl:value-of select="if($entityOut=$modelName/relationships/relationship[@name=$relNameOut]/@parentTable) then $parentRelOut else $childRelOut"/>
		</xsl:variable>							
		
		<switchPanel classes="mda-{@templatePosition}ContentMargin">
			<xsl:if test="@viewCondition!=''">
				<xsl:attribute name="parts" select="@viewCondition"/>
			</xsl:if>
			<option activeLabel="{@label}" inactiveLabel="Show Linked Records">					
				<rowIterator relation="{$childRel}:{$childRelOut}" template="relatedRow.tmpl" emptyMessageKey="No records" previewLimit="{sf:nvl(@previewLimit,$guiConfig/guiPreferences/recordDetailVisualization/@previewLimit)}">
					<!-- rev_party_has_child_party:party_has_parent_party -->
					<templateParameters>
						<parameter name="firstColumnFormat" value="{replace(replace(replace(MNlinkedRecordsParams/firstColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}" /> <!-- {@columnLabel}: {@columnName} -->
						<parameter name="secondColumnFormat" value="{replace(replace(replace(MNlinkedRecordsParams/secondColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}"/> <!-- Company: ${cmo_company_name} -->
					</templateParameters>
					<header>
		                <element action="compare_records" classes="headerButton headerButtonAction-instance-compare" id="{$table_name}_instance_action_compare"
		                         label="Compare"
		                         tooltip="Compare records">
		                    <enablement>
		                        <selection min="2"/>
		                    </enablement>
		                </element>
		                <element action="list_records" classes="headerButton headerButtonAction-instance-list" id="{$table_name}_instance_action_list" label="List"
		                         tooltip="Show records in list">
		                    <enablement>
		                        <selection min="2"/>
		                    </enablement>
		                </element>
                        <element action="showAll" classes="headerButton headerButtonShowAll" label="View all" tooltip="Show all records"/>
                    </header>
					<selectionModel type="multi"/>
					<!-- <selectionModel type="multi" mode="edit" /> -->
				</rowIterator>
			</option>
			<option activeLabel="{@label} Relationships" inactiveLabel="Show Relationships">
				<rowIterator relation="{$childRel}" template="relatedRow.tmpl" emptyMessageKey="No records" previewLimit="{sf:nvl(@previewLimit,$guiConfig/guiPreferences/recordDetailVisualization/@previewLimit)}">
					<templateParameters>
						<parameter name="firstColumnFormat" value="{replace(replace(replace(MNrelationParams/firstColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}" /> <!-- ParentId: ${{parent_id}} | RelType: ${{cmo_p2p_rel_type}} -->
						<parameter name="secondColumnFormat" value="{replace(replace(replace(MNrelationParams/secondColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}"/>  <!--  Active: ${{eng_active}} | Updated: ${{eng_mtdt}} -->
					</templateParameters>
					<header>
		                <element action="compare_records" classes="headerButton headerButtonAction-instance-compare" id="{$table_name}_instance_action_compare"
		                         label="Compare"
		                         tooltip="Compare records">
		                    <enablement>
		                        <selection min="2"/>
		                    </enablement>
		                </element>
		                <element action="list_records" classes="headerButton headerButtonAction-instance-list" id="{$table_name}_instance_action_list" label="List"
		                         tooltip="Show records in list">
		                    <enablement>
		                        <selection min="2"/>
		                    </enablement>
		                </element>
                        <element action="showAll" classes="headerButton headerButtonShowAll" label="View all" tooltip="Show all records"/>
						<element action="deactivate" classes="headerButtonActionDeactivate" tooltip="Deactivate selected records">
							<enablement>
								<modes>
									<mode>edit</mode>
									<mode>activation</mode>
								</modes>
								<selection min="1" />
							</enablement>
						</element>
						<element action="activate" classes="headerButtonActionActivate" tooltip="Activate selected records">
		                      <enablement>
		                          <modes>
								  	<mode>edit</mode>
									<mode>activation</mode>
								  </modes>
		                          <selection min="1" />
		                      </enablement>
               			</element>
					</header>
					<selectionModel type="multi"/>
				</rowIterator>
			</option>
		</switchPanel>
	</xsl:template>

	<xsl:template match="labeledRelatedMNList" mode="inst">
		<xsl:param name="table_name"/>
		<xsl:param name="instanceModel"/>
		<xsl:variable name="relName" select="@relationship"/>
		<xsl:variable name="relNameOut" select="@relationship_out"/>
		<xsl:variable name="entityOut" select="@entity_out"/>
		
		<xsl:variable name="childRel" select="sf:nvl($instanceModel/relationships/relationship[@name=$relName]/@childRole,concat('rev_',$relName))"/>
		<xsl:variable name="parentRel" select="sf:nvl($instanceModel/relationships/relationship[@name=$relName]/@parentRole,$relName)"/>
		<xsl:variable name="childRelOut" select="sf:nvl($instanceModel/relationships/relationship[@name=$relNameOut]/@childRole,concat('rev_',$relNameOut))"/>
		<xsl:variable name="parentRelOut" select="sf:nvl($instanceModel/relationships/relationship[@name=$relNameOut]/@parentRole,$relNameOut)"/>		
		
		<xsl:variable name="relationLeft">
			<xsl:value-of select="if($table_name=$instanceModel/relationships/relationship[@name=$relName]/@parentTable) then $childRel else $parentRel"/>
		</xsl:variable>
		<xsl:variable name="relationRight">
			<xsl:value-of select="if($entityOut=$instanceModel/relationships/relationship[@name=$relNameOut]/@parentTable) then $parentRelOut else $childRelOut"/>
		</xsl:variable>	
		
		<switchPanel classes="mda-{@templatePosition}ContentMargin">
				<xsl:if test="@viewCondition!=''">
					<xsl:attribute name="parts" select="@viewCondition"/>
				</xsl:if>
			<option activeLabel="{@label}" inactiveLabel="Show Linked Records">									
				<rowIterator relation="{$childRel}:{$childRelOut}" template="relatedRow.tmpl" emptyMessageKey="No records" previewLimit="{sf:nvl(@previewLimit,$guiConfig/guiPreferences/recordDetailVisualization/@previewLimit)}">
					<!-- rev_party_has_child_party:party_has_parent_party -->
					<templateParameters>
						<parameter name="firstColumnFormat" value="{replace(replace(replace(MNlinkedRecordsParams/firstColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}" /> <!-- {@columnLabel}: {@columnName} -->
						<parameter name="secondColumnFormat" value="{replace(replace(replace(MNlinkedRecordsParams/secondColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}"/> <!-- Company: ${cmo_company_name} -->
					</templateParameters>
					<header>
		                <element action="compare_records" classes="headerButton headerButtonAction-instance-compare" id="{$table_name}_instance_action_compare"
		                         label="Compare"
		                         tooltip="Compare records">
		                    <enablement>
		                        <selection min="2"/>
		                    </enablement>
		                </element>
		                <element action="list_records" classes="headerButton headerButtonAction-instance-list" id="{$table_name}_instance_action_list" label="List"
		                         tooltip="Show records in list">
		                    <enablement>
		                        <selection min="2"/>
		                    </enablement>
		                </element>					
						<element label="View all" action="showAll" classes="headerButton headerButtonShowAll" tooltip="Show all records"/>
					</header>
					<!-- <selectionModel type="multi" mode="edit" /> -->
					<selectionModel type="multi"/>
				</rowIterator>
			</option>
			<option activeLabel="{@label} Relationships" inactiveLabel="Show Relationships">
				<rowIterator relation="{$childRel}" template="relatedRow.tmpl" emptyMessageKey="No records" previewLimit="{sf:nvl(@previewLimit,$guiConfig/guiPreferences/recordDetailVisualization/@previewLimit)}">
					<templateParameters>
						<parameter name="firstColumnFormat" value="{replace(replace(replace(MNrelationParams/firstColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}" /> <!-- ParentId: ${{parent_id}} | RelType: ${{cmo_p2p_rel_type}} -->
						<parameter name="secondColumnFormat" value="{replace(replace(replace(MNrelationParams/secondColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}"/>  <!--  Active: ${{eng_active}} | Updated: ${{eng_mtdt}} -->
					</templateParameters>
					<header>
		                <element action="compare_records" classes="headerButton headerButtonAction-instance-compare" id="{$table_name}_instance_action_compare"
		                         label="Compare"
		                         tooltip="Compare records">
		                    <enablement>
		                        <selection min="2"/>
		                    </enablement>
		                </element>
		                <element action="list_records" classes="headerButton headerButtonAction-instance-list" id="{$table_name}_instance_action_list" label="List"
		                         tooltip="Show records in list">
		                    <enablement>
		                        <selection min="2"/>
		                    </enablement>
		                </element>
						<element label="View all" action="showAll" classes="headerButton headerButtonShowAll" tooltip="Show all records"/>
						<element action="deactivate" classes="headerButtonActionDeactivate" tooltip="Deactivate selected records">
							<enablement>
								<modes>
									<mode>edit</mode>
									<mode>activation</mode>
								</modes>
								<selection min="1" />
							</enablement>
						</element>
						<element action="activate" classes="headerButtonActionActivate" tooltip="Activate selected records">
		                      <enablement>
		                          <modes>
								  	<mode>edit</mode>
									<mode>activation</mode>
								  </modes>
		                          <selection min="1" />
		                      </enablement>
               			</element>
					</header>
					<selectionModel type="multi"/>
				</rowIterator>
			</option>
		</switchPanel>
	</xsl:template>
		
	<xsl:template match="labeledRelatedMNList" mode="sor">
		<xsl:param name="sorModel"/>
		<xsl:param name="table_name"/>
		<xsl:variable name="relName" select="@relationship"/>
		<xsl:variable name="relNameOut" select="@relationship_out"/>
		<xsl:variable name="entityOut" select="@entity_out"/>
		
		<xsl:variable name="childRel" select="sf:nvl($sorModel/relationships/relationship[@name=$relName]/@childRole,concat('rev_',$relName))"/>
		<xsl:variable name="parentRel" select="sf:nvl($sorModel/relationships/relationship[@name=$relName]/@parentRole,$relName)"/>
		<xsl:variable name="childRelOut" select="sf:nvl($sorModel/relationships/relationship[@name=$relNameOut]/@childRole,concat('rev_',$relNameOut))"/>
		<xsl:variable name="parentRelOut" select="sf:nvl($sorModel/relationships/relationship[@name=$relNameOut]/@parentRole,$relNameOut)"/>		
		
		<xsl:variable name="relationLeft">
			<xsl:value-of select="if($table_name=$sorModel/relationships/relationship[@name=$relName]/@parentTable) then $childRel else $parentRel"/>
		</xsl:variable>
		<xsl:variable name="relationRight">
			<xsl:value-of select="if($entityOut=$sorModel/relationships/relationship[@name=$relNameOut]/@parentTable) then $parentRelOut else $childRelOut"/>
		</xsl:variable>	
		
		<switchPanel classes="mda-{@templatePosition}ContentMargin">
				<xsl:if test="@viewCondition!=''">
					<xsl:attribute name="parts" select="@viewCondition"/>
				</xsl:if>
			<option activeLabel="{@label}" inactiveLabel="Show Linked Records">									
				<rowIterator relation="{$childRel}:{$childRelOut}" template="relatedRow.tmpl" emptyMessageKey="No records" previewLimit="{sf:nvl(@previewLimit,$guiConfig/guiPreferences/recordDetailVisualization/@previewLimit)}">
					<!-- rev_party_has_child_party:party_has_parent_party -->
					<templateParameters>
						<parameter name="firstColumnFormat" value="{replace(replace(replace(MNlinkedRecordsParams/firstColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}" /> <!-- {@columnLabel}: {@columnName} -->
						<parameter name="secondColumnFormat" value="{replace(replace(replace(MNlinkedRecordsParams/secondColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}"/> <!-- Company: ${cmo_company_name} -->
					</templateParameters>
					<header>
		                <element action="compare_records" classes="headerButton headerButtonAction-instance-compare" id="{$table_name}_instance_action_compare"
		                         label="Compare"
		                         tooltip="Compare records">
		                    <enablement>
		                        <selection min="2"/>
		                    </enablement>
		                </element>
		                <element action="list_records" classes="headerButton headerButtonAction-instance-list" id="{$table_name}_instance_action_list" label="List"
		                         tooltip="Show records in list">
		                    <enablement>
		                        <selection min="2"/>
		                    </enablement>
		                </element>					
						<element label="View all" action="showAll" classes="headerButton headerButtonShowAll" tooltip="Show all records"/>
					</header>
					<selectionModel type="multi"/>
				</rowIterator>
			</option>
			<option activeLabel="{@label} Relationships" inactiveLabel="Show Relationships">
				<rowIterator relation="{$childRel}" template="relatedRow.tmpl" emptyMessageKey="No records" previewLimit="{sf:nvl(@previewLimit,$guiConfig/guiPreferences/recordDetailVisualization/@previewLimit)}">
					<templateParameters>
						<parameter name="firstColumnFormat" value="{replace(replace(replace(MNrelationParams/firstColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}" /> <!-- ParentId: ${{parent_id}} | RelType: ${{cmo_p2p_rel_type}} -->
						<parameter name="secondColumnFormat" value="{replace(replace(replace(MNrelationParams/secondColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}"/>  <!--  Active: ${{eng_active}} | Updated: ${{eng_mtdt}} -->
					</templateParameters>
					<header>
						<element action="create" classes="headerButton headerButtonActionAdd" label="Add record" tooltip="Adds new record to the relation">
						   <enablement>
							  <modes>
								<mode>edit</mode>
							</modes>
						   </enablement>
						</element>		
		                <element action="compare_records" classes="headerButton headerButtonAction-instance-compare" id="{$table_name}_instance_action_compare"
		                         label="Compare"
		                         tooltip="Compare records">
		                    <enablement>
		                        <selection min="2"/>
		                    </enablement>
		                </element>
		                <element action="list_records" classes="headerButton headerButtonAction-instance-list" id="{$table_name}_instance_action_list" label="List"
		                         tooltip="Show records in list">
		                    <enablement>
		                        <selection min="2"/>
		                    </enablement>
		                </element>									
						<element label="View all" action="showAll" classes="headerButton headerButtonShowAll" tooltip="Show all records"/>
						<element action="deactivate" classes="headerButtonActionDeactivate" tooltip="Deactivate selected records">
							<enablement>
								<modes>
									<mode>edit</mode>
									<mode>activation</mode>
								</modes>
								<selection min="1" />
							</enablement>
						</element>
						<element action="activate" classes="headerButtonActionActivate" tooltip="Activate selected records">
		                      <enablement>
		                          <modes>
								  	<mode>edit</mode>
									<mode>activation</mode>
								  </modes>
		                          <selection min="1" />
		                      </enablement>
               			</element>
					</header>
					<selectionModel type="multi"/>
				</rowIterator>
			</option>
		</switchPanel>
	</xsl:template>		
		
	<xsl:template match="labeledRelatedList">
		<!-- <xsl:param name="templatePosition"/> -->
		<xsl:param name="table_name"/>
		<xsl:variable name="relName" select="@relationship"/>
		<rowIterator classes="mda-{@templatePosition}ContentMargin" relation="{sf:nvl($modelName/relationships/relationship[@name=$relName]/@childRole,concat('rev_',@relationship))}" template="relatedRow.tmpl" labelKey="{@label}" emptyMessageKey="No records" previewLimit="{sf:nvl(@previewLimit,$guiConfig/guiPreferences/recordDetailVisualization/@previewLimit)}">
			<xsl:if test="@viewCondition!=''">
				<xsl:attribute name="parts" select="@viewCondition"/>
			</xsl:if>
			<templateParameters>
				<parameter name="firstColumnFormat" value="{replace(replace(replace(linkedRecordParams/firstColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}" /> <!-- {@columnLabel}: {@columnName} -->
				<parameter name="secondColumnFormat" value="{replace(replace(replace(linkedRecordParams/secondColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}"/> <!-- Company: ${cmo_company_name} -->
			</templateParameters>
			<header>
				<element label="{@label}" classes="" />
                <element action="compare_records" classes="headerButton headerButtonAction-instance-compare" id="{$table_name}_instance_action_compare"
                         label="Compare"
                         tooltip="Compare records">
                    <enablement>
                        <selection min="2"/>
                    </enablement>
                </element>
                <element action="list_records" classes="headerButton headerButtonAction-instance-list" id="{$table_name}_instance_action_list" label="List"
                         tooltip="Show records in list">
                    <enablement>
                        <selection min="2"/>
                    </enablement>
                </element>
				<element label="View all" action="showAll" classes="headerButton headerButtonShowAll" tooltip="Show all records"/>
				<element action="deactivate" classes="headerButtonActionDeactivate" tooltip="Deactivate selected records">
					<enablement>
						<modes>
						 	<mode>edit</mode>
							<mode>activation</mode>
						</modes>
						<selection min="1" />
					</enablement>
				</element>
				<element action="activate" classes="headerButtonActionActivate" tooltip="Activate selected records">
                      <enablement>
                          <modes>
						  	<mode>edit</mode>
							<mode>activation</mode>
						  </modes>
                          <selection min="1" />
                      </enablement>
                </element>
			</header>
			<selectionModel type="multi" />
		</rowIterator>
	</xsl:template>
	
	<xsl:template match="labeledRelatedList" mode="instance">
		<!-- <xsl:param name="templatePosition"/> -->
		<xsl:param name="instanceModel"/>
		<xsl:param name="table_name"/>
		<xsl:variable name="relName" select="@relationship"/>
		<rowIterator classes="mda-{@templatePosition}ContentMargin" relation="{sf:nvl($instanceModel/relationships/relationship[@name=$relName]/@childRole,concat('rev_',@relationship))}" template="relatedRow.tmpl" labelKey="{@label}" emptyMessageKey="No records" previewLimit="{sf:nvl(@previewLimit,$guiConfig/guiPreferences/recordDetailVisualization/@previewLimit)}">
			<xsl:if test="@viewCondition!=''">
				<xsl:attribute name="parts" select="@viewCondition"/>
			</xsl:if>
			<templateParameters>
				<parameter name="firstColumnFormat" value="{replace(replace(replace(linkedRecordParams/firstColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}" /> <!-- {@columnLabel}: {@columnName} -->
				<parameter name="secondColumnFormat" value="{replace(replace(replace(linkedRecordParams/secondColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}"/> <!-- Company: ${cmo_company_name} -->
			</templateParameters>
			<header>
				<element label="{@label}" classes="" />
                <element action="compare_records" classes="headerButton headerButtonAction-instance-compare" id="{$table_name}_instance_action_compare"
                         label="Compare"
                         tooltip="Compare records">
                    <enablement>
                        <selection min="2"/>
                    </enablement>
                </element>
                <element action="list_records" classes="headerButton headerButtonAction-instance-list" id="{$table_name}_instance_action_list" label="List"
                         tooltip="Show records in list">
                    <enablement>
                        <selection min="2"/>
                    </enablement>
                </element>
				<element label="View all" action="showAll" classes="headerButton headerButtonShowAll" tooltip="Show all records"/>
				<element action="deactivate" classes="headerButtonActionDeactivate" tooltip="Deactivate selected records">
					<enablement>
						<modes>
						 	<mode>edit</mode>
							<mode>activation</mode>
						</modes>
						<selection min="1" />
					</enablement>
				</element>
				<element action="activate" classes="headerButtonActionActivate" tooltip="Activate selected records">
                      <enablement>
                          <modes>
						  	<mode>edit</mode>
							<mode>activation</mode>
						  </modes>
                          <selection min="1" />
                      </enablement>
                </element>
			</header>
			<selectionModel type="multi" />
		</rowIterator>
	</xsl:template>	
		
	<xsl:template match="labeledRelatedList" mode="sor">
		<xsl:param name="sorModel"/>
		<xsl:param name="table_name"/>
		<xsl:variable name="relName" select="@relationship"/>
		<rowIterator classes="mda-{@templatePosition}ContentMargin" relation="{sf:nvl($sorModel/relationships/relationship[@name=$relName]/@childRole,concat('rev_',@relationship))}" template="relatedRow.tmpl" labelKey="{@label}" emptyMessageKey="No records" previewLimit="{sf:nvl(@previewLimit,$guiConfig/guiPreferences/recordDetailVisualization/@previewLimit)}">
			<xsl:if test="@viewCondition!=''">
				<xsl:attribute name="parts" select="@viewCondition"/>
			</xsl:if>
			<templateParameters>
				<parameter name="firstColumnFormat" value="{replace(replace(replace(linkedRecordParams/firstColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}" /> <!-- {@columnLabel}: {@columnName} -->
				<parameter name="secondColumnFormat" value="{replace(replace(replace(linkedRecordParams/secondColumnFormat,'eng_','sys:eng_'),'eng_last_update_date','eng_mtdt'),'eng_source_system','eng_system')}"/> <!-- Company: ${cmo_company_name} -->
			</templateParameters>
			<header>
				<element label="{@label}" classes="" />
				<element action="create" classes="headerButton headerButtonActionAdd" label="Add record" tooltip="Adds new record to the relation">
				   <enablement>
					  <modes>
						<mode>edit</mode>
					</modes>
				   </enablement>
				</element>				
				<element label="View all" action="showAll" classes="headerButton headerButtonShowAll" tooltip="Show all records"/>
				<element action="deactivate" classes="headerButtonActionDeactivate" tooltip="Deactivate selected records">
					<enablement>
						<modes>
						 	<mode>edit</mode>
							<mode>activation</mode>
						</modes>
						<selection min="1" />
					</enablement>
				</element>
				<element action="activate" classes="headerButtonActionActivate" tooltip="Activate selected records">
                      <enablement>
                          <modes>
						  	<mode>edit</mode>
							<mode>activation</mode>
						  </modes>
                          <selection min="1" />
                      </enablement>
                </element>
			</header>
			<selectionModel type="multi" />
		</rowIterator>
	</xsl:template>		
		
	<xsl:template match="listGrid">
		<xsl:param name="masterModels"/>
		<xsl:param name="model_name"/>
		<xsl:param name="table_name"/>
		<xsl:if test="lists/list or @viewCondition!=''">
	        <switchPanel>
	        	<xsl:attribute name="classes">
	        		<xsl:text>mda-</xsl:text><xsl:value-of select="fn:replace(@templatePosition,'bottom','leftRight')"/><xsl:text>ContentMargin</xsl:text>
	        	</xsl:attribute> 
				<xsl:if test="@viewCondition!=''">
					<xsl:attribute name="parts" select="@viewCondition"/>
				</xsl:if>
				<xsl:call-template name="option">
					<xsl:with-param name="listGrid" select="."/>
					<xsl:with-param name="masterModels" select="$masterModels"/>
					<xsl:with-param name="model_name" select="$model_name"/>
					<xsl:with-param name="table_name" select="$table_name"/> <!---->
	          	</xsl:call-template>
			</switchPanel>
		</xsl:if>
	</xsl:template>

	<xsl:template name="option">
		<xsl:param name="listGrid" />
		<xsl:param name="masterModels"/>
		<xsl:param name="model_name"/>
		<xsl:param name="table_name"/>
		<xsl:for-each select="$listGrid/lists/list">
			<option activeLabel="{@label}" inactiveLabel="{@label}">
				<xsl:if test="@condition!=''">
					<xsl:attribute name="parts" select="@condition"/>
				</xsl:if>
				<rowGrid>
					<xsl:attribute name="relation" select="sf:nvl($masterModels/masterModel[@name=$model_name]/relationships/relationship[@name=$listGrid/@relationship and @parentTable=$table_name]/@childRole,concat('rev_',$listGrid/@relationship))"/>
					<!-- <xsl:attribute name="relation" select="concat('rev_',$listGrid/@relationship)"/> -->
					<xsl:attribute name="gridHeight" select="'auto'"/>
				<xsl:if test="@columnMask!=''">
					<xsl:attribute name="columnMask">
						<xsl:value-of select="@columnMask"/>
					</xsl:attribute>
				</xsl:if>
					<columns>
						<xsl:for-each select="columns/column">
							<column>							
								<xsl:attribute name="name">
									<xsl:choose>
										<xsl:when test="lower-case(@name)='eng_last_update_date'">
											<xsl:value-of select="'sys:eng_mtdt'"/>
										</xsl:when>
										<xsl:when test="lower-case(@name)='eng_source_system'">
											<xsl:value-of select="'sys:eng_system'"/>
										</xsl:when>
										<xsl:when test="lower-case(@name)='eng_active'">
											<xsl:value-of select="'sys:eng_active'"/>
										</xsl:when>		
										<xsl:otherwise>
											<xsl:value-of select="fn:lower-case(@name)"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
								<xsl:if test="@condition!=''">
									<xsl:attribute name="parts" select="@condition"/>
								</xsl:if>								
								<!-- <xsl:if test="lower-case(@name)='eng_last_update_date' or lower-case(@name)='eng_source_system' or lower-case(@name)='eng_active'">
									<xsl:attribute name="systemNamespace" select="'system'"/>
								</xsl:if>		 -->													
							</column>
						</xsl:for-each>
	                </columns>
					<actions>
						<action action="openDetail" />
					</actions>
					<!-- matching is on, manualMatch is on and the entity is TLE -->
					<xsl:if test="$masterModels/masterModel[@name=$model_name and $guiConfig/actions/@manualExceptions='true']/masterTables/masterTable[@name=$table_name]/@topLevel='true'">
						<xsl:call-template name="manualMatch">
							<xsl:with-param name="table_name" select="$table_name"/>
						</xsl:call-template>
	                 </xsl:if>
				</rowGrid>
			</option>
		</xsl:for-each>	
	</xsl:template>

	<xsl:template name="manualMatch">
		<xsl:param name="table_name" />
		<selectionModel type="multi"/>
            <header>
                <element action="compare_records" classes="headerButton headerButtonAction-instance-compare" id="{$table_name}_instance_action_compare"
                         label="Compare"
                         tooltip="Compare records">
                    <enablement>
                        <selection min="2"/>
                    </enablement>
                </element>
                <element action="list_records" classes="headerButton headerButtonAction-instance-list" id="{$table_name}_instance_action_list" label="List"
                         tooltip="Show records in list">
                    <enablement>
                        <selection min="2"/>
                    </enablement>
                </element>
				<element action="instance_split" classes="headerButton headerButtonAction-instance-drop" id="{$table_name}_instance_action_split" label="Split"
				         tooltip="Split &amp; isolate records">
				    <enablement>
				        <selection min="1"/>
				    </enablement>
				</element>
            </header> 
	</xsl:template>
</xsl:stylesheet>