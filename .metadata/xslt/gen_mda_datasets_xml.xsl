<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" cdata-section-elements="sql"/>
	<xsl:param name="databaseModel" select="document('param:databaseModel')/*"/>

	<xsl:template match="/*">
		<dataSets>
			<dataSets>
				<xsl:for-each select="datasetsArray/datasetArray[@enable='true']">
					<dataset name="{@name}">
						<columns>
							<column name="id" sourceName="ID" type="LONG" alwaysOrder="asc" />
							
							<xsl:for-each select="instanceTables/instanceTable">
								<xsl:variable name="thisTable" select="@entityName"/>
								<xsl:variable name="thisColumns" select="columns"/>
								<xsl:for-each select="columns/column">
									<xsl:variable name="thisColumn" select="@name"/>
									<xsl:variable name="ccColumn" select="cc/@name"/>
									<xsl:for-each select="$databaseModel/instanceTables/physicalTable[@name=$thisTable]/columns/column[@name=$thisColumn]">				
										<column name="{$ccColumn}" sourceName="{upper-case($ccColumn)}" type="{upper-case(@dqcType)}"/>
									</xsl:for-each>
								</xsl:for-each>
							</xsl:for-each>
							
							<xsl:for-each select="masterTables/masterTable">
								<xsl:variable name="thisTable" select="cc/@entity"/>
								<xsl:variable name="thisLayer" select="cc/@layer"/>
								<xsl:variable name="thisColumns" select="columns"/>
								<xsl:for-each select="columns/column">
									<xsl:variable name="thisColumn" select="@name"/>
									<xsl:variable name="ccColumn" select="cc/@name"/>
									<xsl:for-each select="$databaseModel/masterTables/physicalTable[@name=$thisTable and @layerName=$thisLayer]/columns/column[@name=$thisColumn]">	
										<column name="{$ccColumn}" sourceName="{upper-case($ccColumn)}" type="{upper-case(@dqcType)}"/>
									</xsl:for-each>
								</xsl:for-each>
							</xsl:for-each>
							
							<xsl:for-each select="sorTables/sorTable">
								<xsl:variable name="thisTable" select="@entityName"/>
								<xsl:variable name="thisColumns" select="columns"/>
								<xsl:for-each select="columns/column">
									<xsl:variable name="thisColumn" select="@name"/>
									<xsl:variable name="ccColumn" select="cc/@name"/>
									<xsl:for-each select="$databaseModel/sorTables/physicalTable[@name=$thisTable]/columns/column[@name=$thisColumn]">		
										<column name="{$ccColumn}" sourceName="{upper-case($ccColumn)}" type="{upper-case(@dqcType)}"/>
									</xsl:for-each>
								</xsl:for-each>
							</xsl:for-each>							
							
							<xsl:for-each select="sqlSource/columns/column">
								<column name="{@name}" sourceName="{upper-case(@name)}" type="{upper-case(@type)}"/>		
							</xsl:for-each>								
						</columns>
						<xsl:choose>
							<xsl:when test="instanceTables/instanceTable/@useAsDetail='true'">
								<detail type="INSTANCE" name="{instanceTables/instanceTable[@useAsDetail='true']/@entityName}" masterLayer="" idColumn="id" />	
							</xsl:when>
							<xsl:when test="sorTables/sorTable/@useAsDetail='true'">
								<detail type="SOR" name="{sorTables/sorTable[@useAsDetail='true']/@entityName}" masterLayer="" idColumn="id" />	
							</xsl:when>							
							<xsl:otherwise>
								<detail type="MASTER" name="{masterTables/masterTable[@useAsDetail='true']/cc/@entity}" masterLayer="{masterTables/masterTable[@useAsDetail='true']/cc/@layer}" idColumn="id" />
							</xsl:otherwise>
						</xsl:choose>
						<sqlSource><xsl:value-of select="sf:nvl(sqlSource/sqlOver/sql,sqlSource/cc/sql)" disable-output-escaping='no'/></sqlSource>
					</dataset>
				</xsl:for-each>
			</dataSets>
		</dataSets>		
	</xsl:template>
</xsl:stylesheet>