<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>

<xsl:template match="/*">
	<sampleDefinitions>
	<xsl:if test="@enable='true'">
		<xsl:if test="@sampleSize!=''">
			<xsl:attribute name="sampleSize">
				<xsl:value-of select="@sampleSize"/>
			</xsl:attribute>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="@sampleStrategy='Basic Strategy'">
				<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="true"/>
			</xsl:when>
			<xsl:when test="@sampleStrategy='Statistic Strategy'">
				<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleStatisticStrategy" useSampleThreshold="{@useSampleThreshold}"/>
			</xsl:when>
			<xsl:when test="@sampleStrategy='No Strategy'">
				<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="false"/>
			</xsl:when>			
		</xsl:choose>
		
		<xsl:if test="sampleLayerSettings/sampleLayerSetting/@layer='instance' or $logicalModel/instanceModel/tables/table/guiTab/sampleSetting/@enable='true'">
			<instanceLayer>
				<xsl:if test="sampleLayerSettings/sampleLayerSetting[@layer='instance']/@sampleSize!=''">
					<xsl:attribute name="sampleSize">
						<xsl:value-of select="sampleLayerSettings/sampleLayerSetting[@layer='instance']/@sampleSize"/>
					</xsl:attribute>
				</xsl:if>			
				<xsl:if test="sampleLayerSettings/sampleLayerSetting/@layer='instance'">
					<xsl:choose>
						<xsl:when test="sampleLayerSettings/sampleLayerSetting[@layer='instance']/@sampleStrategy='Basic Strategy'">
							<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="true"/>
						</xsl:when>
						<xsl:when test="sampleLayerSettings/sampleLayerSetting[@layer='instance']/@sampleStrategy='Statistic Strategy'">
							<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleStatisticStrategy" useSampleThreshold="{sampleLayerSettings/sampleLayerSetting[@layer='instance']/@useSampleThreshold}"/>
						</xsl:when>
						<xsl:when test="sampleLayerSettings/sampleLayerSetting[@layer='instance']/@sampleStrategy='No Strategy'">
							<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="false"/>
						</xsl:when>						
					</xsl:choose>					
				</xsl:if>
				<xsl:if test="$logicalModel/instanceModel/tables/table/guiTab/sampleSetting/@enable='true'">
					<entities>
						<xsl:for-each select="$logicalModel/instanceModel/tables/table[guiTab/sampleSetting/@enable='true']">						
							<entity name="{@name}">
								<xsl:if test="guiTab/sampleSetting/@sampleSize!=''">
									<xsl:attribute name="sampleSize">
										<xsl:value-of select="guiTab/sampleSetting/@sampleSize"/>
									</xsl:attribute>
								</xsl:if>
								<xsl:choose>
									<xsl:when test="guiTab/sampleSetting/@sampleStrategy='Basic Strategy'">
										<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="true"/>
									</xsl:when>
									<xsl:when test="guiTab/sampleSetting/@sampleStrategy='Statistic Strategy'">
										<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleStatisticStrategy" useSampleThreshold="{guiTab/sampleSetting/@useSampleThreshold}"/>
									</xsl:when>
									<xsl:when test="guiTab/sampleSetting/@sampleStrategy='No Strategy'">
										<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="false"/>
									</xsl:when>									
								</xsl:choose>		
							</entity>
						</xsl:for-each>
					</entities>				
				</xsl:if>
			</instanceLayer>	
		</xsl:if>
		
		<xsl:if test="sampleLayerSettings/sampleLayerSetting/@layer='dataset' or $logicalModel/datasets/datasetsArray/datasetArray/sampleSetting/@enable='true'">
			<dataSetLayer>
				<xsl:if test="sampleLayerSettings/sampleLayerSetting[@layer='dataset']/@sampleSize!=''">
					<xsl:attribute name="sampleSize">
						<xsl:value-of select="sampleLayerSettings/sampleLayerSetting[@layer='dataset']/@sampleSize"/>
					</xsl:attribute>
				</xsl:if>			
				<xsl:if test="sampleLayerSettings/sampleLayerSetting/@layer='dataset'">
					<xsl:choose>
						<xsl:when test="sampleLayerSettings/sampleLayerSetting[@layer='dataset']/@sampleStrategy='Basic Strategy'">
							<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="true"/>
						</xsl:when>
						<xsl:when test="sampleLayerSettings/sampleLayerSetting[@layer='dataset']/@sampleStrategy='No Strategy'">
							<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="false"/>
						</xsl:when>									
					</xsl:choose>						
				</xsl:if>
				<xsl:if test="$logicalModel/datasets/datasetsArray/datasetArray/sampleSetting/@enable='true'">
					<entities>
						<xsl:for-each select="$logicalModel/datasets/datasetsArray/datasetArray[sampleSetting/@enable='true']">						
							<entity name="{@name}">
								<xsl:if test="sampleSetting/@sampleSize!=''">
									<xsl:attribute name="sampleSize">
										<xsl:value-of select="sampleSetting/@sampleSize"/>
									</xsl:attribute>
								</xsl:if>							
								<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="{sampleSetting/@allowed}"/>	
							</entity>
						</xsl:for-each>
					</entities>				
				</xsl:if>
			</dataSetLayer>
		</xsl:if>

		<xsl:if test="sampleLayerSettings/sampleLayerSetting/@layer='sor' or $logicalModel/sorModel/tables/table/guiTab/sampleSetting/@enable='true'">
			<sorLayer>
				<xsl:if test="sampleLayerSettings/sampleLayerSetting[@layer='sor']/@sampleSize!=''">
					<xsl:attribute name="sampleSize">
						<xsl:value-of select="sampleLayerSettings/sampleLayerSetting[@layer='sor']/@sampleSize"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:if test="sampleLayerSettings/sampleLayerSetting/@layer='sor'">					
					<xsl:choose>
						<xsl:when test="sampleLayerSettings/sampleLayerSetting[@layer='sor']/@sampleStrategy='Basic Strategy'">
							<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="true"/>
						</xsl:when>
						<xsl:when test="sampleLayerSettings/sampleLayerSetting[@layer='sor']/@sampleStrategy='Statistic Strategy'">
							<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleStatisticStrategy" useSampleThreshold="{sampleLayerSettings/sampleLayerSetting[@layer='sor']/@useSampleThreshold}"/>
						</xsl:when>
						<xsl:when test="sampleLayerSettings/sampleLayerSetting[@layer='sor']/@sampleStrategy='No Strategy'">
							<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="false"/>
						</xsl:when>								
					</xsl:choose>		
				</xsl:if>
				<xsl:if test="$logicalModel/sorModel/tables/table/guiTab/sampleSetting/@enable='true'">
					<entities>
						<xsl:for-each select="$logicalModel/sorModel/tables/table[guiTab/sampleSetting/@enable='true']">						
							<entity name="{@name}">
								<xsl:if test="guiTab/sampleSetting/@sampleSize!=''">
									<xsl:attribute name="sampleSize">
										<xsl:value-of select="guiTab/sampleSetting/@sampleSize"/>
									</xsl:attribute>
								</xsl:if>							
								<xsl:choose>
									<xsl:when test="guiTab/sampleSetting/@sampleStrategy='Basic Strategy'">
										<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="true"/>
									</xsl:when>
									<xsl:when test="guiTab/sampleSetting/@sampleStrategy='Statistic Strategy'">
										<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleStatisticStrategy" useSampleThreshold="{guiTab/sampleSetting/@useSampleThreshold}"/>
									</xsl:when>
									<xsl:when test="guiTab/sampleSetting/@sampleStrategy='No Strategy'">
										<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="false"/>
									</xsl:when>											
								</xsl:choose>
							</entity>
						</xsl:for-each>
					</entities>				
				</xsl:if>
			</sorLayer>
		</xsl:if>

		<xsl:if test="sampleLayerSettings/sampleLayerSetting/@layer=$logicalModel/masterModels/masterModel/@name or $logicalModel/masterModels/masterModel/masterTables/masterTable/guiTab/sampleSetting/@enable='true' or $logicalModel/masterModels/masterModel/instanceTables/instanceTable/guiTab/sampleSetting/@enable='true'">
			<xsl:variable name="sampleLayerSettings" select="sampleLayerSettings"/>
			<masterLayers>
				<xsl:for-each select="$logicalModel/masterModels/masterModel">
					<xsl:variable name="layerName" select="@name"/>				
					<masterLayer name="{$layerName}">
						<xsl:if test="$sampleLayerSettings/sampleLayerSetting[@layer=$layerName]/@sampleSize!=''">
							<xsl:attribute name="sampleSize">
								<xsl:value-of select="$sampleLayerSettings/sampleLayerSetting[@layer='sor']/@sampleSize"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:if test="$sampleLayerSettings/sampleLayerSetting/@layer=$layerName">
							<xsl:choose>
								<xsl:when test="$sampleLayerSettings/sampleLayerSetting[@layer=$layerName]/@sampleStrategy='Basic Strategy'">
									<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="true"/>
								</xsl:when>
								<xsl:when test="$sampleLayerSettings/sampleLayerSetting[@layer=$layerName]/@sampleStrategy='Statistic Strategy'">
									<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleStatisticStrategy" useSampleThreshold="{$sampleLayerSettings/sampleLayerSetting[@layer=$layerName]/@useSampleThreshold}"/>
								</xsl:when>
								<xsl:when test="$sampleLayerSettings/sampleLayerSetting[@layer=$layerName]/@sampleStrategy='No Strategy'">
									<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="false"/>
								</xsl:when>										
							</xsl:choose>
						</xsl:if>										
						<xsl:if test="$logicalModel/masterModels/masterModel[@name=$layerName]/masterTables/masterTable/guiTab/sampleSetting/@enable='true' or $logicalModel/masterModels/masterModel[@name=$layerName]/instanceTables/instanceTable/guiTab/sampleSetting/@enable='true'">
							<entities>
								<xsl:for-each select="$logicalModel/masterModels/masterModel[@name=$layerName]/masterTables/masterTable[guiTab/sampleSetting/@enable='true']">
									<entity name="{@name}">
										<xsl:if test="guiTab/sampleSetting/@sampleSize!=''">
											<xsl:attribute name="sampleSize">
												<xsl:value-of select="guiTab/sampleSetting/@sampleSize"/>
											</xsl:attribute>
										</xsl:if>									
										<xsl:choose>
											<xsl:when test="guiTab/sampleSetting/@sampleStrategy='Basic Strategy'">
												<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="true"/>
											</xsl:when>
											<xsl:when test="guiTab/sampleSetting/@sampleStrategy='Statistic Strategy'">
												<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleStatisticStrategy" useSampleThreshold="{guiTab/sampleSetting/@useSampleThreshold}"/>
											</xsl:when>
											<xsl:when test="guiTab/sampleSetting/@sampleStrategy='No Strategy'">
												<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="false"/>
											</xsl:when>													
										</xsl:choose>
									</entity>
								</xsl:for-each>
								<xsl:for-each select="$logicalModel/masterModels/masterModel[@name=$layerName]/instanceTables/instanceTable[guiTab/sampleSetting/@enable='true']">
									<entity name="{@name}">
										<xsl:if test="guiTab/sampleSetting/@sampleSize!=''">
											<xsl:attribute name="sampleSize">
												<xsl:value-of select="guiTab/sampleSetting/@sampleSize"/>
											</xsl:attribute>
										</xsl:if>									
										<xsl:choose>
											<xsl:when test="guiTab/sampleSetting/@sampleStrategy='Basic Strategy'">
												<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="true"/>
											</xsl:when>
											<xsl:when test="guiTab/sampleSetting/@sampleStrategy='Statistic Strategy'">
												<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleStatisticStrategy" useSampleThreshold="{guiTab/sampleSetting/@useSampleThreshold}"/>
											</xsl:when>
											<xsl:when test="guiTab/sampleSetting/@sampleStrategy='No Strategy'">
												<sampleStrategy class="com.ataccama.mda.core.config.sample.MdaSampleBasicStrategy" allowed="false"/>
											</xsl:when>													
										</xsl:choose>
									</entity>
								</xsl:for-each>								
							</entities>
						</xsl:if>
					</masterLayer>
				</xsl:for-each>
			</masterLayers>
		</xsl:if>
	</xsl:if>	
	</sampleDefinitions>

</xsl:template>
</xsl:stylesheet>