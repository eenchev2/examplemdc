<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>	

	<xsl:param name="instanceModel" select="document('param:instanceModel')/*"/>
	<xsl:param name="thisTable" select="document('param:thisTable')/*"/>
	
	<xsl:template match="/*">
		<ccColumns>
			<xsl:for-each select="$instanceModel/relationships/relationship[@childTable=lower-case($thisTable/@name)]/parentToChild/column[@source=$instanceModel/tables/table[lower-case(@name)=$instanceModel/relationships/relationship[@childTable=lower-case($thisTable/@name)]/@parentTable]/matchingTab/multipleMatching/matchingDefinitions/matchingDefinition/lower-case(@masterIdColumn)]">
				<ccColumn name="{@name}"/>
			</xsl:for-each>
			<xsl:for-each select="$instanceModel/relationships/relationship[@childTable=lower-case($thisTable/@name)]/parentToChild/column[@source='master_id']">
				<ccColumn name="{@name}"/>
			</xsl:for-each>
		</ccColumns>
	</xsl:template>

</xsl:stylesheet>