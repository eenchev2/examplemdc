<?xml version='1.0' encoding='UTF-8'?>
<guiPreferences>
	<dataTypeFormats datetime="yyyy-MM-dd HH:mm:ss" boolean="" integer="####" float="######.###" day="yyyy-MM-dd" long="######"/>
	<sampleDefinitions sampleStrategy="" useSampleThreshold="10000" enable="false" sampleSize="30">
		<sampleLayerSettings/>
	</sampleDefinitions>
	<recordDetailVisualization previewLimit="5"/>
</guiPreferences>