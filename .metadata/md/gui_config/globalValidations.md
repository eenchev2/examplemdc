<?xml version='1.0' encoding='UTF-8'?>
<globalValidations>
	<globalValidationSettings>
		<attributeScoreThresholds warningErrorThreshold="10000000" infoWarningThreshold="1000"/>
		<recordScoreThresholds highDQThreshold="10000" lowDQThreshold="10000000"/>
		<validationIconsDQI high="" low="" medium=""/>
	</globalValidationSettings>
	<validationMessages/>
</globalValidations>