<?xml version='1.0' encoding='UTF-8'?>
<wfConfig>
	<workflowLabels waiting_for_publish="Waiting for Approval" enableCons="true" draft="In Progress" sor="SoR WF" return_draft="Back to In Progress" enableSor="true" consolidation="Consolidation WF" move_publish="Submit for Approval"/>
	<workflows/>
	<statuses/>
</wfConfig>