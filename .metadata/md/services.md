<?xml version='1.0' encoding='UTF-8'?>
<nativeServices>
	<consolidationServices>
		<basicServices processPurge="false" getModel="false" listInstance="true" processDelta="false" rwControl="false" batchControl="true" getOverride="false" genTraversal="true" listMaster="true" modelStat="false" getInstance="true" getMaster="true" processMatch="false"/>
		<configurableServices>
			<cleanseService elemId="27874877" enable="true" name="clean_party" entity="party" desc=""/>
		</configurableServices>
	</consolidationServices>
	<sorServiceTab listSor="true" getSor="true">
		<sorServices/>
	</sorServiceTab>
	<endpointsDefinition>
		<httpEndpoint elemId="27864899" enable="true" format="SOAP" listenerNames="all" pathPrefix="test"/>
	</endpointsDefinition>
</nativeServices>