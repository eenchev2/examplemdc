<?xml version='1.0' encoding='UTF-8'?>
<settings deletionStrategy="deactivate" enableRC="true" scope="active" batchSizeRC="100000">
	<lengthValidation databaseType="Oracle R9+" nmePrefixLength="2"/>
	<advancedSettings alternativeKeys="false" expStringLength="500" matchingCompatibility="false" srcStringLength="100">
		<ignoredComparisonColumns/>
	</advancedSettings>
</settings>