<?xml version='1.0' encoding='UTF-8'?>
<system elemId="27865847" name="crm">
	<description></description>
	<model>
		<relationships/>
		<tables>
			<table elemId="27865877" name="crm_customer">
				<description></description>
				<columns/>
			</table>
			<table elemId="27865906" name="crm_address">
				<description></description>
				<columns/>
			</table>
			<table elemId="27865935" name="crm_contact">
				<description></description>
				<columns/>
			</table>
		</tables>
	</model>
	<sourceMappings>
		<mapping elemId="27866051" customOrigin="" entity="party" table="crm_customer">
			<description></description>
		</mapping>
	</sourceMappings>
	<loadOperations>
		<fullLoad elemId="27866080" nameSuffix="full" allTables="true">
			<selectedTables/>
			<advanced deletionStrategy="use global setting (from Preferences)">
				<ignoredComparisonColumns/>
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</fullLoad>
	</loadOperations>
</system>