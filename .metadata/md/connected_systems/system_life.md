<?xml version='1.0' encoding='UTF-8'?>
<system elemId="27869124" name="life">
	<description></description>
	<model>
		<relationships/>
		<tables>
			<table elemId="27869304" name="life_party">
				<description></description>
				<columns/>
			</table>
		</tables>
	</model>
	<sourceMappings>
		<mapping elemId="27869408" customOrigin="" entity="party" table="life_party">
			<description></description>
		</mapping>
		<mapping elemId="27869411" customOrigin="" entity="address" table="life_party">
			<description></description>
		</mapping>
		<mapping elemId="27869412" customOrigin="" entity="contact" table="life_party">
			<description></description>
		</mapping>
	</sourceMappings>
	<loadOperations>
		<fullLoad elemId="27869464" nameSuffix="full" allTables="true">
			<selectedTables/>
			<advanced deletionStrategy="use global setting (from Preferences)">
				<ignoredComparisonColumns/>
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</fullLoad>
	</loadOperations>
</system>