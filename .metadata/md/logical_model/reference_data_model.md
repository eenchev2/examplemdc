<?xml version='1.0' encoding='UTF-8'?>
<dictionary>
	<tables>
		<table elemId="27870656" loadInst="false" usageValidation="none" name="gender" loadMas="true" masterCodeType="string" label="" instUse="cleansing" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="27870657" loadInst="true" usageValidation="none" name="party_type" loadMas="true" masterCodeType="string" label="" instUse="cleansing" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
	</tables>
</dictionary>