<?xml version='1.0' encoding='UTF-8'?>
<masterModel elemId="27867495" name="masters" label="">
	<description></description>
	<relationships/>
	<masterTables>
		<masterTable elemId="27867781" entityRole="golden" topLevel="false" name="party" label="" instanceTable="party">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="" type="long_int" isExp="false" elemId="27867929" isPk="true" size="" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="" type="string" isExp="false" elemId="27867930" isPk="false" size="30" enableValidation="false" name="type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="" type="string" isExp="false" elemId="27867931" isPk="false" size="100" enableValidation="false" name="first_name" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="" type="string" isExp="false" elemId="27867932" isPk="false" size="100" enableValidation="false" name="last_name" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="" type="string" isExp="false" elemId="27867933" isPk="false" size="10" enableValidation="false" name="gender" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="" type="day" isExp="false" elemId="27867934" isPk="false" size="" enableValidation="false" name="birth_date" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="" type="string" isExp="false" elemId="27867935" isPk="false" size="30" enableValidation="false" name="sin" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views/>
				<computedColumns/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression></inputFilterExpression>
				<specialColumns>
					<column elemId="27868294" size="" enableValidation="false" name="group_count" createInto="" label="" type="integer">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</specialColumns>
			</advanced>
		</masterTable>
	</masterTables>
	<instanceTables/>
</masterModel>