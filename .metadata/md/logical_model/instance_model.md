<?xml version='1.0' encoding='UTF-8'?>
<instanceModel label="">
	<relationships>
		<relationship childRole="addresses" childTable="address" elemId="27868225" name="party_has_address" parentRole="party" parentTable="party" type="Same System">
			<description></description>
			<childToParent>
				<column elemId="27878837" method="concatenateDistinct" size="1000" name="cio_address_comp_set" createInto="party_match" label="" source="cio_address_comp" type="string" separator="~">
					<filterExpression></filterExpression>
				</column>
			</childToParent>
			<parentToChild>
				<column elemId="27878520" size="" name="party_master_id" createInto="address_match" label="" source="master_id" type="long_int">
					<filterExpression></filterExpression>
				</column>
			</parentToChild>
			<advanced>
				<extendedSameSystem enable="false" childColumn="" parentColumn=""/>
				<alternativeKey alternativePk="" alternativeFk=""/>
			</advanced>
			<foreignKey>
				<column elemId="27868279" childColumn="party_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="contacts" childTable="contact" elemId="27868438" name="party_has_contact" parentRole="party" parentTable="party" type="Same System">
			<description></description>
			<childToParent>
				<column elemId="27879082" method="concatenateDistinct" size="1000" name="cio_contact_comp_set" createInto="party_match" label="" source="cio_contact_comp" type="string" separator="~">
					<filterExpression></filterExpression>
				</column>
			</childToParent>
			<parentToChild>
				<column elemId="27878766" size="" name="party_master_id" createInto="contact_match" label="" source="master_id" type="long_int">
					<filterExpression></filterExpression>
				</column>
			</parentToChild>
			<advanced>
				<extendedSameSystem enable="false" childColumn="" parentColumn=""/>
				<alternativeKey alternativePk="" alternativeFk=""/>
			</advanced>
			<foreignKey>
				<column elemId="27868551" childColumn="party_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
	</relationships>
	<tables>
		<table elemId="27865322" topLevel="false" name="party" label="" type="instance">
			<description></description>
			<columns>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="" type="string" isExp="false" elemId="27865408" isPk="true" size="200" enableValidation="false" name="source_id" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="" type="string" isExp="false" elemId="27865409" isPk="false" size="100" enableValidation="false" name="first_name" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="" type="string" isExp="false" elemId="27865410" isPk="false" size="100" enableValidation="false" name="middle_name" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="" type="string" isExp="false" elemId="27865411" isPk="false" size="100" enableValidation="false" name="last_name" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="" type="day" isExp="true" elemId="27865412" isPk="false" size="" enableValidation="false" name="birth_date" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="party_type" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="" type="string" isExp="true" elemId="27865413" isPk="false" size="30" enableValidation="false" name="type" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="gender" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="" type="string" isExp="true" elemId="27865414" isPk="false" size="10" enableValidation="false" name="gender" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="" type="string" isExp="true" elemId="27865415" isPk="false" size="30" enableValidation="false" name="sin" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="" type="string" isExp="true" elemId="27868702" isPk="false" size="200" enableValidation="false" name="company_name" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="" type="string" isExp="true" elemId="27868703" isPk="false" size="200" enableValidation="false" name="business_number" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="" type="string" isExp="false" elemId="27868704" isPk="false" size="30" enableValidation="false" name="legal_form" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="" type="day" isExp="true" elemId="27868705" isPk="false" size="" enableValidation="false" name="established_date" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<matchingTab enableIdentify="true" matching="true">
				<matchingTabColumns>
					<column elemId="27871315" size="100" enableValidation="false" name="uni_rul_name" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27871316" size="" enableValidation="false" name="uni_match_quality" label="" type="float" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27871317" size="" enableValidation="false" name="uni_match_id" label="" type="long_int" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27871318" size="1000" enableValidation="false" name="mat_party_type" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27871319" size="1000" enableValidation="false" name="mat_gender" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27871320" size="1000" enableValidation="false" name="mat_first_name" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27871321" size="1000" enableValidation="false" name="mat_middle_name" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27871322" size="1000" enableValidation="false" name="mat_last_name" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27871323" size="1000" enableValidation="false" name="mat_full_name" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27871324" size="1000" enableValidation="false" name="mat_initials" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27871325" size="1000" enableValidation="false" name="mat_person_id" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27871326" size="1000" enableValidation="false" name="mat_birth_date" label="" type="day" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27879146" size="1000" enableValidation="false" name="mat_company_name" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27879147" size="1000" enableValidation="false" name="mat_business_number" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27879148" size="1000" enableValidation="false" name="mat_address_set" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27879149" size="1000" enableValidation="false" name="mat_contact_set" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</matchingTabColumns>
				<multipleMatching disableDefault="false">
					<matchingDefinitions/>
					<rematchIfChangedSectionColumns/>
				</multipleMatching>
				<matchingProposals enable="false"/>
			</matchingTab>
			<aggregationTab groupingColumn="master_id" aggregation="true">
				<aggregationTabColumns>
					<aggregationTabColumn elemId="5848" size="" name="agg_group_size" label="" type="integer" isFk="false">
						<description></description>
					</aggregationTabColumn>
				</aggregationTabColumns>
			</aggregationTab>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views/>
				<computedColumns/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
			</guiTab>
			<advanced>
				<specialColumns>
					<column elemId="27865416" size="" enableValidation="false" name="sco_full_name" createInto="party_clean" label="" type="integer" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27865417" size="2000" enableValidation="false" name="exp_full_name" createInto="party_clean" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27865418" size="" enableValidation="false" name="sco_instance" createInto="party_clean" label="" type="integer" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27865419" size="20" enableValidation="false" name="pat_name" createInto="party_clean" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27865420" size="" enableValidation="false" name="mda_override" createInto="load" label="" type="boolean" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</specialColumns>
				<historicalColumns/>
				<oldValueColumns/>
			</advanced>
		</table>
		<table elemId="27866462" topLevel="false" name="address" label="" type="instance">
			<description></description>
			<columns>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="" type="string" isExp="false" elemId="27866844" isPk="true" size="200" enableValidation="false" name="source_id" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="" type="string" isExp="false" elemId="27866845" isPk="false" size="200" enableValidation="false" name="party_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="" type="string" isExp="true" elemId="27866846" isPk="false" size="30" enableValidation="false" name="type" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="" type="string" isExp="false" elemId="27866847" isPk="false" size="100" enableValidation="false" name="street" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="" type="string" isExp="false" elemId="27866848" isPk="false" size="100" enableValidation="false" name="city" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="" type="string" isExp="false" elemId="27866849" isPk="false" size="100" enableValidation="false" name="state" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="" type="string" isExp="false" elemId="27866850" isPk="false" size="100" enableValidation="false" name="zip" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="" type="string" isExp="true" elemId="27866851" isPk="false" size="100" enableValidation="false" name="address" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<matchingTab enableIdentify="false" matching="true">
				<matchingTabColumns>
					<column elemId="27877985" size="1000" enableValidation="false" name="uni_rule_name" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27877986" size="" enableValidation="false" name="uni_match_id" label="" type="long_int" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27877987" size="1000" enableValidation="false" name="mat_value" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27877988" size="1000" enableValidation="false" name="mat_type" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</matchingTabColumns>
				<multipleMatching disableDefault="false">
					<matchingDefinitions/>
					<rematchIfChangedSectionColumns/>
				</multipleMatching>
				<matchingProposals enable="false"/>
			</matchingTab>
			<aggregationTab groupingColumn="" aggregation="false">
				<aggregationTabColumns/>
			</aggregationTab>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views/>
				<computedColumns/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
			</guiTab>
			<advanced>
				<specialColumns>
					<column elemId="27867303" size="1000" enableValidation="false" name="cio_address_comp" createInto="address_clean" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27867304" size="" enableValidation="false" name="mda_override" createInto="load" label="" type="boolean" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</specialColumns>
				<historicalColumns/>
				<oldValueColumns/>
			</advanced>
		</table>
		<table elemId="27866560" topLevel="false" name="contact" label="" type="instance">
			<description></description>
			<columns>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="" type="string" isExp="false" elemId="27867678" isPk="true" size="200" enableValidation="false" name="source_id" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="" type="string" isExp="false" elemId="27867679" isPk="false" size="200" enableValidation="false" name="party_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="" type="string" isExp="true" elemId="27867680" isPk="false" size="20" enableValidation="false" name="type" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="" type="string" isExp="true" elemId="27867681" isPk="false" size="100" enableValidation="false" name="value" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<matchingTab enableIdentify="false" matching="true">
				<matchingTabColumns>
					<column elemId="27878199" size="1000" enableValidation="false" name="uni_rule_name" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27878200" size="" enableValidation="false" name="uni_match_id" label="" type="long_int" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27878201" size="1000" enableValidation="false" name="mat_value" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27878202" size="1000" enableValidation="false" name="mat_type" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</matchingTabColumns>
				<multipleMatching disableDefault="false">
					<matchingDefinitions/>
					<rematchIfChangedSectionColumns/>
				</multipleMatching>
				<matchingProposals enable="false"/>
			</matchingTab>
			<aggregationTab groupingColumn="" aggregation="false">
				<aggregationTabColumns/>
			</aggregationTab>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views/>
				<computedColumns/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
			</guiTab>
			<advanced>
				<specialColumns>
					<column elemId="27867886" size="1000" enableValidation="false" name="cio_contact_comp" createInto="contact_clean" label="" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27867887" size="" enableValidation="false" name="mda_override" createInto="load" label="" type="boolean" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</specialColumns>
				<historicalColumns/>
				<oldValueColumns/>
			</advanced>
		</table>
	</tables>
	<dicTables/>
</instanceModel>