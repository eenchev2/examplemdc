<?xml version='1.0' encoding='UTF-8'?>
<exportModel>
	<consolidationExportOperations>
		<fullMasterExport elemId="27871512" allEntities="true" scope="use global scope (from Preferences)" name="master_layer_export_full" layerName="masters">
			<description></description>
			<selectedTables/>
			<advanced enableTID="false">
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</fullMasterExport>
		<complexExport elemId="27872407" scope="use global scope (from Preferences)" name="instance_low_dq_full">
			<description></description>
			<dataSources>
				<conditionalInstanceDataSource elemId="27872408" mode="full" entityName="party" prefix="inst" sourceSystem="" scope="use scope of parent export operation" allColumns="true">
					<conditions>
						<condition elemId="27872409" column="sco_instance" value="10000" operator="&gt;=">
							<description></description>
						</condition>
					</conditions>
					<columns/>
				</conditionalInstanceDataSource>
			</dataSources>
			<advanced enableTID="false">
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</complexExport>
		<complexExport elemId="27873266" scope="use global scope (from Preferences)" name="instance_low_dq_crm_increment">
			<description></description>
			<dataSources>
				<conditionalInstanceDataSource elemId="27873267" mode="delta" entityName="party" prefix="inst" sourceSystem="crm" scope="use scope of parent export operation" allColumns="true">
					<conditions>
						<condition elemId="27873268" column="sco_instance" value="10000" operator="&gt;=">
							<description></description>
						</condition>
					</conditions>
					<columns/>
				</conditionalInstanceDataSource>
			</dataSources>
			<advanced enableTID="false">
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</complexExport>
	</consolidationExportOperations>
	<sorExportOperations/>
</exportModel>