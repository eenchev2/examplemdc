<?xml version='1.0' encoding='UTF-8'?>
<metadata xmlns:ame="http://www.ataccama.com/ame/md">
	<settings ame:include="md/settings.md"/>
	<systems>
		<system ame:include="md/connected_systems/system_crm.md"/>
		<system ame:include="md/connected_systems/system_life.md"/>
	</systems>
	<logicalModel>
		<instanceModel ame:include="md/logical_model/instance_model.md"/>
		<masterModels>
			<masterModel ame:include="md/logical_model/master_Model_masters.md"/>
		</masterModels>
		<dictionary ame:include="md/logical_model/reference_data_model.md"/>
		<sorModel ame:include="md/logical_model/sor_model.md"/>
		<datasets ame:include="md/logical_model/datasets.md"/>
	</logicalModel>
	<outputOperations>
		<exportModel ame:include="md/output_interfaces/export_operations.md"/>
		<eventHandler ame:include="md/output_interfaces/event_handler.md"/>
	</outputOperations>
	<nativeServices ame:include="md/services.md"/>
	<streaming enable="true">
		<consolidationStreaming>
			<consumers/>
		</consolidationStreaming>
		<sorStreaming>
			<consumers/>
		</sorStreaming>
	</streaming>
	<guiConfig>
		<searchTab ame:include="md/gui_config/searchTab.md"/>
		<hierarchies ame:include="md/gui_config/hierarchies.md"/>
		<actions ame:include="md/gui_config/actions.md"/>
		<indicators ame:include="md/gui_config/indicators.md"/>
		<wfConfig ame:include="md/gui_config/wfConfig.md"/>
		<globalValidations ame:include="md/gui_config/globalValidations.md"/>
		<guiPreferences ame:include="md/gui_config/guiPreferences.md"/>
		<dataNavigation ame:include="md/gui_config/dataNavigation.md"/>
		<auditing ame:include="md/gui_config/auditing.md"/>
	</guiConfig>
	<advancedSettings>
		<manualMatch ame:include="md/advanced_settings/manualMatch.md"/>
		<taskInfoExport ame:include="md/advanced_settings/taskInfoExport.md"/>
		<reprocessSettings ame:include="md/advanced_settings/reprocessSettings.md"/>
		<historyPlugin ame:include="md/advanced_settings/historyPlugin.md"/>
		<migration ame:include="md/advanced_settings/migration.md"/>
		<sorInitialBatchLoad>
			<sorLoadOperations/>
		</sorInitialBatchLoad>
	</advancedSettings>
	<preview/>
	<documentationRoot calculateDoc="false"/>
</metadata>